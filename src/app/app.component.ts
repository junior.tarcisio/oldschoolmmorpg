import { GameConstants } from './game-engine/game-constants';
import { AudioManager } from './game-engine/audio-manager';
import { Game } from './game-engine/game'
import { ChangeDetectorRef } from '@angular/core'
import { Component, ViewChild, ElementRef } from '@angular/core'
import { bounce, conditionAnimation } from './game-ui/animations/animations';
import DraggingManager from './game-ui/dragging-manager';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations : [bounce(), conditionAnimation()]
})
export class AppComponent {
  @ViewChild('gamecanvas') gameCanvas : ElementRef
  @ViewChild('gamecanvas2') gameCanvas2 : ElementRef
  @ViewChild('name') name : ElementRef
  audioManager = AudioManager
  resolutionScale = 3
  canvasWidth = 792
  canvasHeight = 648
  draggingManager = DraggingManager

  game : Game = null
  start = false
  mousePos = { x: 0, y: 0}
  huds = {
    performance : false,
    mapeditor : false
  }
  loginBgSound = new Audio('assets/intro_ac.mp3');
  overIn = new Audio('assets/over-in.mp3');
  overOut = new Audio('assets/over-out.mp3');

  constructor(private cdRef:ChangeDetectorRef) {}

  volumeChange(volume:number) {
    this.loginBgSound.volume = volume
    localStorage.setItem('global-volume', volume.toString())

    if (this.game && this.game.running) {
      this.game.audioManager.SOUNDS.GET_1.play()
    }
  }

  ngAfterViewInit() {
    this.name.nativeElement.focus()
    this.name.nativeElement.setSelectionRange(this.name.nativeElement.value.length, this.name.nativeElement.value.length)
    this.loginBgSound.play() //boring when testing
    document.addEventListener('mousemove',(e) => {
      this.mousePos.x = e.clientX
      this.mousePos.y = e.clientY
    })


    const previousVolume : number = +localStorage.getItem('global-volume')

    if (previousVolume != null)  {
      setTimeout(()=> {
        this.audioManager.globalVolume = previousVolume
        this.volumeChange(previousVolume)   
      }) 
    }

    const previousResolution : number = +localStorage.getItem('resolution')

    if (previousResolution != null && previousResolution > 1 && previousResolution < 5)  {
      setTimeout(()=> {
        this.resolutionChange(previousResolution)   
        this.screenResize()
      }) 
    }
  }

  enterName() {
    const name = this.name.nativeElement.value
    if (name.length == 0)
      return
    else  {
      window['mmo_hover_remove']()
      this.start = true
      setTimeout(() => this.startGame(name))
      
      // const interval = setInterval(() => {

      //   if (this.loginBgSound.volume > 0.25) 
      //     this.loginBgSound.volume -= 0.1
      //   else 
      //     clearInterval(interval)
      // },500)
    }
  }

  capitalize = (s:string) => s.charAt(0).toUpperCase() + s.slice(1)
  
  startGame(name) {
    this.game = new Game(this.gameCanvas.nativeElement as HTMLCanvasElement, 
                         this.gameCanvas2.nativeElement as HTMLCanvasElement)
    window['game'] = this.game
    this.game.start()
    //this.game.audioManager.SOUNDS.MUSIC_INTRO_1.play()
    this.game.player.name = name
    this.cdRef.detectChanges()
    
    window.addEventListener('resize', () => {
      this.screenResize()
    })
  }

  getGameFilter() {
    if (!this.game || !this.game.running || !this.game.player)
      return ""

    if(!this.game.player.alive)
      return `grayscale(100% ) blur(3px) brightness(50%)`

    const percentDmgTaken = (1-(this.game.player.health / this.game.player.max_health)) * 100

    if (percentDmgTaken < 10)
      return ""

    const grayScale = (percentDmgTaken - 10) * 1.10

    if (grayScale > 85)
      return `grayscale(${ grayScale }% ) blur(2px)`
    else if (grayScale > 70)
      return `grayscale(${ grayScale }% ) blur(1px)`
    else 
      return `grayscale(${ grayScale }%)`
  }

  getMovingItemSprite() {
    if (this.game && this.game.player.alive && this.game.player.movingItem != null) 
      return `url('assets/ItemSpriteTable.png') 0px -${this.game.player.movingItem.getDefinition().frames[0] * 24}px`
    else 
      return null
  }
  
  getDayTime() {
    if (this.game == null) return ""
    const getMinutes = (time) => ((time%1)*60)|0
    const getHour = (time) => time|0
    return `${ getHour(this.game.nightEffectManager.dayHour).toString().padStart(2,'0') }:${ getMinutes(this.game.nightEffectManager.dayHour).toString().padStart(2,'0') }` 
  }

  hasDialogNPC() {
      if (this.game == null || !this.game.running) return
      
      if (this.game.player.npcTarget)
        return true
      else 
        return false
  }

  resolutionChange(value) {
    if (value < 2.2)
      value = 2.2
      
      GameConstants.onChangeResolution(value)
      this.canvasWidth = value * GameConstants.SPRITE_REAL_RESOLUTION * 11
      this.canvasHeight = value * GameConstants.SPRITE_REAL_RESOLUTION * 9

      setTimeout(()=> {
        if (this.game && this.game.ctx) {
          this.game.ctx.imageSmoothingEnabled = false
          this.game.ctx2.imageSmoothingEnabled = false
        }
      })

      localStorage.setItem('resolution', value.toString())
  }

  resizing = false
  resizingClientY
  startResizing(e) {
    console.log(e)
    this.resizing = true
    this.resizingClientY = e.clientY
  }

  resize(e) {
    if (!this.resizing)
      return false

    const amountPercent = (e.clientY / this.resizingClientY)
    const screenMultiplier = GameConstants.SPRITE_SIZE_MULTIPLIER * amountPercent

    if (screenMultiplier > 2.2 && screenMultiplier < 5 && window.innerHeight > screenMultiplier * GameConstants.SPRITE_REAL_RESOLUTION * 9)    {
      this.resolutionChange(screenMultiplier)
      this.resizingClientY = e.clientY
    }

  }  

  stopResizing(e) {
    this.resizing = false
  }

  checkScreenSize() {
    if (window.innerHeight - 70 < GameConstants.SPRITE_SIZE_MULTIPLIER * GameConstants.SPRITE_REAL_RESOLUTION * 9) {

      const percentageWindow =  (window.innerHeight - 70) / (GameConstants.SPRITE_SIZE_MULTIPLIER * GameConstants.SPRITE_REAL_RESOLUTION * 9)
      const screenMultiplier = GameConstants.SPRITE_SIZE_MULTIPLIER * percentageWindow

      this.resolutionChange(screenMultiplier)
    }
  }

  screenResize() {
    const percentageWindow =  (window.innerHeight - 70) / (GameConstants.SPRITE_SIZE_MULTIPLIER * GameConstants.SPRITE_REAL_RESOLUTION * 9)
    const screenMultiplier = GameConstants.SPRITE_SIZE_MULTIPLIER * percentageWindow
    // console.log(`old percentageWindow ${percentageWindow} old GameConstants.SPRITE_SIZE_MULTIPLIER ${GameConstants.SPRITE_SIZE_MULTIPLIER}`)
    // console.log(screenMultiplier)

    this.resolutionChange(screenMultiplier)
  }

}
