import { Game } from "./game"
import { MapEditorSelectionType } from "./map/map-editor"
import { Direction } from "./enumerables/direction"
import { KeyCodes } from "./enumerables/key-codes"
import { MagicEffect } from "./effects/magic-effect"
import { GameConstants } from "./game-constants"
import { Monster } from "./things/monsters/monster"
import itemsDefinition from "./things/items/items-definition"
import tiles from "./map/tiles"
import { ItemFlags } from "./things/items/item-flags";
import { PlayerSpeechType } from "./things/player_speeches";
import { NPC } from "./things/npcs/npc";

/* =================================================================================
* =================================================================================
*                            Commands events
* 
*  Looks a good idea to move the client player events to another layer
*  There's a big bunch of logics here that could be more separated and better 
*  somwhere else. Like a controller idk.
* 
*  There are more logics inside the game start 
* =================================================================================
* ================================================================================= */

class ClickCommand {
    public collidedHitBox : boolean = false
    constructor(public x : number, public y : number, public leftButton : boolean = true) {}
}

export class GameCommandsDOM {
    private game : Game
    public pressedKeys = []
    public mousePos = { x: 0, y: 0}
    public clicksDones : Array<ClickCommand> = []

    bindCommands(game:Game) {
        this.game = game

        game.canvas2.addEventListener("click", (e) => {
            this.clicksDones.push(new ClickCommand(e.offsetX, e.offsetY))
        }, false)

        game.canvas2.addEventListener("contextmenu", (e) => {
            this.clicksDones.push(new ClickCommand(e.offsetX, e.offsetY, false))
            e.preventDefault()
            return false
        }, false);

        game.canvas2.addEventListener("auxclick", (e:MouseEvent) => {
            if (e.button == 1) {
                this.lookCommand(game.eventPositionToSQMPosition(e))
                e.preventDefault()
                return false
            }
        }, false);

        game.canvas2.addEventListener('mousemove', (e) => {
            const rect = game.canvas.getBoundingClientRect()
            this.mousePos =  {
                x: (e.clientX - rect.left)|0,
                y: (e.clientY - rect.top)|0
            };
        }, false)

        document.body.addEventListener("keydown", (e) => {
            var key = e.keyCode || e.which
            
            // console.log(key) 16 shift, 17 control
            this.pressedKeys[e.keyCode] = true
             console.log(key)

            if (key == 13)
                document.getElementById('message').focus()
        })

        document.body.addEventListener("keyup",(e) => {
            this.pressedKeys[e.keyCode] = false
        })
        
        // document.body.addEventListener("keypress",(e) => {
        //     return false
        // })

        const sendSpeechCommand = () => {
            const input = document.getElementById('message') as HTMLInputElement
        
            if (input.value.length == 0) return

            game.player.speak(input.value)
            input.value = ''
        }

        document.getElementById('btn-send').addEventListener("click", sendSpeechCommand)
        document.getElementById('message').addEventListener("keydown", (e) => {
            var key = e.keyCode || e.which
            if (key == 13) sendSpeechCommand()
        })
    }

    lookCommand(screenSQMPos) {

        if (this.game.mapEditor.selection.type > 0) {
            this.game.mapEditor.cleanSelection()
            return
        }

        const mapSQMPos =  {
            x : this.game.player.pos.x + screenSQMPos.x,
            y : this.game.player.pos.y + screenSQMPos.y 
        }


        let msg : string;

        const creature = this.game.getCreatureAtMapPos(mapSQMPos)

        if (creature) {
            if (creature instanceof Monster)
                msg = `You see a ${creature.getName()}.\nIt's is a hostile creature.`
            else 
                msg = `You see ${creature.getName()}.`

        }
        else {
            const item = this.game.getItemAtMapPos(mapSQMPos)

            if (item) {
                const itemDef = itemsDefinition[item.id]
                msg = `You see a ${itemDef.name }.\nIt weighs ${itemDef.weight} kg`
            }
            else {
                let tile;
        
                if (this.game.map[mapSQMPos.y] && this.game.map[mapSQMPos.y][mapSQMPos.x])
                    tile = tiles[this.game.map[mapSQMPos.y][mapSQMPos.x].tile]
                else 
                    tile = tiles[1]

                msg = `You see ${tile.name}.`
            }
        }
        
        this.game.screenMessageManager.addMessage(msg)
    }

    updateMouseCommands() {

        //it will clear the clicks inside the hit boxes
        this.checkMouseEventsOnHitboxes()

        while(this.clicksDones.length > 0) {
            const click = this.clicksDones.pop()

            if (click.collidedHitBox)
                continue
            
            this.checkMouseEventsOnMapAndItems(this.game.pixelPositionToSQMPosition(click), click.leftButton)
        }
    }
    
    checkMouseEventsOnHitboxes() {
        //const timehitbox = performance.now()
        let isCursorFocusFound = false

        for (let i = 0; i < this.game.hitboxes.length; i++) {
            const hitbox = this.game.hitboxes[i]
            
            for (let j = 0; j < this.clicksDones.length; j++) {
                const click = this.clicksDones[j]

                if (click.x >= hitbox.sX && click.x <= hitbox.sX + hitbox.eX &&
                    click.y >= hitbox.sY && click.y <= hitbox.sY + hitbox.eY) {

                    if (hitbox.creature instanceof NPC) {
                        if (this.game.player.npcTarget !== hitbox.creature) {
                            this.game.player.npcTarget = hitbox.creature
                            this.game.audioManager.SOUNDS.NPC_SELAMAT_BELANJA_1.play()
                        }
                        else
                            this.game.player.npcTarget = null
                    } else {
                        if (this.game.player.target !== hitbox.creature)
                            this.game.player.target = hitbox.creature
                        else
                            this.game.player.target = null
                    }

                    click.collidedHitBox = true
                    //console.log('click collide')

                    if (isCursorFocusFound) {
                        if (this.clicksDones.length == 1) {
                            //console.log('the focus is already found and only one click')
                            return

                        }
                        else if (this.clicksDones.filter(x=> x.collidedHitBox == false).length == 0) {
                            //console.log('the focus is already found and no more clickes left')
                            return
                        }
                    }
                }
            }
            
            if (!isCursorFocusFound &&
                this.mousePos.x >= hitbox.sX && this.mousePos.x <= hitbox.sX + hitbox.eX &&
                this.mousePos.y >= hitbox.sY && this.mousePos.y <= hitbox.sY + hitbox.eY) {

                hitbox.creature.drawWithFocus( {x: hitbox.sX + hitbox.eX - GameConstants.spriteCalculatedSize, y:  hitbox.sY + hitbox.eY - GameConstants.spriteCalculatedSize})
                
                if (this.game.player.target != hitbox.creature) {
                    
                    //ico
                    if (hitbox.creature instanceof NPC) {
                        this.game.ctx2.globalAlpha = 0.9
                        this.game.ctx2.drawImage(NPC.speakIMG,
                            this.mousePos.x -  GameConstants.spriteCalculatedSize / 5,
                            this.mousePos.y -  (GameConstants.spriteCalculatedSize / 1.5) - ((GameConstants.spriteCalculatedSize/2) * (Math.abs(0.5 - this.game.frame.timeMs/1000))),
                            GameConstants.spriteCalculatedSize, 
                            GameConstants.spriteCalculatedSize)
                    } else {
                        this.game.ctx2.globalAlpha = 0.65
                        this.game.ctx2.drawImage(Monster.targetImgSmall,
                            this.mousePos.x -  GameConstants.spriteCalculatedSize / 2,
                            this.mousePos.y -  GameConstants.spriteCalculatedSize / 2,
                            GameConstants.spriteCalculatedSize, 
                            GameConstants.spriteCalculatedSize)
                    }
                    
                    // border
                    this.game.ctx2.beginPath()
                    this.game.ctx2.globalAlpha = 0.15
                    this.game.ctx2.lineWidth = 6
                    this.game.ctx2.strokeStyle = '#FFF'
        
                    this.game.ctx2.rect(
                        hitbox.sX, 
                        hitbox.sY, 
                        hitbox.eX, 
                        hitbox.eY)
                    this.game.ctx2.stroke()

                    this.game.ctx2.globalAlpha = 1
                    this.game.ctx2.closePath()

                    // IF NEWBIE MODE???
                    // desc 
                    // const label = "attack"
                    // this.ctx2.beginPath()
                    // this.ctx2.textAlign="center"
                    // this.ctx2.font = "bold 18px Calibri"
                    // this.ctx2.fillStyle = '#f22'
                    // this.ctx2.strokeStyle = '#000'
                    // this.ctx2.lineWidth = 1
                    // this.ctx2.fillText(label,
                    //     mousepos.x, 
                    //     mousepos.y -  GameConstants.spriteCalculatedSize / 2)
                        
                    // this.ctx2.strokeText(label,
                    //     mousepos.x, 
                    //     mousepos.y -  GameConstants.spriteCalculatedSize / 2)
                    // this.ctx2.stroke()
                    // this.ctx2.closePath()
                } 
                
                isCursorFocusFound = true
                if (this.clicksDones.filter(x=> x.collidedHitBox == false).length == 0) {
                    return
                }
            }
        }

        //console.log('hitboxes cost: ' + ( performance.now() - timehitbox))

        return
    }

    checkMouseEventsOnMapAndItems(screenSQMPos, leftButton:boolean=false) {
        
        const mapSQMPos =  {
            x : this.game.player.pos.x + screenSQMPos.x,
            y : this.game.player.pos.y + screenSQMPos.y 
        }
        

        if (this.game.map[mapSQMPos.y] == null || this.game.map[mapSQMPos.y][mapSQMPos.x] == null)
            return

        if (this.game.player.movingItem) {
            this.game.audioManager.SOUNDS.GET_1.play()

            if (this.game.map[mapSQMPos.y][mapSQMPos.x].tile == 0) {
                this.game.addComponent(new MagicEffect({x:mapSQMPos.x,y:mapSQMPos.y}, this.game, 3)) 
            }
            else {
                this.game.map[mapSQMPos.y][mapSQMPos.x].addItem(this.game, mapSQMPos,this.game.player.movingItem)
            }
            
            this.game.player.movingItem = null
            return
        }

        //Mapeditor
        if (this.game.mapEditor.isEditing()) {

            if (leftButton)
                this.game.mapEditor.editSQM(mapSQMPos)
            else 
                this.game.mapEditor.selection.id = this.game.map[mapSQMPos.y][mapSQMPos.x].tile

            return
        }

        const item = this.game.getItemAtMapPos(mapSQMPos)

        //goingGetItem
        if (item) {

            if (leftButton && item.getDefinition().flags & ItemFlags.PICKABLE) {
                if (this.game.player.closeTo(mapSQMPos)) {
                    this.game.player.tryGetItem(mapSQMPos)
                    return
                }
                
                this.game.pointer.goingGetItem = true      
            } 

        } else {
            this.game.pointer.goingGetItem = false
        }


        if (!leftButton) {
            if (this.game.player.closeTo(mapSQMPos)) {
                this.game.player.tryUseItem(mapSQMPos)
                return
            }
            
            this.game.pointer.goingUseItem = true 
        } else {
            this.game.pointer.goingUseItem = false
        }
        

        //check walking
        this.game.pointer.pos.x = mapSQMPos.x
        this.game.pointer.pos.y = mapSQMPos.y
        
        // const timer = performance.now()
        // for (let i = 0; i < 2000; i++) {          
        //     this.game.pathfind.find(this.game.map,nodestart, nodefinal)   
        // }
        // const result = performance.now() - timer
        // this.game.addMessageHistory('execution timer: ' + result);

        this.game.pointer.path = this.game.pathfind.findArray(this.game.player.pos, this.game.pointer.pos, 200)  
        
        if (this.game.pointer.path.length > 0) {
            this.game.pointer.alive = true
            this.game.playerSpeeches.playIfTimeout(this.game.frame.time, PlayerSpeechType.MOVE)
        }

        // while (path != null) {
        //     this.game.addComponent(new MagicEffect({x:path.x,y:path.y}, this.game, 1)) 
        //     path = path.parent
        // }
    }
    
    mousePositionTOSQMPosition() {
        return this.game.eventPositionToSQMPosition( {
            offsetX :this.mousePos.x,
            offsetY :this.mousePos.y
        })
    }

    getMovingTo() : Direction {
        
        if (this.pressedKeys[KeyCodes.LEFT] || this.pressedKeys[KeyCodes.ALT_LEFT])
            return Direction.LEFT

        else if (this.pressedKeys[KeyCodes.UP] || this.pressedKeys[KeyCodes.ALT_UP])
            return Direction.DOWN

        else if (this.pressedKeys[KeyCodes.RIGHT] || this.pressedKeys[KeyCodes.ALT_RIGHT])
            return Direction.RIGHT

        else if (this.pressedKeys[KeyCodes.DOWN] || this.pressedKeys[KeyCodes.ALT_DOWN])
            return Direction.UP
        
        else
            return null
    }
}
