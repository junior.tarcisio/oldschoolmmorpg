import { Game } from './game'
import { Thing } from './things/thing';
import { GameConstants } from './game-constants';

export default class Pointer extends Thing {
  player = true
  game: Game = null
  img
  ticks_reverse = false
  thicks = 0
  goingGetItem = false
  goingUseItem = false
  path : Array<any>

  constructor(game: Game) {
    super({x:0, y: 0}, game)
    this.game = game
    this.alive = false
    
    this.img =  new Image
    this.img.src = "assets/arrow_00.png";
  }

  update() { }

  draw() {
    if (!this.alive) return

    if (!this.ticks_reverse)
      this.thicks++;
    else 
      this.thicks--;

    if (this.thicks > 10) {
      this.ticks_reverse = true;
    }
    else if (this.thicks < -10)
      this.ticks_reverse = false;

    const screenPixelsPosition =
      this.game.gamePositionToScreenPixelsPosition({ x: this.pos.x, y: this.pos.y })

    this.game.ctx2.beginPath()
    
    const adjustedPositionX = screenPixelsPosition.x +
      GameConstants.spriteCalculatedSize / 2

    const adjustedPositionY = screenPixelsPosition.y +
      GameConstants.spriteCalculatedSize / 2

    
    this.game.ctx2.globalAlpha = 0.5;
    this.game.ctx2.shadowOffsetX = -10;
    this.game.ctx2.shadowOffsetY = 10;
    this.game.ctx2.shadowBlur = 25;
    this.game.ctx2.shadowColor = "#000000C0";
    
    this.game.ctx2.arc(adjustedPositionX, adjustedPositionY, 10 + this.thicks/3, 0, 2 * Math.PI)
    this.game.ctx2.fillStyle = '#88EEFFB0'
    this.game.ctx2.fill()
    this.game.ctx2.lineWidth = 4
    this.game.ctx2.strokeStyle = '#0099AAB0'
    this.game.ctx2.stroke()

    
    const adjustY = -1* GameConstants.spriteCalculatedSize / 2;
    this.game.ctx2.drawImage(this.img,
      screenPixelsPosition.x,
      screenPixelsPosition.y + this.thicks + adjustY,
      this.img.width * GameConstants.SPRITE_SIZE_MULTIPLIER, this.img.height * GameConstants.SPRITE_SIZE_MULTIPLIER);
      this.game.ctx2.globalAlpha = 1;

    
    this.game.ctx2.shadowOffsetX = 0;
    this.game.ctx2.shadowOffsetY = 0;
    this.game.ctx2.shadowBlur = 0;
  }
}
