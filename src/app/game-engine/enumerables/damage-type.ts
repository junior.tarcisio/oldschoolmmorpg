export enum DamageType {
    PHYSICAL = "#f11",
    FIRE = "#fa0",
    ENERGY = "#14f",
    ICE = "#1df",
    POISON = "#0f1",
    DEATH = "#500",
    WHITE_EXP = "#FFF",
    HEALING = "#5EF"
}