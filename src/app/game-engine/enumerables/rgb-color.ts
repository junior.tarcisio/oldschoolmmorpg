export enum LightColorRGB {
    RED = '#FF0000',
    BLUE = '#0022FF',
    GREEN = '#00FF00'
}