export enum KeyCodes {
    LEFT = 37,
    DOWN = 38,
    RIGHT = 39,
    UP = 40,
    ALT_LEFT = 65,
    ALT_UP = 83,
    ALT_RIGHT = 68,
    ALT_DOWN = 87,
    SHIFT = 16,
    CONTROL = 17,
    ALT = 18
}