
    /*
     * Ideas
     * 
        What about transforming these calcs on functional programming and like:
        
        const position
                .toScreenPosition()   <-- it would be a class
                .adjustX(-10)
                .centerY

        - refactor all pos.x/y to a class
        - Position abstract x, y
        - PositionSquareMap
            can become
            -> PositionSquareScreen
                can become too        
            -> PositionPixelScreen


        its possible to make a class for abstracting CTX
            receive posSQM
            receive propertylist (that will be copied to ctx)

            ex:
                draw ({
                    pos : pos,
                    adjustX => (x, SQMSize) x + SQMSize / 2
                    adjustY => (y, SQMSize) y + SQMSize / 2
                    fillStyle : '',
                    lineWidth : 2
                })

            maybe just return the drawDefinition and it can be draw
            inside game, very FP:

            return {
                method : drawImage
                pos : pos,
                adjustX => (x, SQMSize) x + SQMSize / 2
                adjustY => (y, SQMSize) y + SQMSize / 2
                fillStyle : '',
                lineWidth : 2
            }
    */
        //testing getImageData Performance
        //var imgData = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
        //this.ctx.putImageData(imgData, 0, 0);

        //illuminated.js http://greweb.me/2012/05/illuminated-js-2d-lights-and-shadows-rendering-engine-for-html5-applications/
        //fps drops with 1 light (1000 -> 300 and doesn't really work well with existing components on this game I guess)
        // eval(`
        // var lamp = new window.illuminated.Lamp({  
        //     position: new window.illuminated.Vec2(${this.spriteCalculatedSize()*6-36}, ${this.spriteCalculatedSize()*5-36}),  
        //     distance: 220,  
        //     diffuse: 0.8,  
        //     color: 'rgba(250,220,150,0.3)',  
        //     radius: 150,  
        //     samples: 10,  
        //     angle: 0,  
        //     roughness: 0
        //   }).render(this.ctx)`)

        // var c=document.getElementById('myCanvas');
        // var ctx=c.getContext('2d');
        // ctx.fillStyle='blue';
        // ctx.fillRect(10,10,50,50);
        // ctx.globalCompositeOperation='destination-out';
        // ctx.beginPath();
        // ctx.fillStyle='red';
        // ctx.arc(50,50,30,0,2*Math.PI);
        // ctx.fill();
            
        // let contrastImage = function (imgData, contrast){  //input range [-100..100]
        //     var d = imgData.data;
        //     contrast = (contrast/100) + 1;  //convert to decimal & shift range: [0..2]
        //     var intercept = 128 * (1 - contrast);

        //     for(var i=0;i<d.length;i+=4){   //r,g,b,a
        //         d[i] = d[i] * 4  //*contrast + intercept;
        //         d[i+1] = d[i+1] *4 //*contrast + intercept;
        //         d[i+2] = d[i+2] * 4 //*contrast + intercept;
        //     }

        //     return imgData;
        // }
        
        // const pos = this.player.getPixelPositionOnScreen()
            
        // this.ctx.arc(pos.x+36, pos.y+36, 50, 0, 2 * Math.PI)
        // this.ctx.fillStyle = '#FFFFFF50'
        // this.ctx.fill() 

        // let d = this.ctx.getImageData(pos.x-72*2, pos.y-72*2, 72 + 72*4, 72 + 72*4);       
        // this.ctx.putImageData(contrastImage(d, 90),pos.x-72*2, pos.y-72*2);