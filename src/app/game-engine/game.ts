import { Monster } from './things/monsters/monster';
import { MapEditor } from './map/map-editor'
import { MapTile, MapFactory } from './map/map'
import { Direction } from './enumerables/direction'
import { ScreenMessageManager } from './things/screen-message-manager'
import { NightEffectManager } from './effects/night-effect-manager'
import { WeatherManager } from './effects/weather-manager'
import { Creature } from './things/creature'
import { FpsCounter } from './fps-counter'
import { AudioManager } from './audio-manager'
import { Item } from './things/items/item'
import { GameCommandsDOM } from './game-commands-dom'
import { PathFind } from './pathfind'
import tiles from './map/tiles'
import Player from './things/player'
import Pointer from './pointer'
import { GameFrame } from './game-frame'
import { GameConstants } from './game-constants'
import { PlayerSpechees } from './things/player_speeches';
import { NPC } from './things/npcs/npc';
import { MagicEffect } from './effects/magic-effect';
import { ItemFlags } from './things/items/item-flags';

export class HitBox { 
    constructor( public creature:Creature, public sX:number, public eX:number, public sY:number, public eY:number) {}
}

export class Game {
    public canvas: HTMLCanvasElement
    public ctx: CanvasRenderingContext2D
    public canvas2: HTMLCanvasElement
    public ctx2: CanvasRenderingContext2D

    public pSetTimeoutID: Number
    public drawMovingModifierX: number
    public drawMovingModifierY: number

    public running: boolean = false
    private paused = false;
    public updateTimer
    public drawTimer

    public map : Array<Array<MapTile>>
    public player : Player
    public pointer: Pointer
    public hitboxes :Array<HitBox>
    public screenMessageManager : ScreenMessageManager
    public nightEffectManager : NightEffectManager
    public weatherManager : WeatherManager
    public messageHistory : Array<any>
    public messageHistoryLast : number
    public callbackMessageHistory : Function
    public fps : FpsCounter
    public mapEditor : MapEditor
    public audioManager : AudioManager    
    public pathfind : PathFind
    public frame : GameFrame
    public gameCommandsDOM : GameCommandsDOM
    public playerSpeeches : PlayerSpechees
    public respawnList : Array<{respawnTime:number, monster:Monster}>

    constructor(canvas: HTMLCanvasElement, canvas2 : HTMLCanvasElement) {
        this.canvas = canvas
        this.canvas2 = canvas2
        this.ctx = this.canvas.getContext("2d")
        this.ctx2 = this.canvas2.getContext("2d")
        const highquality = false;
        this.ctx.imageSmoothingEnabled = highquality
        this.ctx2.imageSmoothingEnabled = highquality
        //this.ctx.imageSmoothingQuality = "low"
    }

    start() {
        this.running = true
        this.frame = new GameFrame()
        this.map = new MapFactory().load(this)
        this.player = new Player(GameConstants.PLAYER_INITIAL_POSITION, this)
        this.map[GameConstants.PLAYER_INITIAL_POSITION.y][GameConstants.PLAYER_INITIAL_POSITION.x].addCreature(this.player)
        this.pointer = new Pointer(this)
        this.messageHistory = new Array()
        this.screenMessageManager = new ScreenMessageManager(this)
        this.nightEffectManager = new NightEffectManager(this)
        this.weatherManager = new WeatherManager(this)
        this.mapEditor = new MapEditor(this)
        this.audioManager = new AudioManager()
        this.fps = new FpsCounter()
        this.pathfind = new PathFind(this)
        this.hitboxes = new Array
        this.playerSpeeches = new PlayerSpechees(this.audioManager)
        this.respawnList = new Array
        
        this.screenMessageManager.addMessage(GameConstants.WELCOME_MESSAGE)      
        this.gameCommandsDOM = new GameCommandsDOM()  
        this.gameCommandsDOM.bindCommands(this)

        this.update()
    }

    addComponent (c) {
        if (!this.map) return
        
        if (this.map[c.pos.y] != null && this.map[c.pos.y][c.pos.x] != null)
            this.map[c.pos.y][c.pos.x].addEffect(c)
        else {
            console.log(`caugth invalid component:`)
            console.log(c)
        }
    }

    pause() {
        this.paused = !this.paused
    }
    
    addMessageHistory(message:string, type:number=0) {
        this.messageHistoryLast = this.frame.time
        this.messageHistory.push({
            creation : new Date(),
            message : message,
            type
        })

        if (this.messageHistory.length > 100)
            this.messageHistory.shift()

        if (this.callbackMessageHistory)
            this.callbackMessageHistory()
    }

    /* =================================================================================
     * =================================================================================
     *                            Loops game update e draw
     * =================================================================================
     * ================================================================================= */

//#region 
    
    update() {
        this.frame.next()

        if (!this.running) return

        if (this.paused) {
            this.drawPause()
            this.nextFrame(16)
            return
        }

        this.hitboxes = new Array

        //----------------------------------------------------
        // call update actions
        //----------------------------------------------------

        //maybe a list of middlewares with a common interface, but night draws first?? what about lightlist
        this.screenMessageManager.update()
        this.pointer.update()
        this.player.updateInventoryItems()
        this.respawnPendingMonsters()

        //----------------------------------------------------
        // call draw actions
        //----------------------------------------------------
        this.cleanCanvases()
        this.nightEffectManager.draw()  
        this.updateVisibleMap(this.frame.time)
        //this.updateHitboxes()
        this.gameCommandsDOM.updateMouseCommands()
        this.weatherManager.draw()

        //Components that uses the second canvas
        this.pointer.draw()
        this.mapEditor.drawMapEditorCursor(this.frame.time)
        this.screenMessageManager.draw()
        
        //finish draw performance
        //--------------------------------------------
        if (this.fps.show)
            this.fps.calculateFps(this.frame.time, 0, this.frame.getPerformanceTimerElapsed())
        
        this.nextFrame(this.frame.getPerformanceTimerElapsed())
    }

    nextFrame = (elapsedTime) => this.pSetTimeoutID = window.setTimeout(() => this.update(), 15 - (elapsedTime))

    addMonsterToRespawn(monster:Monster) {
        this.respawnList.push({
            respawnTime: this.frame.time + GameConstants.RESPAWN_TIME,
            monster : monster  
        })
    }

    respawnPendingMonsters() {
        let respawn

        while(this.respawnList.length > 0) {
            respawn = this.respawnList[0];
            
            //ordered
            if (respawn.respawnTime < this.frame.time) {
                this.respawnList.shift()
                respawn.monster.respawn()
            }
            else {
                return
            }
        }
    }
    
    //setTimeout can garantee more it won't overlap the previous frame (Better than setinterval? I guess so).
    //~62 cycles per second 16ms

    setDrawingMovingModifiers() {
        this.drawMovingModifierX = 0 
        this.drawMovingModifierY = 0 

        if (this.player.movingPercent > 0) {
            const modifierSize = GameConstants.spriteCalculatedSize * (this.player.movingPercent / 100) - GameConstants.spriteCalculatedSize
    
            //Get moving percent to screen
            if (this.player.movingToDirection != null && this.player.movingPercent != null) {
                switch (this.player.movingToDirection) {
                    case Direction.LEFT:
                        this.drawMovingModifierX -= modifierSize
                        break
                    case Direction.DOWN:
                        this.drawMovingModifierY += modifierSize
                        break
                    case Direction.RIGHT:
                        this.drawMovingModifierX += modifierSize
                        break
                    case Direction.UP:
                        this.drawMovingModifierY -= modifierSize
                        break
                }
            }
        }

        const calculatedCameraAcceleration = this.player.getCalculatedDrawCameraAcceleration()
        this.drawMovingModifierX += calculatedCameraAcceleration.x
        this.drawMovingModifierY += calculatedCameraAcceleration.y
    }

    cleanCanvases() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.ctx2.clearRect(0, 0, this.canvas.width, this.canvas.height)
    }

    updateVisibleMap(currentFrameTime:number) {

        let mapTile : MapTile

        const layersDraw = [
            new Array, //items
            new Array, //creatures
            new Array, //magic effects
        ]

        //speeches and magic effects on map?
        //remove component list?

        this.player.update()
        this.setDrawingMovingModifiers()
        
        //draw on screen tiles
        for (var y = -2; y < 11; y++) {
            for (var x = -2; x < 13; x++) {

                const tileDrawPosX = x * GameConstants.spriteCalculatedSize - this.drawMovingModifierX
                const tileDrawPosY = y * GameConstants.spriteCalculatedSize - this.drawMovingModifierY

                //if position invalid in map array, consider it as a water tile (0)
                if (this.map[y + this.player.pos.y - 4] != null  &&
                    this.map[y + this.player.pos.y - 4][x + this.player.pos.x - 5] != null) {
                    mapTile = this.map[y + this.player.pos.y - 4][x + this.player.pos.x - 5]

                    tiles[mapTile.tile].draw(this, currentFrameTime, tileDrawPosX, tileDrawPosY)
                
                    if (mapTile.itemList && mapTile.itemList.length > 0) {
                        for (let itemIndex = 0; itemIndex < mapTile.itemList.length; itemIndex++) {
                            const item = mapTile.itemList[itemIndex]
                            item.update(this, currentFrameTime)

                            if (!item.alive) {
                                mapTile.removeItem(item)
                                // this.addComponent(new MagicEffect({x: x, y: y}, this, MagicEffect.E_ENERGY))
                                continue
                            }

                            //layersDraw[0].push(item)
                            //pass the pos to the array somehow, can pass to the creature, so it simplifies the draw

                            //needs refactor maybe on layersdraw? pass thing + position to the array
                            if (!(item.getDefinition() .flags & ItemFlags.BLOCK_PATH))//can change to, item.ontop/2sqm?
                                item.draw(this, tileDrawPosX, tileDrawPosY, currentFrameTime)
                            else
                                layersDraw[1].push({thing: item, tileDrawPosX : tileDrawPosX, tileDrawPosY : tileDrawPosY})
                        }
                    }
                    
                    if (mapTile.creatureList && mapTile.creatureList.length > 0) {
                        for (let creatureIndex = 0; creatureIndex < mapTile.creatureList.length; creatureIndex++) {
                            const creature = mapTile.creatureList[creatureIndex]

                            if (creature === this.player)
                                continue

                            creature.update()

                            if (!creature.alive) {
                                mapTile.removeCreature(creature)
                                continue
                            }

                            layersDraw[1].push({thing: creature, tileDrawPosX : tileDrawPosX, tileDrawPosY : tileDrawPosY})
                            //item.draw(this, tileDrawPosX, tileDrawPosY, currentFrameTime)
                        }
                    }
                    
                    if (mapTile.effectsList && mapTile.effectsList.length > 0) {
                        for (let effectIndex = 0; effectIndex < mapTile.effectsList.length; effectIndex++) {
                            const effect = mapTile.effectsList[effectIndex]

                            if (!effect.alive) {
                                mapTile.removeEffect(effect)
                                continue
                            }

                            layersDraw[2].push({thing: effect, tileDrawPosX : tileDrawPosX, tileDrawPosY : tileDrawPosY})
                        }
                    }
                }
                else {
                    tiles[0].draw(this, currentFrameTime, tileDrawPosX, tileDrawPosY)
                }
            }
        }

        this.player.draw()
        
        for (let i = 0; i < layersDraw.length; i++) {
            for (let j = 0; j < layersDraw[i].length; j++) {
                if (layersDraw[i][j].thing.draw)
                    layersDraw[i][j].thing.draw(this, layersDraw[i][j].tileDrawPosX, layersDraw[i][j].tileDrawPosY, currentFrameTime)
            }
        }
    }

    drawPause() {
        const PAUSE_TEXT = "PAUSED"
        this.ctx.beginPath()
        this.ctx.textAlign = "center"
        this.ctx.font = "bold 64px Calibri"
        this.ctx.fillStyle = '#FFF'
        this.ctx.strokeStyle = '#000'
        this.ctx.lineWidth = 1
        this.ctx.fillText(PAUSE_TEXT, 400, 340)
        this.ctx.strokeText(PAUSE_TEXT, 400, 340)
    }

//#endregion
    
    /* =================================================================================
     * =================================================================================
     *
     *                            Position conversion events
     *                            --------------------------
     * 
     *  Refactor it into position objects
     *      -> Position (abstract) with x/y
     * 
     *  Especialized types, to formalize what type of position is being used and it's methods
     *      -> PositionPixelsScreen
     *          can become both SQM
     *          (does it make sense come back to pixel? maybe on drawing)
     *      -> PositionSQMGlobal 
     *          can become a sqm pos on screen
     *      -> PositionSQMScreen
     *          can become a sqm pos global
     *  
     * =================================================================================
     * ================================================================================= */
    
    getCreatureAtMapPos(mapSQMPos) : Creature {
        if (this.map[mapSQMPos.y] && 
            this.map[mapSQMPos.y][mapSQMPos.x] &&
            this.map[mapSQMPos.y][mapSQMPos.x].creatureList &&
            this.map[mapSQMPos.y][mapSQMPos.x].creatureList.length > 0)
            return this.map[mapSQMPos.y][mapSQMPos.x].creatureList[0]
        else 
            return null
    }

    getItemAtMapPos(mapSQMPos) : Item {
        if (this.map[mapSQMPos.y] && 
            this.map[mapSQMPos.y][mapSQMPos.x] &&
            this.map[mapSQMPos.y][mapSQMPos.x].itemList  &&
            this.map[mapSQMPos.y][mapSQMPos.x].itemList.length > 0)
            return this.map[mapSQMPos.y][mapSQMPos.x].itemList[this.map[mapSQMPos.y][mapSQMPos.x].itemList.length-1]
        else 
            return null
    }
    
    gamePositionToScreenSQMPosition = (pos) => {
        return {
            x : pos.x - this.player.pos.x + GameConstants.SCREEN_CENTER_SQM_X,
            y : pos.y - this.player.pos.y + GameConstants.SCREEN_CENTER_SQM_Y
        }  
    }

    gamePositionToScreenPixelsPosition = (pos) => {
        const screenSQMPosition = this.gamePositionToScreenSQMPosition(pos)
        return {
            x : screenSQMPosition.x * GameConstants.spriteCalculatedSize - this.drawMovingModifierX,
            y : screenSQMPosition.y * GameConstants.spriteCalculatedSize - this.drawMovingModifierY
        }
    }

    
    pixelPositionToSQMPosition(e) {
        return {
            x : (((e.x + this.drawMovingModifierX) / GameConstants.spriteCalculatedSize)|0) - GameConstants.SCREEN_CENTER_SQM_X,
            y : (((e.y + this.drawMovingModifierY) / GameConstants.spriteCalculatedSize)|0) - GameConstants.SCREEN_CENTER_SQM_Y
        }
    }

    eventPositionToSQMPosition(e) {
        return {
            x : (((e.offsetX + this.drawMovingModifierX) / GameConstants.spriteCalculatedSize)|0) - GameConstants.SCREEN_CENTER_SQM_X,
            y : (((e.offsetY + this.drawMovingModifierY) / GameConstants.spriteCalculatedSize)|0) - GameConstants.SCREEN_CENTER_SQM_Y
        }
    }
}
