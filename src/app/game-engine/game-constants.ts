export class GameConstants {
    public static readonly SPRITE_REAL_RESOLUTION = 24

    // public static readonly SPRITE_SIZE_MULTIPLIER = 4

    // public static readonly spriteCalculatedSize = GameConstants.SPRITE_REAL_RESOLUTION * GameConstants.SPRITE_SIZE_MULTIPLIER

    public static onChangeResolution(val) {
        GameConstants.SPRITE_SIZE_MULTIPLIER = val
        GameConstants.spriteCalculatedSize = GameConstants.SPRITE_REAL_RESOLUTION * GameConstants.SPRITE_SIZE_MULTIPLIER
    }
    
    public static SPRITE_SIZE_MULTIPLIER = 3
    public static spriteCalculatedSize = GameConstants.SPRITE_REAL_RESOLUTION * GameConstants.SPRITE_SIZE_MULTIPLIER

    public static GLOBAL_SHADOW_X = -1 * (GameConstants.spriteCalculatedSize/5)
    public static GLOBAL_SHADOW_Y = -1 * (GameConstants.spriteCalculatedSize/7)
    public static GLOBAL_SHADOW_BLUR = 25
    public static GLOBAL_SHADOW_COLOR = "#000000D0"
    
    //end

    public static readonly SCREEN_CENTER_SQM_X = 5
    public static readonly SCREEN_CENTER_SQM_Y = 4
    public static readonly PLAYER_INITIAL_POSITION = { x : 5, y : 4 }
    public static readonly WELCOME_MESSAGE = 'Welcome to NO-Bones, the oldschool mmorpg.\nI hope you enjoy your stay here'
    public static readonly RESPAWN_TIME = 120000
}