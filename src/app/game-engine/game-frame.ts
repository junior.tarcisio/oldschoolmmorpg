export class GameFrame {
    performanceTimer : number
    time : number
    random : number
    timeMs : number

    constructor() {
        this.next()
    }

    public next() {
        this.performanceTimer = performance.now()
        this.time = Date.now()
        this.random = Math.random()
        this.timeMs = this.time % 1000
    }

    public getPerformanceTimerElapsed() {
        return performance.now() - this.performanceTimer
    }
}