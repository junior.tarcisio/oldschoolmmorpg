export class AudioManager {
    public static globalVolume = 1

    readonly SOUNDS = {
        STORM : new ManagedAudio(this, 'assets/storm.mp3'),
        THUNDER_1 : new ManagedAudio(this, 'assets/thunder1.mp3'),
        THUNDER_2 : new ManagedAudio(this, 'assets/thunder2.mp3'),
        THUNDER_3 : new ManagedAudio(this, 'assets/thunder3.mp3'),
        STEP_1 : new ManagedAudio(this, 'assets/step1.mp3'),
        STEP_2 : new ManagedAudio(this, 'assets/step2.mp3'),
        STEP_3 : new ManagedAudio(this, 'assets/step3.mp3'),
        HIT : new ManagedAudio(this, 'assets/hit.mp3'),
        HIT_1_1 : new ManagedAudio(this, 'assets/hit1_1.mp3'),
        HIT_1_2 : new ManagedAudio(this, 'assets/hit1_2.mp3'),
        HIT_1_3 : new ManagedAudio(this, 'assets/hit1_3.mp3'),
        MAGIC_3 : new ManagedAudio(this, 'assets/magic3.mp3'),
        MAGIC_5 : new ManagedAudio(this, 'assets/magic5.mp3'),
        MAGIC_6 : new ManagedAudio(this, 'assets/magic6.mp3'),
        MONSTER_1 : new ManagedAudio(this, 'assets/monster1.mp3'),
        EXPLOSION_1 : new ManagedAudio(this, 'assets/explosion1.mp3'),
        HIT_2 : new ManagedAudio(this, 'assets/hit2.mp3'),
        DIE_1 : new ManagedAudio(this, 'assets/die1.mp3'),
        DIE : new ManagedAudio(this, 'assets/die.mp3'),
        GET_1 : new ManagedAudio(this, 'assets/get1.mp3'),
        PUT_1 : new ManagedAudio(this, 'assets/put1.mp3'),
        DRINK_1 : new ManagedAudio(this, 'assets/drink1.mp3'),
        // SPEECH_UFUAWD : new ManagedAudio(this, 'assets/speech_ufuawd.mp3'),
        // SPEECH_LETSGO : new ManagedAudio(this, 'assets/speech_letsgo_2.mp3'),
        // SPEECH_FOLLOWME : new ManagedAudio(this, 'assets/speech_followme.mp3'),
        // SPEECH_HAHA : new ManagedAudio(this, 'assets/speech_haha.mp3'),
        HIT_ARMOR_1 : new ManagedAudio(this, 'assets/hit_armor_1.mp3'),
        HIT_ARMOR_2 : new ManagedAudio(this, 'assets/hit_armor_2.mp3'),
        HIT_ARMOR_3 : new ManagedAudio(this, 'assets/hit_armor_3.mp3'),
        MONSTER_KA_00 : new ManagedAudio(this, 'assets/monster_KA_00.mp3'),
        MONSTER_KA_01 : new ManagedAudio(this, 'assets/monster_KA_01.mp3'),
        MONSTER_KA_02 : new ManagedAudio(this, 'assets/monster_KA_02.mp3'),
        MONSTER_POC_00 : new ManagedAudio(this, 'assets/monster_POC_00.mp3'),
        NPC_SELAMAT_BELANJA_1 : new ManagedAudio(this, 'assets/npc_selemat_belenaja_1.mp3'),
        COIN_1 : new ManagedAudio(this, 'assets/coin_1.mp3'),
        COIN_2 : new ManagedAudio(this, 'assets/coin_2.mp3'),
        CHEW_01 : new ManagedAudio(this, 'assets/chew_01.mp3'),
        CHEW_02 : new ManagedAudio(this, 'assets/chew_02.mp3'),
        MAGIC_ENERGY_01 : new ManagedAudio(this, 'assets/magic_energy_2.mp3'),
        MAGIC_ENERGY_02 : new ManagedAudio(this, 'assets/magic_energy_3.mp3'),
        MAGIC_FIRE_01 : new ManagedAudio(this, 'assets/magic_fire_1.mp3'),
        MUSIC_INTRO_1 : new ManagedAudio(this, 'assets/intro_ac.mp3'),
        // SPELL_LIGH_TEST : new ManagedAudio(this, 'assets/spell-light-test.mp3'),
        // SPELL_FLAMUS_TEST : new ManagedAudio(this, 'assets/spell-flamus-test.mp3')
    }
}

export class ManagedAudio  {
    private audio:HTMLAudioElement
    private volume :number

    constructor(private audioManager:AudioManager, audioURI:string) {
        this.audio = new Audio(audioURI)
    }

    play(volume:number=1) {
        this.setVolume(volume)
        this.audio.play()
            .catch(this.logError)
    }
    
    playLoop(volume:number=1) {
        if (!this.audio.loop) {
            this.audio.loop = true
            this.setVolume(volume)
            this.audio.play()
                .catch(this.logError)
        }
    }

    getVolume() {
        return this.volume
    }

    setVolume(volume:number) {
        if (volume > 1) volume = 1
        this.volume = volume
        this.audio.volume = AudioManager.globalVolume * volume
    }

    logError = (reason) => 
        console.log(`failed to play audio [${this.audio.src}], reason: ${reason}`)
}