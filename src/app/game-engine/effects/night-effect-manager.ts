import { Game } from "../game";


export class NightEffectManager {
    game : Game

    readonly endCircleAngle = 2*Math.PI
    readonly DAY_MAX_HOUR = 24.00
    
    dayHour = 6
    DRAWING_MODE_MULTI_LIGHT = true
    lightqueue : Array<LightEffect> = new Array
    reducedLightEffects = false

    constructor(game:Game){
        this.game = game
    }

    calculateDarknessRedByte = (dayHour:number) => {
        if (dayHour > 17 && dayHour < 20.5) {
            const redPeakTime = 18.5 //from 17,0 to 20,5
            const redDuration = 3.5
            return (redDuration/2 - Math.abs((redPeakTime - dayHour))) / (redDuration/2) 
        } else if (dayHour < 6.5 && dayHour > 4.5) {
            const redPeakTime = 5.5 //from 4,0 to 6,5
            const redDuration = 2
            return (redDuration/2 - Math.abs((redPeakTime - dayHour))) / (redDuration/2) 
        } else {
            return 0
        }
    }

    calculateBlueByte = (darknessByte:number) => (darknessByte/3) | 0

    draw() {
        this.dayHour += 0.001
        if (this.dayHour >= this.DAY_MAX_HOUR)
            this.dayHour = 0.00

        //converts 240  % into 255 for converting to hexa alpha channel later
        //subtract 128 ten multiply by 2, so the half will the max
        //from 0 - 254 [or 127*2] (max darkness)
        //255 - 127 = 128               night max
        //0   - 127 = -127 (abs = 127)  night max
        //127 - 127 = 0 (day max)
        let darknessAlphaByte //= (Math.abs((this.dayHour/this.DAY_MAX_HOUR)*255 - 127) * 2) | 0 

        if (this.dayHour > 7 && this.dayHour < 17) {
            darknessAlphaByte = 0
        } else if (this.dayHour < 4 || this.dayHour > 21) {
            darknessAlphaByte = 240
        } else if (this.dayHour > 17 && this.dayHour < 21) {

            const progressNightFall = (this.dayHour - 17)/4 //0->4 so  0-0 / 4-1
            darknessAlphaByte =  240 * progressNightFall | 0

        } else if (this.dayHour > 4 && this.dayHour < 7) {

            const progressNightFall = 1 - ((this.dayHour - 4)/3) //0->1 so  0-0 / 3-0
            darknessAlphaByte =  240 * progressNightFall | 0
        }

        //4-7 - getting day
        //7-17 = max
        //17-21 = getting dark
        //21 - 4 max night + blue later

        if (darknessAlphaByte > 245)
            darknessAlphaByte = 245
        
        darknessAlphaByte = darknessAlphaByte - this.game.weatherManager.thunderOpacity*2
        if (darknessAlphaByte < 0 ) darknessAlphaByte = 0

        const darknessColorHexa_A = darknessAlphaByte.toString(16).padStart(2,'0')


        const darknessRedByte = this.calculateDarknessRedByte(this.dayHour)        
        const darknessColorHexa_B  = this.calculateBlueByte(darknessAlphaByte).toString(16).padStart(2,'0') 
        
        let darknessColorHexa_RG
        if (darknessRedByte > 0) {       
            const calcShadeToHexa = (shadeFactor,maxColor) => Math.round(shadeFactor*maxColor).toString(16).padStart(2,'0')    
            darknessColorHexa_RG = `#${calcShadeToHexa(darknessRedByte,72)}${calcShadeToHexa(darknessRedByte,32)}`
        }
        else
            darknessColorHexa_RG = `#0000`

        //make the black cover
        this.drawDarkness(darknessColorHexa_RG, darknessColorHexa_A)
        
        const darknessColorHexa_RGBA = darknessColorHexa_RG + darknessColorHexa_B  + darknessColorHexa_A

        //TODO: fix screen pos to game pos        
        for (let i = 0; i < this.lightqueue.length; i++) {
            const light = this.lightqueue[i];            
            this.cutDarkness(light.pos.x + 32, light.pos.y + 32, darknessColorHexa_RGBA, darknessAlphaByte, light.intensity) 
        }
        
        for (let i = 0; i < this.lightqueue.length; i++) {
            const light = this.lightqueue[i];            
            if (!light.rgbColor) continue
            this.makeLightColorfulAt(light.pos.x + 32, light.pos.y + 32, light.rgbColor, darknessAlphaByte, light.intensity)
        }

        this.lightqueue = []
    }

    addLightEffect(pos, intensity:number, rgbColor:string =null) {
        if (this.lightqueue.length < 256)
            this.lightqueue.push(new LightEffect(pos, intensity, rgbColor))
    }

    //refactor to an object darknesscolor (rgba..), getComposition, etc...
    drawDarkness(darknessColorHexa_RG, darknessColorHexa_A) {

        this.game.ctx2.globalCompositeOperation = "source-over";
        this.game.ctx2.fillStyle = darknessColorHexa_RG + '20' + darknessColorHexa_A;
        this.game.ctx2.fillRect(0, 0, this.game.canvas.width, this.game.canvas.height);
    }

    

    cutDarkness = (x,y, darknessColorHexa_RGBA, darknessAlphaByte, intensity) => {
        if (darknessAlphaByte <= 8) return
        this.game.ctx2.globalCompositeOperation = "destination-out";

        const gradient = this.game.ctx2.createRadialGradient(x, y, 0, x, y, 200*intensity +  Math.random()*(20*intensity));
        gradient.addColorStop(0.1, darknessColorHexa_RGBA);
        // gradient.addColorStop(0.4 + Math.random()*0.1, `#00000060` );
        gradient.addColorStop(0.6 + Math.random()*0.1, `#00000030` );
        // gradient.addColorStop(0.95, `#00000000` );
        gradient.addColorStop(1, `#00000000` );
        this.game.ctx2.fillStyle=gradient;

        this.game.ctx2.beginPath()
        this.game.ctx2.arc(x,y,220*intensity,0, this.endCircleAngle);
        this.game.ctx2.fill();
        this.game.ctx2.closePath()
        this.game.ctx2.globalCompositeOperation = "source-over";
    }
    
    //for these cases, it's required a new calc to apply the light effects reversely to the darkness 
    //means (the more dark, more light -> the less dark, less light)
    //it can make return at the beginning if no factor
    makeLightColorfulAt = (x,y, colorHexa, darknessAlphaByte, intensity) => {
        // return
        //if (darknessAlphaByte <= 8 || this.reducedLightEffects) return
        // const colorLightHexa_A = ((darknessAlphaByte/4)|0).toString(16).padStart(2,'0')
        
        const colorLightHexa_A = ((30 + darknessAlphaByte/8)|0).toString(16).padStart(2,'0')
        
        const gradient = this.game.ctx2.createRadialGradient(x, y, 5*intensity, x, y, 220*intensity + Math.random()*(20*intensity));                
        gradient.addColorStop(0, `${colorHexa}${colorLightHexa_A}`);                        
        gradient.addColorStop(.3, `${colorHexa}${colorLightHexa_A}`);     
        gradient.addColorStop(1, `#00000000` ); 
        this.game.ctx2.fillStyle=gradient;

        this.game.ctx2.beginPath()
        this.game.ctx2.arc(x,y,220*intensity,0,2*Math.PI);
        this.game.ctx2.fill();
        this.game.ctx2.closePath()
    }
}

// window['i'] = 1
// window['j'] = `#00000000`
// window['k'] =.3

export class LightEffect {
    rgbColor = `none`
    intensity = 0
    pos = { x : 0, y : 0}

    constructor(pos, intensity, rgbColor) {        
        this.pos = pos
        this.intensity = intensity
        this.rgbColor = rgbColor
    }
}
