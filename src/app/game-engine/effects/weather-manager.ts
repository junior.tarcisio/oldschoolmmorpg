import { Game } from "../game";
import { MagicEffect } from "./magic-effect";
import { GameConstants } from "../game-constants";

export class WeatherManager {
    game : Game

    rain = true
    readonly MAX_RAIN_ANGLE = 30
    rainAngleX = 0

    thunderRunning :boolean = false
    thunderOpacity :number = 0
    thunderOpacityReverse : boolean = false
    thunderCount = 0;
    thunderLastRun = Date.now()

    /*
     * these weather/night managers can become plugins?
     * I think the thunder is more dependent of and less important than the night system
     * so, how to invert the dependency?
     *      -> add some variable to inject the value?
     *      -> keep like this for now
     */

    constructor(game:Game){
        this.game = game
    }

    draw() {
        
        this.rainAngleX +=  Math.random() - 0.51 
        if (this.rainAngleX > this.MAX_RAIN_ANGLE)
            this.rainAngleX = this.MAX_RAIN_ANGLE
        else if (this.rainAngleX < this.MAX_RAIN_ANGLE*-1)
            this.rainAngleX = this.MAX_RAIN_ANGLE*-1

        if (this.rainAngleX <= 0) {
            this.game.audioManager.SOUNDS.STORM.setVolume(0)
            this.thunderOpacity = 0
            return 
        } else {
            this.game.audioManager.SOUNDS.STORM.playLoop()

            const percentualVolume = (this.rainAngleX*1.5 / this.MAX_RAIN_ANGLE) 
            this.game.audioManager.SOUNDS.STORM.setVolume(percentualVolume)
        }

        for (let y = 0; y < 9; y++) {
            for (let x = 0; x < 11; x++) {

                //rain
                const randomResult = Math.random()
                if (this.rain && randomResult > 0.95) {
                    this.game.ctx.beginPath();
                    this.game.ctx.lineWidth = 3 
                    this.game.ctx.strokeStyle = "#EEEEFFA0"   
                    this.game.ctx.moveTo(GameConstants.spriteCalculatedSize * x + 22,GameConstants.spriteCalculatedSize * y + 22)
                    this.game.ctx.lineTo(GameConstants.spriteCalculatedSize * x + 22 + this.rainAngleX, GameConstants.spriteCalculatedSize * y + 22 + this.rainAngleX*2)
                    this.game.ctx.stroke()
                }

                if (randomResult > 0.999 && this.rainAngleX > 10) {
                    this.game.addComponent(new MagicEffect({x:this.game.player.pos.x - 5 + x,y:this.game.player.pos.y - 4 + y}, this.game, 3)) 
                }
            }            
        }

        this.game.ctx.closePath()
        this.drawThunder()
    }  

    drawThunder() {
        if (!this.thunderRunning) {
                        
            //Tempest audio volume follows the rain intensity
            //this.game.audioManager.SOUNDS.STORM.setVolume((this.rainAngleX/this.MAX_RAIN_ANGLE)*0.85)

            // the stronger the rain, the more frequent is the rain
            // before 0.95
            // 30/500 = 0,06 -> 939
            // 30/1000 = 0,03 -> 0,969
            // 20/1000 = 0,02 -> 0,979

            if (Math.random() > (0.999 - this.rainAngleX/500) && Date.now() - this.thunderLastRun > 4000) {
                this.thunderRunning = true
                this.thunderLastRun = Date.now()

                const randomResult = Math.random()
                const randomVolume = 0.7 + Math.random()*0.3
                if (randomResult < 0.333)
                    this.game.audioManager.SOUNDS.THUNDER_1.play(randomVolume)
                else if (randomResult < 0.500) //reduced change/ annoying
                    this.game.audioManager.SOUNDS.THUNDER_2.play(randomVolume)
                else 
                    this.game.audioManager.SOUNDS.THUNDER_3.play(randomVolume)
            }

            return
        } 

        if (!this.thunderOpacityReverse) {
            this.thunderOpacity += 10

            if (this.thunderOpacity >= 100) {
                this.thunderOpacity = 100
                this.thunderOpacityReverse = true
            }
        } else {
            this.thunderOpacity -= 10

            if (this.thunderOpacity == 60) {
                if (Math.random() > 0.5 && this.thunderCount < 3)  {
                    this.thunderCount++
                    this.thunderOpacityReverse = false
                }
            }

            if (this.thunderOpacity <= 0) {
                this.thunderRunning = false       
                this.thunderOpacityReverse = false
                this.thunderOpacity = 0
                this.thunderCount = 0         
                return        
            }
        }

        this.game.ctx2.globalCompositeOperation='lighter';
        this.game.ctx2.fillStyle = "#dafdff" + this.thunderOpacity.toString(16).padStart(2, '0')
        this.game.ctx2.fillRect(
            0,
            0,
            this.game.canvas.width,
            this.game.canvas.height)
        this.game.ctx2.globalCompositeOperation='source-over';
    }
}

