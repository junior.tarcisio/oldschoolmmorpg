import { Game } from "../game";
import { GameConstants } from "../game-constants";
import { DamageType } from "../enumerables/damage-type";

export class DamageLabel {
    game : Game
    pos : any
    alive : boolean
    message : number
    createdAt : number
    type : DamageType

    constructor(position, message : any, game : Game, type : DamageType = DamageType.PHYSICAL, public posYmodifier = 0, public lifeTime = 1500) {
        this.message = message
        this.createdAt = game.frame.time
        this.alive = true
        this.pos = position
        this.type = type

        console.log(`test2: ${this.type}`)
        // if (type == 0)  //red
        //     this.color = '#f11';
        // else if (type == 1) //light blue
        //     this.color = '#5EF';
        // else if (type == 2) // white
        //     this.color = '#FFF';
        // else  //gray
        //     this.color = '#999';
    }

    draw(game: Game, tileDrawPosX:number, tileDrawPosY:number, currentFrameTime:number) {

        const elapsed = currentFrameTime - this.createdAt 

        if (elapsed > this.lifeTime) {
            this.alive = false
            return
        }

        //center horizontaly
        const adjustedPositionX = tileDrawPosX + 
            GameConstants.spriteCalculatedSize / 2

        //1/3 up + animation
        const adjustedPositionY = tileDrawPosY + 
            (GameConstants.spriteCalculatedSize* -1 / 3) - (elapsed/32) - this.posYmodifier

        //FillText
        game.ctx2.textAlign = "center"
        game.ctx2.font = "bold 24px Calibri"
        game.ctx2.fillStyle = this.type
        game.ctx2.fillText(this.message.toString(), adjustedPositionX, adjustedPositionY)

        //Stroke black border
        game.ctx2.strokeStyle = '#000'
        game.ctx2.lineWidth = 1
        game.ctx2.strokeText(this.message.toString(), adjustedPositionX, adjustedPositionY)
    }
}
