import { LightColorRGB } from '../enumerables/rgb-color';
import { Game } from "../game";
import { GameConstants } from '../game-constants';
import meSpriteTable from './magic-effect';
import { Direction } from '../enumerables/direction';

export class ShootEffectDefinition {
    frames: Array<number>
    light : any = null
    opacity : number

    constructor(frames: Array<number>, light : { level : number, color : LightColorRGB } = null, opacity : number = 1) {
        this.frames = frames
        this.light = light
        this.opacity = opacity
    }
}

export class ShootEffect {
    static readonly  EFFECT_DURATION_MS = 300
    
    public pos = { x: 0, y: 0 }
    public posTo = { x: 0, y: 0 }
    public alive : boolean
    public creationTime :number 
    public definition : ShootEffectDefinition
    public direction : Direction

    constructor(game: Game, positionFrom: any, positionTo : any, definitionID: number = 0) {
        this.pos.x = positionFrom.x
        this.pos.y = positionFrom.y
        this.posTo.x = positionTo.x
        this.posTo.y = positionTo.y
        this.creationTime = game.frame.time
        this.alive = true
        this.definition = shootEffectsDefinition[definitionID]

        if (this.definition.frames.length >= 4)
            this.direction = this.getDirection()
    }

    getDirection() : Direction {
        let distX = Math.abs(this.pos.x - this.posTo.x)
        let distY = Math.abs(this.pos.y - this.posTo.y)

        if (distY > distX)
            return this.pos.y > this.posTo.y? Direction.UP:Direction.DOWN
        else 
            return this.pos.x > this.posTo.x? Direction.LEFT:Direction.RIGHT
    }

    getFrame() : number {
        if (!this.direction) return 0
        return this.direction - 1 // 0 left, 1 up, 2 right, 3 down...
    }

    draw(game: Game, tileDrawPosX:number, tileDrawPosY:number, currentFrameTime:number) {

        const progress = (currentFrameTime - this.creationTime) / ShootEffect.EFFECT_DURATION_MS

        if (progress >= 1) {
            this.alive = false   
            return
        }

        const drawX = tileDrawPosX + ((this.posTo.x - this.pos.x) * GameConstants.spriteCalculatedSize * progress)
        const drawY = tileDrawPosY + ((this.posTo.y - this.pos.y) * GameConstants.spriteCalculatedSize * progress)
        
        const currentFrame : number = this.getFrame()

        game.ctx.globalAlpha = this.definition.opacity    
        game.ctx.drawImage(meSpriteTable,
            0,
            this.definition.frames[currentFrame] * GameConstants.SPRITE_REAL_RESOLUTION, 
            GameConstants.SPRITE_REAL_RESOLUTION,
            GameConstants.SPRITE_REAL_RESOLUTION,
            drawX,
            drawY,
            GameConstants.spriteCalculatedSize,
            GameConstants.spriteCalculatedSize)
        game.ctx.globalAlpha = 1

        if (this.definition.light) 
            game.nightEffectManager.addLightEffect({ x: drawX, y: drawY}, this.definition.light.level, this.definition.light.color)        
    }
    
    static readonly E_FIRE = 0
    static readonly E_ENERGY = 1
    static readonly E_ARROW = 2
}

const shootEffectsDefinition = new Array<ShootEffectDefinition>()
shootEffectsDefinition[ShootEffect.E_FIRE] = new ShootEffectDefinition([96], { level: 0.5, color: LightColorRGB.RED}, 0.8)
shootEffectsDefinition[ShootEffect.E_ENERGY] = new ShootEffectDefinition([31], { level: 0.5, color: LightColorRGB.BLUE}, 0.8)
shootEffectsDefinition[ShootEffect.E_ARROW] = new ShootEffectDefinition([106,107,108,109])