import { LightColorRGB } from './../enumerables/rgb-color';
import { Game } from "../game";
import { GameConstants } from '../game-constants';

export class MagicEffectDefinition {
    frames: Array<number>
    light : any = null
    opacity : number 

    constructor(frames: Array<number>, light : { level : number, color : LightColorRGB } = null, opacity : number = 1) {
        this.frames = frames
        this.light = light
        this.opacity = opacity
    }
}

export class MagicEffect {
    public static readonly FRAME_DURATION_MS = 30
    public pos = { x: 0, y: 0 }
    public alive : boolean
    public creationTime :number 
    public definition : MagicEffectDefinition

    constructor(position: any, game: Game, definitionID: number = 0) {
        this.pos = position
        this.creationTime = game.frame.time
        this.alive = true
        this.definition = magicEffectsDefinition[definitionID]
    }

    draw(game: Game, tileDrawPosX:number, tileDrawPosY:number, currentFrameTime:number) {
        if (currentFrameTime - this.creationTime >= this.definition.frames.length * MagicEffect.FRAME_DURATION_MS) {
            this.alive = false   
            return
        }

        const drawX = tileDrawPosX //- game.drawMovingModifierX
        const drawY = tileDrawPosY //- game.drawMovingModifierY
        
        const elapsedTime = currentFrameTime - this.creationTime 
        const currentFrame = elapsedTime / MagicEffect.FRAME_DURATION_MS | 0

        game.ctx.globalAlpha = this.definition.opacity        
        game.ctx.drawImage(meSpriteTable,
            0,
            this.definition.frames[currentFrame] * GameConstants.SPRITE_REAL_RESOLUTION, 
            GameConstants.SPRITE_REAL_RESOLUTION,
            GameConstants.SPRITE_REAL_RESOLUTION,
            drawX,
            drawY,
            GameConstants.spriteCalculatedSize,
            GameConstants.spriteCalculatedSize)
        game.ctx.globalAlpha = 1

        if (this.definition.light) {            
            //15 - 7.5 = 7.5
            //0 - 7.5  =-> 7.5 
            //7 - 7.5 = 0.5
            //8 - 7.5 = 1.5
            //6 - 7.5 = 1.5
            //7.5 / 7.5 * 10
            const meProgress = (elapsedTime / (this.definition.frames.length * MagicEffect.FRAME_DURATION_MS)) //from 0 to 1
            const lightIntensity = Math.abs(meProgress - 0.5)*2  // from -0.5 passing through 0 to 0.5  ==>  1 - 0 - 1
            const reverseLightIntensity = 1 - lightIntensity // 0 - 1 - 0

            //console.log(lightIntensity//)
            game.nightEffectManager.addLightEffect({ x: drawX, y: drawY}, reverseLightIntensity * this.definition.light.level, this.definition.light.color)
        }
    }
    
    static readonly E_BLOOD = 0
    static readonly E_HEAL = 1
    static readonly E_EXPLOSION = 2
    static readonly E_WATER_DROP = 3
    static readonly E_ENERGY = 4
    static readonly E_SLASH = 5
    static readonly E_BITE = 6
    static readonly E_SCRATCH = 7
    static readonly E_DIE_BLOOD = 8
    static readonly E_SPARK_ARMOR = 9
    static readonly E_SMOKE = 10
    static readonly E_SMOKE_PUFT = 11
    static readonly E_SMOKE_SMALL = 12
    static readonly E_SMOKE_PUFT_SMALL = 13
    static readonly E_FIRE_STRIKE = 14
    static readonly E_SHADOW = 15
}

const meSpriteTable = new Image;
meSpriteTable.src = "assets/meSpriteTable.png";

export default meSpriteTable

const magicEffectsDefinition = new Array<MagicEffectDefinition>()
magicEffectsDefinition[MagicEffect.E_BLOOD] = new MagicEffectDefinition([5,4,3,2,1,0,1,2,3,4,5])
magicEffectsDefinition[MagicEffect.E_HEAL] = new MagicEffectDefinition([6,7,8,9,10,9,8,9,10,11], {level : 0.75, color : LightColorRGB.BLUE})
magicEffectsDefinition[MagicEffect.E_EXPLOSION] = new MagicEffectDefinition([12, 13, 14, 15, 16, 17, 18, 17, 16, 17, 18, 18, 17, 16, 17, 18, 18, 17, 16, 15, 14, 13, 12], { level : 0.75, color : LightColorRGB.RED}, 0.75 )
magicEffectsDefinition[MagicEffect.E_WATER_DROP] = new MagicEffectDefinition([19,20,21,22,23,24,25,26])
magicEffectsDefinition[MagicEffect.E_ENERGY] = new MagicEffectDefinition([27,28,29,30,31,32,33,34,35,36,37,38,39,38,33,32,31,30,29,28,27], {level : 0.65, color : LightColorRGB.BLUE}, 0.75)
magicEffectsDefinition[MagicEffect.E_SLASH] = new MagicEffectDefinition([40,40,41,41,42,42,43,43,44,44])
magicEffectsDefinition[MagicEffect.E_BITE] = new MagicEffectDefinition([45,45,46,46,47,47,48,48,49,49,50,50,51,51,52,52])
magicEffectsDefinition[MagicEffect.E_SCRATCH] = new MagicEffectDefinition([53,54,55,56,57,58,59,60,61])
magicEffectsDefinition[MagicEffect.E_DIE_BLOOD] = new MagicEffectDefinition([62,63,64,65,66,67,68,69,70,71,72,73,74])
magicEffectsDefinition[MagicEffect.E_SPARK_ARMOR] = new MagicEffectDefinition([76,77,78,79,80,81,82,83,84])
magicEffectsDefinition[MagicEffect.E_SMOKE] = new MagicEffectDefinition([85,86,87,88,89,90,91,92,93, 94, 95, 94, 93, 92,91,90,89,88,87, 86,85])
magicEffectsDefinition[MagicEffect.E_SMOKE_PUFT] = new MagicEffectDefinition([86,88,90,92,93, 94, 95, 94, 93,  92, 91,90,89,88,87, 86,85], null, 0.65)
magicEffectsDefinition[MagicEffect.E_SMOKE_SMALL] = new MagicEffectDefinition([85,86,87,88,89,90,91,90,89,88,87,86,85])
magicEffectsDefinition[MagicEffect.E_SMOKE_PUFT_SMALL] = new MagicEffectDefinition([90,91,90,89,88,87,86,85])
magicEffectsDefinition[MagicEffect.E_FIRE_STRIKE] = new MagicEffectDefinition([96,96,97,97,98,98,99,99,100,100,99,98,97,96,101,101,102,,102,103,102,104,104,105,105], { level : 0.75, color : LightColorRGB.RED}, 0.9 )
magicEffectsDefinition[MagicEffect.E_SHADOW] = new MagicEffectDefinition([110,110,111,111,112,112,113,113,114,114,115,115,116,116], null, 0.75 )