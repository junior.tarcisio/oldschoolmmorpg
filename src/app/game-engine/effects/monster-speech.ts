import { Game } from "../game";
import { Thing } from "../things/thing";
import { GameConstants } from "../game-constants";

export class MonsterSpeech {
    pos : any
    alive : boolean
    msg : string
    createdAt : number

    constructor(position, msg, game:Game) {
        this.pos = position
        this.msg = msg
        this.alive = true
        this.createdAt = game.frame.time
    }

    draw(game: Game, tileDrawPosX:number, tileDrawPosY:number, currentFrameTime:number) {
        
        if (currentFrameTime - this.createdAt > 5000)
            this.alive = false;
            
        //center horizontaly
        const adjustedPositionX = tileDrawPosX + 
        GameConstants.spriteCalculatedSize / 2

        //center up
        const adjustedPositionY = tileDrawPosY + 
            GameConstants.spriteCalculatedSize* -1 / 2
            
        game.ctx2.textAlign="center";
        game.ctx2.font = "bold 18px Calibri";
        game.ctx2.fillStyle = '#f60';
        game.ctx2.strokeStyle = '#000';
        game.ctx2.lineWidth = 1;
        game.ctx2.fillText(this.msg, adjustedPositionX, adjustedPositionY)            
        game.ctx2.strokeText(this.msg, adjustedPositionX, adjustedPositionY)
    }
}
