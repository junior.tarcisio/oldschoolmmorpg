import { Direction } from "./enumerables/direction";
import { MapTile } from "./map/map";

export default class Position {
    public x : number = 0;
    public y : number = 0;

    constructor(x : number, y : number) {
        this.x = x;
        this.y = y;
    }

    static getSubjacentSQM(map, pos, direction : Direction) : MapTile {

        switch (direction) {
            case (Direction.LEFT):
                if (map[pos.y] != null && map[pos.y][pos.x - 1]  != null)
                    return map[pos.y][pos.x - 1]    
                break            
            case (Direction.RIGHT):
                if(map[pos.y] != null && map[pos.y][pos.x + 1] != null)
                    return map[pos.y][pos.x + 1]
                break
            case (Direction.UP):
                if (map[pos.y - 1] != null && map[pos.y - 1][pos.x] != null)
                    return map[pos.y - 1][pos.x]
                break
            case (Direction.DOWN):
                if (map[pos.y + 1] != null && map[pos.y + 1][pos.x] != null)
                    return map[pos.y + 1][pos.x]
                break
        }

        return null
    }

    static getSubjacentPOS(pos, direction : Direction) {
        switch (direction) {
            case (Direction.LEFT):
                return new Position(pos.x - 1, pos.y)
            case (Direction.RIGHT):
                return new Position(pos.x + 1, pos.y)
            case (Direction.UP):
                return new Position(pos.x, pos.y - 1)
            case (Direction.DOWN):
                return new Position(pos.x, pos.y + 1)
        }
    }
}