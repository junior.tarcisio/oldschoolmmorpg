import { MovementType } from './things/monsters/monster-definition';
import tiles from './map/tiles';
import { Game } from './game';
import { MagicEffect } from './effects/magic-effect';

export class PathFind {
    
    readonly RELATIVE_POS = [{x:1,y:0},{x:-1,y:0},{x:0,y:1},{x:0,y:-1}]

    constructor(private game : Game) {        
    }

    //add to the list using binary search
    addArraySortedByF(array, value) {
        var low = 0,
            high = array.length
    
        while (low < high) {
            var mid = (low + high) >>> 1
            if (array[mid].f < value.f) low = mid + 1
            else high = mid
        }

        array.splice(low, 0, value);
    }

    findArray(start, end, max_tries = 30, range = 0, mov_type : MovementType = MovementType.WALK) {
        let nodeResult 

        if (mov_type == MovementType.WALK)
            nodeResult = this.find(start, end, max_tries, range)  
        else
            nodeResult = this.findLikeGhost(start, end, max_tries, range)  

        const path = new Array

        if (nodeResult == null)
            return path
                
        path.push(nodeResult)
        //this.game.addComponent(new MagicEffect({x:nodeResult.x,y:nodeResult.y}, this.game, 0)) 

        while(nodeResult.parent) {
            nodeResult = nodeResult.parent
            //this.game.addComponent(new MagicEffect({x:nodeResult.x,y:nodeResult.y}, this.game, 0)) 
            path.push(nodeResult)
        }

        return path
    }

    find(start, end, max_tries = 30, range = 0) : any {        
        const mathabsref = Math.abs
        let tries = 0
        
        start.g = 0
        start.f = 0
        start.h = 0
        
        end.g = 0
        end.f = 0
        end.h = 0
        
        if(tiles[this.game.map[end.y][end.x].tile].blockable)
            return []

        let closedList : any[][] = [[]]
        //mapped by x,y 

        let openList = [] 
        //ordered by algorithm addArraySortedByF

        openList.push(start)

        while(openList.length > 0 && tries < max_tries) {
            const nodeSmallestCost = openList.shift()

            //add closedlist
            if (!closedList[nodeSmallestCost.y])
                closedList[nodeSmallestCost.y] = []
            closedList[nodeSmallestCost.y][nodeSmallestCost.x] = nodeSmallestCost

            //this.game.addComponent(new MagicEffect({x:nodeSmallestCost.x,y:nodeSmallestCost.y}, this.game, 1)) 
            
            if (range == 0) {
                if (nodeSmallestCost.x == end.x && 
                    nodeSmallestCost.y == end.y) {
                    return nodeSmallestCost
                }
            } else {
                if (mathabsref(nodeSmallestCost.x - end.x) <= range && 
                    mathabsref(nodeSmallestCost.y - end.y) <= range) {
                    return nodeSmallestCost
                }
            }
            
            for (let i = 0; i < this.RELATIVE_POS.length; i++) {
                const relative = {
                    y : nodeSmallestCost.y + this.RELATIVE_POS[i].y,
                    x : nodeSmallestCost.x + this.RELATIVE_POS[i].x,
                    g : 0,
                    h : 0,
                    f : 0,
                    parent : null
                }
                
                if (closedList[relative.y] &&
                    closedList[relative.y][relative.x])
                    continue

                if (!this.game.map[relative.y] || !this.game.map[relative.y][relative.x])
                    continue
                    
                if (this.game.map[relative.y][relative.x].movementBlocked() && ! (relative.x == end.x && relative.y == end.y))
                    continue

                relative.g = (nodeSmallestCost.g + 1 ) //- (1* this.game.map[relative.y][relative.x].getTile().speed)
                relative.h = (Math.abs(relative.x - end.x)**2) + (Math.abs(relative.y - end.y)**2)
                relative.f = relative.g + relative.h
                relative.parent = nodeSmallestCost

                //TODO: heavy find, faster to let add duplicated nodes than making this search
                //if (openList.find(x=> x.x == relative.x && x.y == relative.y && x.g < relative.g) == null)
                    this.addArraySortedByF(openList, relative)            
            }

            tries++
        }
        
        return null
    }
    
    findLikeGhost(start, end, max_tries = 30, range = 0) : any {        
        const mathabsref = Math.abs
        let tries = 0
        
        start.g = 0
        start.f = 0
        start.h = 0
        
        end.g = 0
        end.f = 0
        end.h = 0
        
        if(tiles[this.game.map[end.y][end.x].tile].blockable)
            return []

        let closedList : any[][] = [[]]
        //mapped by x,y 

        let openList = [] 
        //ordered by algorithm addArraySortedByF

        openList.push(start)

        while(openList.length > 0 && tries < max_tries) {
            const nodeSmallestCost = openList.shift()

            //add closedlist
            if (!closedList[nodeSmallestCost.y])
                closedList[nodeSmallestCost.y] = []
            closedList[nodeSmallestCost.y][nodeSmallestCost.x] = nodeSmallestCost

            //this.game.addComponent(new MagicEffect({x:nodeSmallestCost.x,y:nodeSmallestCost.y}, this.game, 1)) 
            
            if (range == 0) {
                if (nodeSmallestCost.x == end.x && 
                    nodeSmallestCost.y == end.y) {
                    return nodeSmallestCost
                }
            } else {
                if (mathabsref(nodeSmallestCost.x - end.x) <= range && 
                    mathabsref(nodeSmallestCost.y - end.y) <= range) {
                    return nodeSmallestCost
                }
            }
            
            for (let i = 0; i < this.RELATIVE_POS.length; i++) {
                const relative = {
                    y : nodeSmallestCost.y + this.RELATIVE_POS[i].y,
                    x : nodeSmallestCost.x + this.RELATIVE_POS[i].x,
                    g : 0,
                    h : 0,
                    f : 0,
                    parent : null
                }
                
                if (closedList[relative.y] &&
                    closedList[relative.y][relative.x])
                    continue

                if (!this.game.map[relative.y] || !this.game.map[relative.y][relative.x])
                    continue
                    
                if (this.game.map[relative.y][relative.x].hasCreature() && ! (relative.x == end.x && relative.y == end.y))
                    continue

                relative.g = (nodeSmallestCost.g + 1)
                relative.h = (Math.abs(relative.x - end.x)**2) + (Math.abs(relative.y - end.y)**2)
                relative.f = relative.g + relative.h
                relative.parent = nodeSmallestCost

                //TODO: heavy find, faster to let add duplicated nodes than making this search
                //if (openList.find(x=> x.x == relative.x && x.y == relative.y && x.g < relative.g) == null)
                    this.addArraySortedByF(openList, relative)            
            }

            tries++
        }
        
        return null
    }
}


