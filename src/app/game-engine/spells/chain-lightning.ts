import { ShootEffect } from './../effects/shoot-effect';
import { MagicEffect } from '../effects/magic-effect';
import { Creature } from '../things/creature'
import { Spell } from './spell';
import Player from '../things/player';
import { DamageType } from '../enumerables/damage-type';

export class SpellChainLightning extends Spell {
    constructor() {
        super({
            name: 'Chain lightning',
            mana: 80,
            level: 0,
            vocation: 0,
            ico: '⚡'
        })
    }
    
    static readonly area  = [ 
                                                              
                                                              
                                                {x:-1, y:-3}, {x:0, y:-3}, {x:1, y:-3},  
                                   {x:-2, y:-2},{x:-1, y:-2}, {x:0, y:-2}, {x:1, y:-2}, {x:2, y:-2}, 
                      {x:-3, y:-1},{x:-2, y:-1},{x:-1, y:-1}, {x:0, y:-1}, {x:1, y:-1}, {x:2, y:-1}, {x:3, y:-1},  
                         {x:-3, y:0},{x:-2, y:0},{x:-1, y:0}, {x:0, y:0},  {x:1, y: 0}, {x:2, y: 0}, {x:3, y: 0}, 
                        {x:-3, y:1},{x:-2, y:1},{x:-1, y:1},  {x:0, y:1},  {x:1, y:1},  {x:2, y:1},  {x:3, y:1},
                                    {x:-2, y:2},{x:-1, y:2},  {x:0, y:2},  {x:1, y:2},  {x:2, y:2},  
                                                {x:-1, y:3},  {x:0, y:3},  {x:1, y:3},  
                                                              
                                                              
    ]
    
    public getSpellFormulaPlayer(player: Player) {
        return {
            min: 70,
            max: 180
        }
    }

    public static createFormula(min: number, max: number) {
        return {
            min: min,
            max: max
        }
    }

    public cast(caster : Creature, target : Creature, formula: any) : boolean {

        if (!caster.target) {
            caster.game.addComponent(new MagicEffect(caster.pos, caster.game, MagicEffect.E_SMOKE_PUFT))
            return false
        }
        
        if (caster.distanceForShootTo(caster.target.pos) > 4) {
            caster.game.addComponent(new MagicEffect(caster.pos, caster.game, MagicEffect.E_SMOKE_PUFT))
            return false
        }

        const damage = formula.min + ((formula.max - formula.min) * Math.random()) | 0

        for (let index = 0; index < SpellChainLightning.area.length; index++) {
            const area = SpellChainLightning.area[index];
            
            const posSqmAffected = {
                x: caster.target.pos.x + area.x,
                y: caster.target.pos.y + area.y
            }

            if (!(caster.game.map[posSqmAffected.y] && caster.game.map[posSqmAffected.y][posSqmAffected.x]))
                continue 

            const creatureList = caster.game.map[posSqmAffected.y][posSqmAffected.x].creatureList

            if (creatureList && creatureList.length > 0) {
                let hasEnemy = false
                    
                for (let j = creatureList.length - 1; j >= 0; j--) {
                    if (creatureList[j] != caster) {
                        creatureList[j].receiveDamage(caster,damage, DamageType.ENERGY)
                        hasEnemy = true
                    }
                }

                if (hasEnemy) {
                    caster.game.addComponent(new MagicEffect(posSqmAffected, caster.game, 4)) 
                    caster.game.addComponent(new ShootEffect(caster.game, caster.pos, posSqmAffected, ShootEffect.E_ENERGY)) 
                }
            }
            
        }

        caster.game.audioManager.SOUNDS.MAGIC_ENERGY_02.play()
        return true
    }
}