import { MagicEffect } from '../effects/magic-effect';
import { Creature } from '../things/creature'
import { Spell } from './spell';
import Player from '../things/player';
import { DamageType } from '../enumerables/damage-type';

export class SpellWhirlWind extends Spell {

    constructor() {
        super({
            name: 'Whirl Wind',
            mana: 100,
            level: 0,
            vocation: 0,
            ico: '👊'
        })
    }

    static readonly area = [
        { x: -1, y: -1 }, { x: 0, y: -1 }, { x: 1, y: -1 },
        { x: -1, y: 0 }, { x: 1, y: 0 },
        { x: -1, y: 1 }, { x: 0, y: 1 }, { x: 1, y: 1 },
    ]

    public getSpellFormulaPlayer(player: Player) {
        return {
            min: player.getAttackDamage(),
            max: player.getAttackDamage() * 2
        }
    }

    public static createFormula(min: number, max: number) {
        return {
            min: min,
            max: max
        }
    }

    cast(caster: Creature, target: Creature, formula: any): boolean {

        const damage = formula.min + ((formula.max - formula.min) * Math.random()) | 0

        for (let i = 0; i < SpellWhirlWind.area.length; i++) {
            const area = SpellWhirlWind.area[i];
            const posSqmAffected = {
                x: caster.pos.x + area.x,
                y: caster.pos.y + area.y
            }

            if (!(caster.game.map[posSqmAffected.y] && caster.game.map[posSqmAffected.y][posSqmAffected.x]))
                continue 
                
            caster.game.addComponent(new MagicEffect(posSqmAffected, caster.game, 7))
            
            const creatureList = caster.game.map[posSqmAffected.y][posSqmAffected.x].creatureList

            if (creatureList && creatureList.length > 0) {
                for (let j = creatureList.length - 1; j >= 0; j--) 
                    creatureList[j].receiveDamage(caster,damage, DamageType.DEATH)
            }
        }
        caster.game.audioManager.SOUNDS.HIT_2.play()

        return true
    }
}