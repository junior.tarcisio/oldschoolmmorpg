import { ShootEffect } from './../effects/shoot-effect';
import { MagicEffect } from '../effects/magic-effect';
import { Creature } from '../things/creature'
import { Spell } from './spell';
import Player from '../things/player';
import { DamageType } from '../enumerables/damage-type';

export class SpellEnergyStrike extends Spell {
    constructor() {
        super({
            name: 'Energy Strike',
            mana: 20,
            level: 0,
            vocation: 0,
            ico: '⚡'
        })
    }
        
    public getSpellFormulaPlayer(player: Player) {
        return {
            min: 50,
            max: 120
        }
    }

    public static createFormula(min: number, max: number) {
        return {
            min: min,
            max: max
        }
    }

    public cast(caster : Creature, target : Creature, formula: any) : boolean {

        if (!caster.target) {
            caster.game.addComponent(new MagicEffect(caster.pos, caster.game, MagicEffect.E_SMOKE_PUFT))
            return false
        }
        
        if (caster.distanceForShootTo(caster.target.pos) > 4) {
            caster.game.addComponent(new MagicEffect(caster.pos, caster.game, MagicEffect.E_SMOKE_PUFT))
            return false
        }

        const damage = formula.min + ((formula.max - formula.min) * Math.random()) | 0

        caster.target.receiveDamage(caster,damage, DamageType.ENERGY)
        caster.game.addComponent(new MagicEffect(caster.target.pos, caster.game, 4)) 
        caster.game.addComponent(new ShootEffect(caster.game, caster.pos, caster.target.pos, ShootEffect.E_ENERGY)) 

        caster.game.audioManager.SOUNDS.MAGIC_ENERGY_02.play()
        return true
    }
}