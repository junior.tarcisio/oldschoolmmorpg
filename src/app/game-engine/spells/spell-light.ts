import { LightColorRGB } from './../enumerables/rgb-color';
import { ConditionType } from '../conditions/conditions-definition';
import { MagicEffect } from '../effects/magic-effect';
import { Creature } from '../things/creature'
import { Spell } from './spell';
import Player from '../things/player';

export class SpellLight extends Spell {
    constructor() {
        super({
            name: 'Haste',
            mana: 25,
            level: 0,
            vocation: 0,
            ico: '💡'
        })
    }
    
    public getSpellFormulaPlayer(player: Player) {
        return SpellLight.createFormula(1.5, LightColorRGB.BLUE, 120000)
    }

    public cast(caster : Creature, target : Creature, formula : any) : boolean {
        
        caster.addCondition(ConditionType.LIGHT, { intensity: formula.intensity, color: formula.color, duration: formula.duration})
        caster.game.addComponent(new MagicEffect(target.pos, caster.game, 1))
        caster.game.audioManager.SOUNDS.MAGIC_6.play()
        // caster.game.audioManager.SOUNDS.SPELL_LIGH_TEST.play()

        return true
    }

    public static createFormula(intensity:number, color:LightColorRGB, duration:number) {
        return {
            intensity : intensity,
            color : color,
            duration : duration
        }
    }
}