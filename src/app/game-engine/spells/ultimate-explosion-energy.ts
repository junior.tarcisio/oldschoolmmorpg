import { MagicEffect } from '../effects/magic-effect';
import { Creature } from '../things/creature'
import { Spell } from './spell';
import Player from '../things/player';
import { DamageType } from '../enumerables/damage-type';

export class SpellUltimateExplosionEnergy extends Spell {
    constructor() {
        super({
            name: 'Ultimate energy explosion',
            mana: 1000,
            level: 0,
            vocation: 0,
            ico: '⚡'
        })
    }
    
    static readonly area  = [ 
                                                              {x:0, y:-5}, 
                                  {x:-2, y:-4}, {x:-1, y:-4}, {x:0, y:-4}, {x:1, y:-4}, {x:2, y:-4},
                    {x:-3, y:-3}, {x:-2, y:-3}, {x:-1, y:-3}, {x:0, y:-3}, {x:1, y:-3}, {x:2, y:-3}, {x:3, y:-3}, 
         {x:-4, y:-2},{x:-3, y:-2},{x:-2, y:-2},{x:-1, y:-2}, {x:0, y:-2}, {x:1, y:-2}, {x:2, y:-2}, {x:3, y:-2}, {x:4, y:-2},
         {x:-4, y:-1},{x:-3, y:-1},{x:-2, y:-1},{x:-1, y:-1}, {x:0, y:-1}, {x:1, y:-1}, {x:2, y:-1}, {x:3, y:-1}, {x:4, y:-1}, 
{x:-5, y:0},{x:-4, y:0},{x:-3, y:0},{x:-2, y:0},{x:-1, y:0},               {x:1, y: 0}, {x:2, y: 0}, {x:3, y: 0}, {x:4, y: 0}, {x:5, y: 0},
            {x:-4, y:1},{x:-3, y:1},{x:-2, y:1},{x:-1, y:1},  {x:0, y:1}, {x:1, y:1}, {x:2, y:1},{x:3, y:1},{x:4, y:1},  
            {x:-4, y:2},{x:-3, y:2},{x:-2, y:2},{x:-1, y:2},  {x:0, y:2}, {x:1, y:2}, {x:2, y:2},{x:3, y:2},{x:4, y:2},
                      {x:-3, y:3}, {x:-2, y:3}, {x:-1, y:3},  {x:0, y:3}, {x:1, y:3},{x:2, y:3}, {x:3, y:3},
                                   {x:-2, y:4}, {x:-1, y:4},  {x:0, y:4}, {x:1, y:4},{x:2, y:4},
                                                              {x:0, y:5}, 
    ]
    
    public getSpellFormulaPlayer(player: Player) {
        return {
            min: 200,
            max: 400
        }
    }

    public static createFormula(min: number, max: number) {
        return {
            min: min,
            max: max
        }
    }

    public cast(caster : Creature, target : Creature, formula: any) : boolean {

        const damage = formula.min + ((formula.max - formula.min) * Math.random()) | 0

        for (let index = 0; index < SpellUltimateExplosionEnergy.area.length; index++) {
            const area = SpellUltimateExplosionEnergy.area[index];
            const posSqmAffected = {
                x: caster.pos.x + area.x,
                y: caster.pos.y + area.y
            }

            if (!(caster.game.map[posSqmAffected.y] && caster.game.map[posSqmAffected.y][posSqmAffected.x]))
                continue 

            caster.game.addComponent(new MagicEffect(posSqmAffected, caster.game, 4)) 

            const creatureList = caster.game.map[posSqmAffected.y][posSqmAffected.x].creatureList

            if (creatureList && creatureList.length > 0) {
                for (let j = creatureList.length - 1; j >= 0; j--) 
                    creatureList[j].receiveDamage(caster,damage, DamageType.ENERGY)
            }
        }

        caster.game.audioManager.SOUNDS.MAGIC_ENERGY_01.play()
        return true
    }
}