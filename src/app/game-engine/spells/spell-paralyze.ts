import { LightColorRGB } from './../enumerables/rgb-color';
import { ConditionType } from '../conditions/conditions-definition';
import { MagicEffect } from '../effects/magic-effect';
import { Creature } from '../things/creature'
import { Spell } from './spell';
import Player from '../things/player';
import { Condition } from '../conditions/condition';

export class SpellParalyze extends Spell {
    constructor() {
        super({
            name: 'Paralyze',
            mana: 25,
            level: 0,
            vocation: 0,
            ico: '💡'
        })
    }
    
    public getSpellFormulaPlayer(player: Player) {
        return SpellParalyze.createFormula(0.333, 4000)
    }

    public cast(caster : Creature, target : Creature, formula : any) : boolean {
        
        if (!caster.target) {
            caster.game.addComponent(new MagicEffect(caster.pos, caster.game, MagicEffect.E_SMOKE_PUFT))
            return false
        }
        
        if (caster.distanceForShootTo(caster.target.pos) > 6) {
            caster.game.addComponent(new MagicEffect(caster.pos, caster.game, MagicEffect.E_SMOKE_PUFT))
            return false
        }

        caster.target.conditions[ConditionType.HASTE] = null
        caster.target.addCondition(ConditionType.PARALYZED, { amount: formula.amount, duration: formula.duration})
        caster.game.addComponent(new MagicEffect(target.pos, caster.game, 1))
        caster.game.audioManager.SOUNDS.MAGIC_6.play()

        return true
    }

    public static createFormula(amount:number, duration:number) {
        return {
            amount : amount,
            duration : duration
        }
    }
}