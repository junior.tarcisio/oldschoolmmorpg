import { ConditionType } from '../conditions/conditions-definition';
import { MagicEffect } from '../effects/magic-effect';
import { Creature } from '../things/creature'
import { Spell } from './spell';
import Player from '../things/player';

export class SpellHaste extends Spell {
    constructor() {
        super({
            name: 'Haste',
            mana: 25,
            level: 0,
            vocation: 0,
            ico: '🏃'
        })
    }
    
    public getSpellFormulaPlayer(player: Player) {
        return { amount: player.speed*0.1 + 10, duration : 60000 }
    }

    public cast(caster: Creature, target: Creature, formula : any): boolean {
        caster.addCondition(ConditionType.HASTE, formula)

        caster.game.addComponent(new MagicEffect(target.pos, caster.game, 1))
        caster.game.audioManager.SOUNDS.MAGIC_6.play()

        return true
    }

    public static createFormula(amount : number, duration: number) {
        return {
            amount : amount,
            duration : duration
        }
    }
}