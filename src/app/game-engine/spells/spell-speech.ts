
import Player from '../things/player';
import { SpellType } from './spells-definition';

export class SpellSpeech  {
    constructor() {}
    
    tryToCast(player : Player, message:string) : boolean {
        let spellType : SpellType

        
        const msg_normalized = message.trim().toLowerCase()

        if (msg_normalized == "exura")
            spellType = SpellType.HEALING
        else if (msg_normalized == "utani hur")
            spellType = SpellType.HASTE
        else if (msg_normalized == "lummos")
            spellType = SpellType.LIGHT
        else if (msg_normalized == "exori")
            spellType = SpellType.WHIRLD_WILD
        else if (msg_normalized == "flammus maxima")
            spellType = SpellType.ULTIMATE_EXPLOSION_FIRE
        else if (msg_normalized == "electrifium maxima")
            spellType = SpellType.ULTIMATE_EXPLOSION_ENERGY
        else if (msg_normalized == "electrifium")
            spellType = SpellType.CHAIN_LIGHTNING           
        else if (msg_normalized == "flammus gran")
            spellType = SpellType.GREAT_FIRE_BALL        
        else if (msg_normalized == "!day")
            player.game.nightEffectManager.dayHour = 8    
             
        if (spellType != null) 
            return player.castSpell(spellType, player)

        return true
    }
}