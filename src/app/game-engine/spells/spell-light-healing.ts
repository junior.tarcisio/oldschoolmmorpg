import { MagicEffect } from '../effects/magic-effect';
import { Creature } from '../things/creature'
import { Spell } from './spell';
import Player from '../things/player';
import { ConditionType } from '../conditions/conditions-definition';

export class SpellLightHealing extends Spell {
    constructor() {
        super({
            name: 'Healing',
            mana: 25,
            level: 0,
            vocation: 0,
            ico: '&#9960;'
        })
    }
    
    public getSpellFormulaPlayer(player: Player) {
        return {
            min : 40,
            max : 60
        }
    }

    public static createFormula(min : number, max: number) {
        return {
            min : min,
            max : max
        }
    }

    public cast(caster : Creature, target : Creature, formula : any) : boolean {
        caster.conditions[ConditionType.PARALYZED] = null 

        const amountHeal = formula.min + ((formula.max - formula.min) * Math.random()) | 0

        target.receiveHealing(amountHeal)  
        caster.game.addComponent(new MagicEffect(target.pos, caster.game, 1))
        caster.game.audioManager.SOUNDS.MAGIC_6.play()

        return true
    }
}