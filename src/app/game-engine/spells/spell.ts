import { Creature } from '../things/creature'
import Player from '../things/player'

export abstract class Spell  {
    name:string = ''
    mana:number = 0
    level:number = 1
    vocation:number = 0
    ico:string = '⭐'

    constructor(props) {
        for(let key in props) {
            this[key] = props[key]
        }
    }
    
    public abstract getSpellFormulaPlayer(player:Player) : any

    public abstract cast(caster : Creature, target : Creature, formula : any);
}