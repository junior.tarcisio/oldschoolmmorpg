import { SpellBasicAttack } from './spell-basic-attack';
import { SpellGreatFireBall } from './great-fire-ball';
import { SpellUltimateExplosionEnergy } from './ultimate-explosion-energy';
import { SpellUltimateExplosion } from './ultimate-explosion';
import { SpellLightHealing } from './spell-light-healing';
import { SpellHaste } from "./spell-haste";
import { Spell } from "./spell";
import { SpellLight } from './spell-light';
import { SpellWhirlWind } from './whirl-wind';
import { SpellChainLightning } from './chain-lightning';
import { SpellEnergyStrike } from './energy-strike';
import { SpellFireStrike } from './fire-strike';
import { SpellParalyze } from './spell-paralyze';
import { SpellArrowsRain } from './arrows-rain';

export enum SpellType {
    BASIC_ATTACK = 0,
    HEALING = 1,
    HASTE = 2,
    LIGHT = 3,
    WHIRLD_WILD = 4,
    FIRE_STRIKE = 5,
    GREAT_FIRE_BALL = 6,
    ULTIMATE_EXPLOSION_FIRE = 7,
    ENERGY_STRIKE = 8,
    CHAIN_LIGHTNING = 9,
    ULTIMATE_EXPLOSION_ENERGY = 10,
    PARALYZE = 11,
    ARROWS_RAIN = 12
}

const spellsDefinition = new Array<Spell>()
spellsDefinition[SpellType.HEALING] = new SpellLightHealing()
spellsDefinition[SpellType.HASTE] = new SpellHaste()
spellsDefinition[SpellType.LIGHT] = new SpellLight()
spellsDefinition[SpellType.WHIRLD_WILD] = new SpellWhirlWind()
spellsDefinition[SpellType.ULTIMATE_EXPLOSION_FIRE] = new SpellUltimateExplosion()
spellsDefinition[SpellType.ULTIMATE_EXPLOSION_ENERGY] = new SpellUltimateExplosionEnergy()
spellsDefinition[SpellType.CHAIN_LIGHTNING] = new SpellChainLightning()
spellsDefinition[SpellType.GREAT_FIRE_BALL] = new SpellGreatFireBall()
spellsDefinition[SpellType.ENERGY_STRIKE] = new SpellEnergyStrike()
spellsDefinition[SpellType.FIRE_STRIKE] = new SpellFireStrike()
spellsDefinition[SpellType.BASIC_ATTACK] = new SpellBasicAttack()
spellsDefinition[SpellType.PARALYZE] = new SpellParalyze()
spellsDefinition[SpellType.ARROWS_RAIN] = new SpellArrowsRain()

export default spellsDefinition