import { DamageType } from './../enumerables/damage-type';
import { ShootEffect } from '../effects/shoot-effect';
import { MagicEffect } from '../effects/magic-effect';
import { Creature } from '../things/creature'
import { Spell } from './spell';
import Player from '../things/player';
import { ManagedAudio } from '../audio-manager';

export class SpellBasicAttack extends Spell {
    constructor() {
        super({
            name: 'Basic Attack',
            mana: 0,
            level: 0,
            vocation: 0,
            ico: '🗡'
        })
    }

    public getSpellFormulaPlayer(player: Player) {
        return {
            min: 0,
            max: 1,
            magicEffect: MagicEffect.E_SLASH,
            type : DamageType.PHYSICAL,
            range : 0,
            shootEffect : null
        }
    }

    public static createFormula(min: number, max: number = min, magicEffect : number = MagicEffect.E_SLASH, 
        type : DamageType, range : number = 0, shootEffect : number = null, audio : ManagedAudio = null) 
    {
        return {
            min: min,
            max: max,
            magicEffect: magicEffect,
            type : type,
            range : range,
            shootEffect : shootEffect,
            audio: audio
        }
    }

    public cast(caster : Creature, target : Creature, formula: any) : boolean {

        if (!caster.target) {
            return false
        }
        
        if (caster.distanceForShootTo(caster.target.pos) > formula.range) {
            return false
        }

        const damage = formula.min + ((formula.max - formula.min) * Math.random()) | 0

        caster.target.receiveDamage(caster,damage, formula.type)
        caster.game.addComponent(new MagicEffect(caster.target.pos, caster.game, formula.magicEffect)) 

        if (formula.shootEffect != null)
            caster.game.addComponent(new ShootEffect(caster.game, caster.pos, caster.target.pos, formula.shootEffect)) 

        if (formula.audio)
            formula.audio.play()

        return true
    }
}