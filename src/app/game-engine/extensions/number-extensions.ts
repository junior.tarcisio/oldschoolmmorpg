//https://stackoverflow.com/questions/12802383/extending-array-in-typescript
interface Number {
    floor(): Number;
}

Number.prototype.floor = function () {
    return (this|0);
}