export class FpsCounter {
    show = true
    now = 0
    avg = 0
    real = 0
    realSUM = 0
    avgLastThink = 0
    avgList : Array<number> = []

    constructor() {

    }

    calculateFps(currentFrameTime:number, drawTimer:number, updateTimer:number) {
        this.now = (1000/(drawTimer + updateTimer)) | 0
        this.realSUM++
    
        this.avgList.push(this.now)
        if (this.avgList.length > 60)
          this.avgList.shift()

        if (currentFrameTime - this.avgLastThink >= 1000) {
            this.avg = this.avgList.reduce((total, amount, index, array) => {
                total += amount
                return index === array.length-1? (total/array.length)|0 : total        
            })
            this.real = this.realSUM
            this.realSUM = 0
            this.avgLastThink = Date.now()
        }
    }
}