import { LightColorRGB } from './../enumerables/rgb-color';
import { Tile } from './tile'

const tiles = new Array<Tile>();
tiles[0] = new Tile("water", true, [1, 2]);
tiles[1] = new Tile("grass", false, [0]);
tiles[2] = new Tile("floor", false, [3], null, 1.5);
tiles[3] = new Tile("floor", false, [4], null, 1.5);
tiles[4] = new Tile("floor", false, [5], null, 1.5);
tiles[5] = new Tile("stone", true, [6]);
tiles[6] = new Tile("grass", false, [7]);
tiles[7] = new Tile("fire", true, [8, 9], LightColorRGB.RED);

//edges
tiles[8] = new Tile("water edge", false, [10]);
tiles[9] = new Tile("water edge", false, [11]);
tiles[10] = new Tile("water edge", false, [12]);
tiles[11] = new Tile("water edge", false, [13]);

//corner outer
tiles[12] = new Tile("water edge", false, [14]);
tiles[13] = new Tile("water edge", false, [15]);
tiles[14] = new Tile("water edge", false, [16]);
tiles[15] = new Tile("water edge", false, [17]);

//corner inner
tiles[16] = new Tile("water edge", false, [18]);
tiles[17] = new Tile("water edge", false, [19]);
tiles[18] = new Tile("water edge", false, [20]);
tiles[19] = new Tile("water edge", false, [21]);

tiles[20] = new Tile("slime", true, [22, 23], LightColorRGB.GREEN);
tiles[21] = new Tile("grass", false, [24]);
tiles[22] = new Tile("grass", false, [25]);
tiles[23] = new Tile("grass", false, [26]);
tiles[24] = new Tile("grass", false, [27]);
tiles[25] = new Tile("flower", false, [28]);
tiles[26] = new Tile("sword", false, [29]);
tiles[27] = new Tile("fire", false, [30,31,32], LightColorRGB.RED);
tiles[28] = new Tile("stones", false, [34]);
tiles[29] = new Tile("stone", false, [35]);
tiles[30] = new Tile("fire", false, [36,37], LightColorRGB.RED);
tiles[31] = new Tile("tree bush", true, [38]);
tiles[32] = new Tile("tree bush", true, [39]);
tiles[33] = new Tile("tree bush", true, [40]);
tiles[34] = new Tile("tree bush", true, [41]);
tiles[35] = new Tile("crystal", true, [42,43], LightColorRGB.BLUE);
tiles[36] = new Tile("stairs", false, [45], null, 0.4);
tiles[37] = new Tile("stones", true, [46], null);
tiles[38] = new Tile("stones", true, [47], null);

export default tiles;