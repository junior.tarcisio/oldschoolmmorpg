import { Game } from './../game';
import { MapTile } from './map';
import { Monster } from '../things/monsters/monster';
import tiles from './tiles';
import spriteTable from '../map/sprite-table'
import itemsSpriteTable from '../things/items/items-sprite-table'
import itemsDefinition from '../things/items/items-definition';
import monstersDefinition from '../things/monsters/monster-definition';
import { MagicEffect } from '../effects/magic-effect';
import { GameConstants } from '../game-constants';
import { Item } from '../things/items/item';
import { NPC } from '../things/npcs/npc';

export enum MapEditorSelectionType {
    None = 0,
    Eraser = 1,
    Tile = 2,
    Item = 3,
    Monster = 4
}

export class MapEditorSelection {
    type : MapEditorSelectionType = MapEditorSelectionType.None
    id : number = null
}

export class MapEditor {
    game : Game
    animationFrame = 0
    selectedTab = 0
    readonly ANIMATION_LENGTH = 15

    constructor(game:Game) {
        this.game = game
    }
    
    selection : MapEditorSelection = new MapEditorSelection()

    selectTile(tileID : number) {
        this.selection.type = MapEditorSelectionType.Tile
        this.selection.id = tileID
        //this.game.player.enableGodMode()
    }

    selectItem(itemID: number) {
        this.selection.type = MapEditorSelectionType.Item
        this.selection.id = itemID
        //this.game.player.enableGodMode()
    }

    selectMonster(monsterID: number) {
        this.selection.type = MapEditorSelectionType.Monster
        this.selection.id = monsterID
        //this.game.player.enableGodMode()
    }

    saveMap() : any {
        const mapSaveTiles = new Array<Array<number>>()
        const mapSaveItems = new Array<Array<number>>()
        const mapSaveMonsters = new Array<Array<number>>()
        const mapSaveNPCs = new Array<Array<number>>()

        for (let y = 0; y < this.game.map.length; y++) {            
            mapSaveTiles[y] = new Array<number>()
            //mapSaveItems[y] = new Array<number>()


            for (let x = 0; x < this.game.map[y].length; x++) {
                const mapX = this.game.map[y][x]
                
                if (mapX.tile > 0)
                    mapSaveTiles[y][x] = mapX.tile
                else
                    mapSaveTiles[y][x] = null
                
                if (mapX.itemList != null && mapX.itemList.length > 0) {
                    if (mapSaveItems[y] == null)
                        mapSaveItems[y] = new Array<number>()

                    //TODO:Refactor to N items (is it required?)
                    mapSaveItems[y][x] = mapX.itemList[0].id
                    // if (mapX.itemList.length == 1)
                    //     mapSaveItems[y][x] = mapX.itemList[0].id
                    // else {
                    //     mapSaveItems[y][x] = new Array<number>()
                    // }
                }
                
                if (mapX.creatureList != null && mapX.creatureList.length > 0) {

                    if (mapX.creatureList[0] instanceof Monster) {
                        if (mapSaveMonsters[y] == null)
                            mapSaveMonsters[y] = new Array<number>()
    
                        mapSaveMonsters[y][x] = (mapX.creatureList[0] as Monster).monsterDefinitionID
                    }
                    else if (mapX.creatureList[0] instanceof NPC) {
                        const npc = (mapX.creatureList[0] as NPC)

                        if (mapSaveNPCs[npc.originPos.y] == null)
                            mapSaveNPCs[npc.originPos.y] = new Array<number>()
    
                        mapSaveNPCs[npc.originPos.y][npc.originPos.x] = (mapX.creatureList[0] as NPC).npcDefinitionID
                    }
                }
            }
        }

        return 'export const map = ' + JSON.stringify({ 
            tiles : mapSaveTiles, 
            items : mapSaveItems, 
            monsters: mapSaveMonsters,
            npcs : mapSaveNPCs }).replace(new RegExp('null', 'g'), '').replace(new RegExp(/\],/, 'g'), '],\n')
    }

    drawMapEditorCursor(currentFrameTime:number) {
        
        if (this.selection.type == MapEditorSelectionType.None)
            return

        const pos = this.game.gameCommandsDOM.mousePositionTOSQMPosition()

        pos.x += GameConstants.SCREEN_CENTER_SQM_X
        pos.y += GameConstants.SCREEN_CENTER_SQM_Y
        
        
        this.game.ctx.beginPath()
        this.game.ctx.globalAlpha = 0.333
        this.game.ctx.lineWidth = 6
        this.game.ctx.strokeStyle = '#F00'

        this.game.ctx.rect(
            pos.x * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierX, 
            pos.y * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierY, 
            GameConstants.spriteCalculatedSize, 
            GameConstants.spriteCalculatedSize)
        this.game.ctx.stroke()
        this.game.ctx.globalAlpha = 1
        
        if (this.animationFrame > this.ANIMATION_LENGTH)
            this.animationFrame = this.ANIMATION_LENGTH*-1
        else
            this.animationFrame+= 0.5;

        const anmfactor = Math.abs(Math.abs(this.animationFrame)  - this.ANIMATION_LENGTH) //from  0 (15-15) -> 15 (0-15) -> 0
        

        this.game.ctx.globalAlpha = 1.25 - (anmfactor/this.ANIMATION_LENGTH)
        this.game.ctx.shadowOffsetX = anmfactor/2;
        this.game.ctx.shadowOffsetY = anmfactor;
        this.game.ctx.shadowBlur = anmfactor;
        this.game.ctx.shadowColor = "#000000B0";

        if (this.selection.type == MapEditorSelectionType.Tile) {
            const tile = tiles[this.selection.id];
            const currentFrameMod = ((currentFrameTime/100) % tile.frames.length)|0
            this.game.ctx.drawImage(spriteTable,
                //start fucking bug on resizing sprite table UPPER/BOTTOM tile mixing 1px with current TILE on resize
                //probably it happens because the canvas mix the parent textures on stretching?
                //So I'm clipping the edges of the texture
                1,
                tile.frames[currentFrameMod] * GameConstants.SPRITE_REAL_RESOLUTION + 1, 
                GameConstants.SPRITE_REAL_RESOLUTION - 2,
                GameConstants.SPRITE_REAL_RESOLUTION - 2,
                //end fucking clipping bug
                pos.x * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierX - anmfactor/2,
                pos.y * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierY - anmfactor,
                GameConstants.spriteCalculatedSize,
                GameConstants.spriteCalculatedSize)
        }
        else if (this.selection.type == MapEditorSelectionType.Item) {
            const item = itemsDefinition[this.selection.id]
            this.game.ctx.drawImage(itemsSpriteTable,
                //start fucking bug on resizing sprite table UPPER/BOTTOM tile mixing 1px with current TILE on resize
                //probably it happens because the canvas mix the parent textures on stretching?
                //So I'm clipping the edges of the texture
                1,
                item.frames[0] * GameConstants.SPRITE_REAL_RESOLUTION + 1, 
                GameConstants.SPRITE_REAL_RESOLUTION - 2,
                GameConstants.SPRITE_REAL_RESOLUTION - 2,
                //end fucking clipping bug
                pos.x * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierX - anmfactor/2,
                pos.y * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierY - anmfactor,
                GameConstants.spriteCalculatedSize,
                GameConstants.spriteCalculatedSize)
        }
        else if (this.selection.type == MapEditorSelectionType.Monster) {
            const monster = monstersDefinition[this.selection.id]
            this.game.ctx.drawImage(monster.frames[((currentFrameTime/100) % monster.frames.length)|0],
                // 0,
                // 0, 
                // GameConstants.SPRITE_REAL_RESOLUTION * monster.size.x,
                // GameConstants.SPRITE_REAL_RESOLUTION * monster.size.y,
                pos.x * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierX - anmfactor/2 - (72 * (monster.size.x - 1)),
                pos.y * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierY - anmfactor - (72 * (monster.size.y - 1)),
                GameConstants.spriteCalculatedSize * monster.size.x,
                GameConstants.spriteCalculatedSize * monster.size.y)
        }
        else if (this.selection.type == MapEditorSelectionType.Eraser) {
            this.game.ctx.beginPath()
            this.game.ctx.globalAlpha = 0.333
            this.game.ctx.lineWidth = 10
            this.game.ctx.strokeStyle = '#F00'
            this.game.ctx.moveTo(
                pos.x * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierX, 
                pos.y * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierY)
            this.game.ctx.lineTo(
                pos.x * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierX + GameConstants.spriteCalculatedSize, 
                pos.y * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierY + GameConstants.spriteCalculatedSize)
            this.game.ctx.stroke()
            
            this.game.ctx.moveTo(
                pos.x * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierX, 
                pos.y * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierY + GameConstants.spriteCalculatedSize)
            this.game.ctx.lineTo(
                pos.x * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierX + GameConstants.spriteCalculatedSize, 
                pos.y * GameConstants.spriteCalculatedSize - this.game.drawMovingModifierY)
            this.game.ctx.stroke()
            this.game.ctx.closePath()
        }
            
        this.game.ctx.shadowOffsetX = 0;
        this.game.ctx.shadowOffsetY = 0;
        this.game.ctx.shadowBlur = 0;
        this.game.ctx.globalAlpha = 1

    
    }

    isEditing = () => this.selection.type != MapEditorSelectionType.None

    editSQM(mapSQMPos) {

        if (this.selection.type == MapEditorSelectionType.Tile) {
            this.game.map[mapSQMPos.y][mapSQMPos.x].tile = this.selection.id
            this.game.addComponent(new MagicEffect(mapSQMPos, this.game, 4)) 
        }
        else if (this.selection.type == MapEditorSelectionType.Item) {            
            this.game.map[mapSQMPos.y][mapSQMPos.x].addItem(this.game, mapSQMPos ,new Item(this.selection.id))
            this.game.addComponent(new MagicEffect(mapSQMPos, this.game, 4)) 
        }
        else if (this.selection.type == MapEditorSelectionType.Monster) {            
            this.game.map[mapSQMPos.y][mapSQMPos.x].addCreature(
                new Monster(this.selection.id,{x : mapSQMPos.x, y : mapSQMPos.y}, this.game))
            this.game.addComponent(new MagicEffect(mapSQMPos, this.game, 4)) 
        }
        else if (this.selection.type == MapEditorSelectionType.Eraser) {           
            const creatureList = this.game.map[mapSQMPos.y][mapSQMPos.x].creatureList
            const itemList = this.game.map[mapSQMPos.y][mapSQMPos.x].itemList

            if (creatureList && creatureList.length > 0) {
                creatureList.pop()
                this.game.addComponent(new MagicEffect(mapSQMPos, this.game, 2)) 
            }
            else if (itemList != null && itemList.length > 0) {
                itemList.pop()
                this.game.addComponent(new MagicEffect(mapSQMPos, this.game, 2)) 
            }
            else {
                this.game.map[mapSQMPos.y][mapSQMPos.x].tile = 0                
                this.game.addComponent(new MagicEffect(mapSQMPos, this.game, 2)) 
            }
        }
    }

    resize(y,x) {

        for (let i = 0; i < y; i++) {
            if (this.game.map[i] == null || typeof this.game.map[i] == "undefined")
                this.game.map[i] = []
            
            for (let j = 0; j < x; j++) {
                if (this.game.map[i][j] == null || typeof this.game.map[i][j] == "undefined")
                    this.game.map[i][j] = new MapTile(0)
            }
        }
    }

    cleanSelection() {        
        this.selection.type = MapEditorSelectionType.None
    }
}
