import { Creature } from "../things/creature";
import itemsDefinition from "../things/items/items-definition";
import tiles from "./tiles";
import { Game } from "../game";
import { Monster } from "../things/monsters/monster";
import { Item } from "../things/items/item";
import { ItemFlags } from "../things/items/item-flags";
import {map} from "./map-file"
import { NPC } from "../things/npcs/npc";
import { MagicEffect } from "../effects/magic-effect";

export class MapFactory {

    load(game:Game) : Array<Array<MapTile>> {
        let mapTiles = new Array<Array<MapTile>>()

        for (let y = 0; y < map.tiles.length; y++) {
            mapTiles[y] = new Array<MapTile>()

            for (let x = 0; x < map.tiles[y].length; x++) {
                if (map.tiles[y][x] != null)
                    mapTiles[y][x] = new MapTile(map.tiles[y][x])
                else 
                    mapTiles[y][x] = new MapTile(0)

                if (map.items[y] != null && map.items[y][x] != null) 
                    mapTiles[y][x].addItem(game, {x: x, y: y}, new Item(map.items[y][x]))
                
                if (map.monsters[y] != null && map.monsters[y][x] != null) 
                    mapTiles[y][x].addCreature(new Monster(map.monsters[y][x], {x:x, y:y}, game))
                
                if (map.npcs[y] != null && map.npcs[y][x] != null) 
                    mapTiles[y][x].addCreature(new NPC(map.npcs[y][x], {x:x, y:y}, game))
            }
        }

        return mapTiles
    }
}

export class MapTile {
    public tile : number = null
    public decorationTile : number = null
    public creatureList : Array<Creature> = null
    public itemList : Array<Item> = null
    public effectsList : Array<any> = null

    constructor(tile) {
        this.tile = tile
    }

    addCreature(creature : Creature) {
        if (this.creatureList == null)
            this.creatureList = new Array<Creature>();
        
        this.creatureList.push(creature);
    }

    removeCreature(creature : Creature) {
        const index = this.creatureList.indexOf(creature);
        this.creatureList.splice(index, 1);
    }
    
    addItem(game:Game, mapSQMPos, item : Item) {
        if (this.tile == 0){            
            game.addComponent(new MagicEffect({x:mapSQMPos.x,y:mapSQMPos.y}, game, 3)) 
            return
        }

        if (this.itemList == null)
            this.itemList = new Array<Item>();
        
        this.itemList.push(item);
    }

    removeItem(item : Item) {
        const index = this.itemList.indexOf(item);
        this.itemList.splice(index, 1);
    }
    
    addEffect(effect) {
        if (this.effectsList == null)
            this.effectsList = new Array<Item>();
        
        this.effectsList.push(effect);
    }

    removeEffect(effect) {
        const index = this.effectsList.indexOf(effect);
        this.effectsList.splice(index, 1);
    }

    movementBlocked () : boolean {
        //maybe can cache this information in a variable bool and change the value when items are added/removed or creatures
        return  tiles[this.tile].blockable || 
                (this.creatureList != null && this.creatureList.length > 0)  || 
                (this.itemList != null && this.itemList.length > 0 && itemsDefinition[this.itemList[0].id].flags & ItemFlags.BLOCK_PATH)
                //(this.itemList != null && this.itemList.filter(x=> itemsDefinition[x.id].blockable).length > 0)
    }

    
    movementBlockedIgnoreCreature () : boolean {
        //maybe can cache this information in a variable bool and change the value when items are added/removed or creatures
        return  tiles[this.tile].blockable || 
                (this.itemList != null && this.itemList.length > 0 && itemsDefinition[this.itemList[0].id].flags & ItemFlags.BLOCK_PATH)
                //(this.itemList != null && this.itemList.filter(x=> itemsDefinition[x.id].blockable).length > 0)
    }

    hasCreature() : boolean {
        return (this.creatureList != null && this.creatureList.length > 0)
    }

    getTile() {
        return tiles[this.tile]
    }
        
}
