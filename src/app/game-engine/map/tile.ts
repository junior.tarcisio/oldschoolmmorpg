import { LightColorRGB } from './../enumerables/rgb-color'
import spriteTable from './sprite-table'
import { Game } from '../game';
import { GameConstants } from '../game-constants';

export class Tile {
    name
    blockable
    frames
    frameTimer
    speed : number = 1
    lightColor: LightColorRGB

    constructor(name, blockable, frames, lightColor:LightColorRGB=null, speed:number=1) {
        this.name = name
        this.blockable = blockable
        this.frames = frames
        //this.currentFrame = 0
        this.frameTimer = Date.now()
        this.lightColor = lightColor
        this.speed = speed
    }

    getCurrentFrame = (currentFrameTime:number) => (currentFrameTime/1000|0) % this.frames.length

    draw(game: Game, currentFrameTime:number, tileDrawPosX :number, tileDrawPosY:number) {
        const currentFrame = this.getCurrentFrame(currentFrameTime)

        game.ctx.drawImage(spriteTable,
            //start clipping bug 
            // on resizing sprite table UPPER/BOTTOM tile mixing 1px with current TILE on resize
            // probably it happens because the canvas mix the parent textures on stretching?
            // So I'm clipping the edges of the texture
            1,
            this.frames[currentFrame] * GameConstants.SPRITE_REAL_RESOLUTION + 1, 
            GameConstants.SPRITE_REAL_RESOLUTION - 2,
            GameConstants.SPRITE_REAL_RESOLUTION - 2,
            //end clipping bug
            tileDrawPosX,
            tileDrawPosY,
            GameConstants.spriteCalculatedSize,
            GameConstants.spriteCalculatedSize)                

        if (this.lightColor)
            game.nightEffectManager.addLightEffect({ x: tileDrawPosX, y: tileDrawPosY}, 1, this.lightColor)

    }
}
