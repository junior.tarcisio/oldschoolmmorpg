import { ConditionParalyzed } from './condition-paralyze';
import { ConditionExhausted } from './condition-exhausted';
import { ConditionFeeding } from "./condition-feeding";
import { ConditionHaste } from "./condition-haste";
import { ConditionLight } from "./condition-light";

export enum ConditionType {
    FEEDING = 1,
    HASTE = 2,
    LIGHT = 4,
    EXHAUSTED = 8,
    PARALYZED = 16
}

const conditionsDefinition = new Array<any>()
conditionsDefinition[ConditionType.FEEDING] = ConditionFeeding
conditionsDefinition[ConditionType.HASTE] = ConditionHaste
conditionsDefinition[ConditionType.LIGHT] = ConditionLight
conditionsDefinition[ConditionType.EXHAUSTED] = ConditionExhausted
conditionsDefinition[ConditionType.PARALYZED] = ConditionParalyzed


export default conditionsDefinition