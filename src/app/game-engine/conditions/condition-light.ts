import { Condition } from "./condition";
import { Creature } from "../things/creature";

export class ConditionLight extends Condition {
    public duration : number
    public expireAt : number 
    public light : any
    static readonly DECAY_UNDER_PERCENT = 0.7
    static readonly DECAY_UNDER_DIFF_FROM_100_PERCENT = 1 - ConditionLight.DECAY_UNDER_PERCENT

    constructor() {
        super('💡', 'Magic light')
    }

    add(creature: Creature, props: any) : boolean {
        this.duration = props.duration
        this.expireAt = creature.game.frame.time + props.duration
        this.light = props

        if (!this.alive)
            this.alive = true

        return true
    }

    getCurrentLight(time:number) {
        const timeLeftPercent = (this.expireAt - time) / this.duration

        if (this.light.intensity < 0.3) {
            return {
                intensity : 0.3,
                color : this.light.color
            }       
        }

        if (timeLeftPercent > ConditionLight.DECAY_UNDER_PERCENT) {
            return {
                intensity : this.light.intensity,
                color : this.light.color
            }        
        } 
        else {
            return {
                intensity : this.light.intensity * (timeLeftPercent + ConditionLight.DECAY_UNDER_DIFF_FROM_100_PERCENT),
                color : this.light.color
            }        
        }
          
    }
    
    update(creature:Creature) {

        if (creature.game.frame.time > this.expireAt) {
            this.remove(creature)
            this.alive = false
        }
    }

    remove(creature: Creature) : boolean {
        return true
    }

    getSecondsLeft(now:number) : number {
        return ((this.expireAt - now) / 1000)|0
    }
}