import { Condition } from "./condition";
import { Creature } from "../things/creature";

export class ConditionParalyzed extends Condition {
    public expireAt : number 
    public amount : number

    constructor() {
        super('🐌','Decreased speed') //🏃 👣 😖 😵 🚶 🤐
    }

    add(creature: Creature, props: any) : boolean {
    
        this.amount = props.amount
        this.expireAt = creature.game.frame.time + props.duration

        if (!this.alive)
            this.alive = true

        return true
    }

    getCurrentAmount() {
        return this.amount
    }
    
    update(creature:Creature) {

        if (creature.game.frame.time > this.expireAt) {
            this.remove(creature)
            this.alive = false
        }
    }

    remove(creature: Creature) : boolean {
        return true
    }

    getSecondsLeft(now:number) : number {
        return ((this.expireAt - now) / 1000)|0
    }
}