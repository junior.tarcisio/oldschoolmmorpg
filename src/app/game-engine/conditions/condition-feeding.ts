import { Condition } from "./condition";
import { Creature } from "../things/creature";

export class ConditionFeeding extends Condition {

    static readonly MAX_FOOD_MULTIPLIER : number = 6 // 600 = 10 minutes
    static readonly MAX_FOOD_TICKS : number = 100 * ConditionFeeding.MAX_FOOD_MULTIPLIER
    static readonly TICK_INTERVAL : number = 1000
    public foodTicks : number = 0
    public lastTick : number = 0

    constructor() {
        super('🍗', 'Digesting food') //🥩 🍖 🍜 🍴
    }

    add(creature: Creature, props: any) : boolean {
        
        if (this.foodTicks + props*ConditionFeeding.MAX_FOOD_MULTIPLIER > ConditionFeeding.MAX_FOOD_TICKS) {
            creature.game.addMessageHistory('You are full.')
            return false
        }

        this.foodTicks += props*ConditionFeeding.MAX_FOOD_MULTIPLIER

        if (!this.alive)
            this.alive = true

        return true
    }
    
    update(creature:Creature) {
        const now = creature.game.frame.time

        if (now < this.lastTick + ConditionFeeding.TICK_INTERVAL)
            return

        if (this.foodTicks <= 0) {
            this.remove(creature)
            this.alive = false
        }

        this.foodTicks--
        creature.receiveHealth(2)
        creature.receiveMana(1,false)
        this.lastTick = now
    }

    remove(creature: Creature) : boolean {
        return true
    }

    getSecondsLeft(now:number) : number {
        return  (this.foodTicks * ConditionFeeding.TICK_INTERVAL / 1000)|0
    }
}