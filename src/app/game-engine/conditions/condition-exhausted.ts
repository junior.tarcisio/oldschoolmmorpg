import { Condition } from "./condition";
import { Creature } from "../things/creature";

export class ConditionExhausted extends Condition {
    public expireAt : number 
    public amount : number

    constructor() {
        super('🕑','Your spells are on cooldown') //🔕 🔈 🕑 🚫 🛑 💬
    }

    add(creature: Creature, props: any) : boolean {
    
        this.expireAt = creature.game.frame.time + props

        if (!this.alive)
            this.alive = true

        return true
    }

    getCurrentAmount() {
        return this.amount
    }
    
    update(creature:Creature) {

        if (creature.game.frame.time > this.expireAt) {
            this.remove(creature)
            this.alive = false
        }
    }

    remove(creature: Creature) : boolean {
        return true
    }

    getSecondsLeft(now:number) : number {
        return  (((this.expireAt - now) / 100)|0) / 10
    }
}