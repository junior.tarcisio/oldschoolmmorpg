import { Creature } from "../things/creature";

export abstract class Condition {

    constructor(public ico:string, public description:string){

    }

    public alive = true

    abstract add(creature:Creature, props:any) : boolean

    abstract update(creature:Creature)

    abstract remove(creature:Creature) : boolean
    
    abstract getSecondsLeft(now:number) : number
}
