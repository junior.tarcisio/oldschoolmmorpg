import { Direction } from './../enumerables/direction';
import { Game } from "../game";

export abstract class Thing {
    public readonly game;
    public pos = { x: 0, y: 0 };
    public alive : boolean;

    constructor(position, game : Game) {
        this.game = game;
        this.pos.x = position.x;
        this.pos.y = position.y;
        this.alive = true;
    }
    
    closeTo = (pos) : boolean => Math.abs(this.pos.x - pos.x) <= 1 && Math.abs(this.pos.y - pos.y) <= 1

    distanceTo = (pos) : number =>  Math.abs(this.pos.x - pos.x) + Math.abs(this.pos.y - pos.y)

    distanceForShootTo = (pos) : number => {
        let distX = Math.abs(this.pos.x - pos.x)
        let distY = Math.abs(this.pos.y - pos.y)
         
        if (distX >= distY)
            return distX
        else
            return distY
    } 
    
    distanceBetween = (pos1,pos2) : number =>  Math.abs(pos1.x - pos2.x) + Math.abs(pos1.y - pos2.y)

    getMapTile = () => this.game.map[this.pos.y][this.pos.x]

    getDirectionPos(pos, direction : Direction){
        const newPos ={x:pos.x, y:pos.y}
        switch (direction) {
            case Direction.LEFT:
                newPos.x--
                break
            case Direction.UP:
                newPos.y--
                break
            case Direction.RIGHT:
                newPos.x++
                break
            case Direction.DOWN:
                newPos.y++
                break
        }
        return newPos
    }

    abstract draw(game: Game, tileDrawPosX:number, tileDrawPosY:number, currentFrameTime:number) : void
}