import { LightColorRGB } from '../../enumerables/rgb-color'
import { ItemFlags } from './item-flags'
import { ItemScriptHealthPotion } from './scripts/item_script_health_potion';
import { ItemScriptManaPotion } from './scripts/item_script_mana_potion';
import { SlotType } from '../player';
import { ItemScriptEat } from './scripts/item_script_eat';
import { ShootEffect } from '../../effects/shoot-effect';

export enum ItemProperties {
    LIGHT_COLOR,
    ITEM_SCRIPT,
    ITEM_SCRIPT_POWER,
    SLOT_TYPE,
    ATTACK_DAMAGE,
    ATTACK_RANGE,
    SHOOT_EFFECT,
    ARMOR,
    SPEED
}

export class ItemDefinition {
    name: string
    frames: Array<number>
    weight: number = null
    flags : ItemFlags
    framesY:Array<number>
    properties
    castShadows : boolean = false

    constructor(name: string, frames: Array<number>, weight : number = null, flags : ItemFlags, framesY:Array<number> = [], castShadows = false) {
        this.name = name
        this.frames = frames
        this.weight = weight
        this.flags = flags
        this.properties = {}
        this.framesY = framesY
        this.castShadows = castShadows
    }

    getCurrentFrame = (currentFrameTime:number) => (currentFrameTime/1000|0) % this.frames.length
}

const itemsDefinition = new Array<ItemDefinition>()
itemsDefinition[0] = new ItemDefinition("stone", [0], 250, ItemFlags.BLOCK_PATH)
itemsDefinition[1] = new ItemDefinition("sword", [1], 10.5, ItemFlags.PICKABLE)
itemsDefinition[1].properties[ItemProperties.SLOT_TYPE] = SlotType.RIGHT_HAND | SlotType.LEFT_HAND
itemsDefinition[1].properties[ItemProperties.ATTACK_DAMAGE] = 30
itemsDefinition[2] = new ItemDefinition("dead panda", [2], 145.0, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[3] = new ItemDefinition("stones", [3], 6, ItemFlags.NONE)
itemsDefinition[4] = new ItemDefinition("stone", [4], 2, ItemFlags.NONE)
itemsDefinition[5] = new ItemDefinition("grass", [5,6], 0.5, ItemFlags.NONE)
itemsDefinition[6] = new ItemDefinition("slained pocong", [7], 55.0, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[7] = new ItemDefinition("slained kuntilanak", [8], 65.0, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[8] = new ItemDefinition("slained soul reaper", [9], 95.0, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[9] = new ItemDefinition("dead crusader", [10], 95.0, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[10] = new ItemDefinition("crystal sword", [11,12], 15.0, ItemFlags.PICKABLE)
itemsDefinition[10].properties[ItemProperties.LIGHT_COLOR] = LightColorRGB.BLUE
itemsDefinition[10].properties[ItemProperties.SLOT_TYPE] = SlotType.RIGHT_HAND | SlotType.LEFT_HAND
itemsDefinition[10].properties[ItemProperties.ATTACK_DAMAGE] = 110
itemsDefinition[11] = new ItemDefinition("fire sword", [13,14,15], 12.5, ItemFlags.PICKABLE)
itemsDefinition[11].properties[ItemProperties.LIGHT_COLOR] = LightColorRGB.RED
itemsDefinition[11].properties[ItemProperties.SLOT_TYPE] = SlotType.RIGHT_HAND | SlotType.LEFT_HAND
itemsDefinition[11].properties[ItemProperties.ATTACK_DAMAGE] = 150
itemsDefinition[12] = new ItemDefinition("dead barbarian", [16], 115.0, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[13] = new ItemDefinition("leaves", [17], .1, ItemFlags.NONE)
itemsDefinition[14] = new ItemDefinition("leaves", [18], .1, ItemFlags.NONE)
itemsDefinition[15] = new ItemDefinition("leaves", [19], .1, ItemFlags.NONE)
itemsDefinition[16] = new ItemDefinition("leaves", [20], .1, ItemFlags.NONE)
itemsDefinition[17] = new ItemDefinition("leaf", [21], .1, ItemFlags.NONE)
itemsDefinition[18] = new ItemDefinition("big leaf", [22], .2, ItemFlags.NONE)
itemsDefinition[19] = new ItemDefinition("big leaves", [23], .4, ItemFlags.NONE)
itemsDefinition[20] = new ItemDefinition("old leaves", [24], .1, ItemFlags.NONE)
itemsDefinition[21] = new ItemDefinition("old leaves", [25], .1, ItemFlags.NONE)
itemsDefinition[22] = new ItemDefinition("old leaves", [26], .1, ItemFlags.NONE)
itemsDefinition[23] = new ItemDefinition("old leaves", [27], .1, ItemFlags.NONE)
itemsDefinition[24] = new ItemDefinition("old leaf", [28], .1, ItemFlags.NONE)
itemsDefinition[25] = new ItemDefinition("big old leaf", [29], .2, ItemFlags.NONE)
itemsDefinition[26] = new ItemDefinition("big old leaves", [30], .4, ItemFlags.NONE)
itemsDefinition[27] = new ItemDefinition("rice plantation", [31], .4, ItemFlags.BLOCK_PATH)
itemsDefinition[28] = new ItemDefinition("rice plantation", [32], .4, ItemFlags.BLOCK_PATH)
itemsDefinition[29] = new ItemDefinition("rice plantation", [33], .4, ItemFlags.BLOCK_PATH)
itemsDefinition[30] = new ItemDefinition("dead queen ant", [34], 30, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[31] = new ItemDefinition("dead java spider", [35], 50, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[32] = new ItemDefinition("slained skeleton", [36], 40, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[33] = new ItemDefinition("dead butterfly", [37], 2, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[34] = new ItemDefinition("fire", [38,39], 2, ItemFlags.NONE)
itemsDefinition[34].properties[ItemProperties.LIGHT_COLOR] = LightColorRGB.RED
itemsDefinition[35] = new ItemDefinition("dead blue giant dragonfly", [40], 20, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[36] = new ItemDefinition("dead red giant dragonfly", [41], 20, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[37] = new ItemDefinition("dead green giant dragonfly", [42], 20, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[38] = new ItemDefinition("big health potion", [43,44], .250, ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[38].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptHealthPotion
itemsDefinition[38].properties[ItemProperties.ITEM_SCRIPT_POWER] = 500
itemsDefinition[39] = new ItemDefinition("big mana potion", [45,46], .250, ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[39].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptManaPotion
itemsDefinition[39].properties[ItemProperties.ITEM_SCRIPT_POWER] = 300
itemsDefinition[40] = new ItemDefinition("small health potion", [47], .150, ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[40].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptHealthPotion
itemsDefinition[40].properties[ItemProperties.ITEM_SCRIPT_POWER] = 50
itemsDefinition[41] = new ItemDefinition("small mana potion", [48], .150, ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[41].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptManaPotion
itemsDefinition[41].properties[ItemProperties.ITEM_SCRIPT_POWER] = 50
itemsDefinition[42] = new ItemDefinition("magic horned plate armor", [50], 20.0, ItemFlags.PICKABLE)
itemsDefinition[42].properties[ItemProperties.SLOT_TYPE] = SlotType.CHEST
itemsDefinition[42].properties[ItemProperties.ARMOR] = 15
itemsDefinition[43] = new ItemDefinition("plate armor", [49], 20.0, ItemFlags.PICKABLE)
itemsDefinition[43].properties[ItemProperties.SLOT_TYPE] = SlotType.CHEST
itemsDefinition[44] = new ItemDefinition("magic horned plate helmet", [51], 20.0, ItemFlags.PICKABLE)
itemsDefinition[44].properties[ItemProperties.SLOT_TYPE] = SlotType.HEAD
itemsDefinition[44].properties[ItemProperties.ARMOR] = 10
itemsDefinition[45] = new ItemDefinition("magic horned plate shield", [52], 20.0, ItemFlags.PICKABLE)
itemsDefinition[45].properties[ItemProperties.SLOT_TYPE] = SlotType.RIGHT_HAND | SlotType.LEFT_HAND
itemsDefinition[46] = new ItemDefinition("magic amulet", [53], .2, ItemFlags.PICKABLE)
itemsDefinition[46].properties[ItemProperties.SLOT_TYPE] = SlotType.NECK
itemsDefinition[47] = new ItemDefinition("magic ring", [54], .1, ItemFlags.PICKABLE)
itemsDefinition[47].properties[ItemProperties.SLOT_TYPE] = SlotType.RIGHT_FINGER | SlotType.LEFT_FINGER 
itemsDefinition[48] = new ItemDefinition("magic plate sarung", [55], .1, ItemFlags.PICKABLE)
itemsDefinition[48].properties[ItemProperties.SLOT_TYPE] = SlotType.LEGS
itemsDefinition[48].properties[ItemProperties.ARMOR] = 9
itemsDefinition[49] = new ItemDefinition("simple boots", [56], .1, ItemFlags.PICKABLE)
itemsDefinition[49].properties[ItemProperties.SLOT_TYPE] = SlotType.FEET
itemsDefinition[49].properties[ItemProperties.ARMOR] = 1
itemsDefinition[49].properties[ItemProperties.SPEED] = 10
itemsDefinition[50] = new ItemDefinition("graveyard", [57], .1, ItemFlags.BLOCK_PATH)
itemsDefinition[51] = new ItemDefinition("graveyard", [58], .1, ItemFlags.NONE)
itemsDefinition[52] = new ItemDefinition("stone", [59], 5000, ItemFlags.BLOCK_PATH)
itemsDefinition[53] = new ItemDefinition("stone", [60], 5000, ItemFlags.BLOCK_PATH)
itemsDefinition[54] = new ItemDefinition("stone", [61], 5000, ItemFlags.BLOCK_PATH)
itemsDefinition[55] = new ItemDefinition("stone", [62], 5000, ItemFlags.BLOCK_PATH)
itemsDefinition[56] = new ItemDefinition("bell", [63], 1000, ItemFlags.BLOCK_PATH)
itemsDefinition[57] = new ItemDefinition("komodo keris", [64,65], 4.5, ItemFlags.PICKABLE)
itemsDefinition[57].properties[ItemProperties.SLOT_TYPE] = SlotType.LEFT_HAND | SlotType.RIGHT_HAND
itemsDefinition[57].properties[ItemProperties.ATTACK_DAMAGE] = 100
// itemsDefinition[57].properties[ItemProperties.ATTACK_RANGE] = 1
// itemsDefinition[58] = new ItemDefinition("Bell", [66], 10000, ItemFlags.BLOCK_PATH)
// itemsDefinition[59] = new ItemDefinition("Bell", [67], 10000, ItemFlags.BLOCK_PATH)
// itemsDefinition[60] = new ItemDefinition("Bell", [68], 10000, ItemFlags.BLOCK_PATH)
// itemsDefinition[61] = new ItemDefinition("Bell", [69], 10000, ItemFlags.BLOCK_PATH) 
itemsDefinition[58] = new ItemDefinition("bell", [68], 10000, ItemFlags.BLOCK_PATH, [66])
itemsDefinition[59] = new ItemDefinition("bell", [69], 10000, ItemFlags.BLOCK_PATH, [67])
itemsDefinition[60] = new ItemDefinition("tree", [71], 10000, ItemFlags.BLOCK_PATH, [70])
itemsDefinition[61] = new ItemDefinition("tree", [73], 10000, ItemFlags.BLOCK_PATH, [72])
itemsDefinition[62] = new ItemDefinition("gold", [74], 0.010, ItemFlags.PICKABLE)
itemsDefinition[63] = new ItemDefinition("dead human", [75], 68.0, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[64] = new ItemDefinition("dead ant", [76], 68.0, ItemFlags.NONE | ItemFlags.CONTAINER)
itemsDefinition[65] = new ItemDefinition("tempe goreng", [77], 0.05, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[65].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[65].properties[ItemProperties.ITEM_SCRIPT_POWER] = 20
itemsDefinition[66] = new ItemDefinition("sate ayam", [78], 0.05, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[66].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[66].properties[ItemProperties.ITEM_SCRIPT_POWER] = 20
itemsDefinition[67] = new ItemDefinition("pack sate ayam", [79], 0.2, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[67].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[67].properties[ItemProperties.ITEM_SCRIPT_POWER] = 50
itemsDefinition[68] = new ItemDefinition("bakso", [80], 0.75, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[68].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[68].properties[ItemProperties.ITEM_SCRIPT_POWER] = 75
itemsDefinition[69] = new ItemDefinition("bakso bowl", [81], 0.75, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[69].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[69].properties[ItemProperties.ITEM_SCRIPT_POWER] = 75
itemsDefinition[70] = new ItemDefinition("bakso ball", [82], 0.75, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[70].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[70].properties[ItemProperties.ITEM_SCRIPT_POWER] = 5
itemsDefinition[71] = new ItemDefinition("ketupat", [83], 0.25, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[71].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[71].properties[ItemProperties.ITEM_SCRIPT_POWER] = 25
itemsDefinition[72] = new ItemDefinition("nastar", [84], 0.05, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[72].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[72].properties[ItemProperties.ITEM_SCRIPT_POWER] = 5
itemsDefinition[73] = new ItemDefinition("kerupuk", [85], 0.1, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[73].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[73].properties[ItemProperties.ITEM_SCRIPT_POWER] = 10
itemsDefinition[74] = new ItemDefinition("nasi goreng telur", [86], 0.5, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[74].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[74].properties[ItemProperties.ITEM_SCRIPT_POWER] = 50
itemsDefinition[75] = new ItemDefinition("durian", [87], 5, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[75].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[75].properties[ItemProperties.ITEM_SCRIPT_POWER] = 100
itemsDefinition[76] = new ItemDefinition("dragon fruit", [88], 1, ItemFlags.NONE | ItemFlags.PICKABLE | ItemFlags.USABLE)
itemsDefinition[76].properties[ItemProperties.ITEM_SCRIPT] = ItemScriptEat
itemsDefinition[76].properties[ItemProperties.ITEM_SCRIPT_POWER] = 30
itemsDefinition[77] = new ItemDefinition("komodo keris", [89], 3, ItemFlags.PICKABLE)
itemsDefinition[77].properties[ItemProperties.SLOT_TYPE] = SlotType.LEFT_HAND | SlotType.RIGHT_HAND
itemsDefinition[77].properties[ItemProperties.ATTACK_DAMAGE] = 80
itemsDefinition[77].properties[ItemProperties.ATTACK_RANGE] = 5
itemsDefinition[77].properties[ItemProperties.SHOOT_EFFECT] = ShootEffect.E_ARROW
export default itemsDefinition

