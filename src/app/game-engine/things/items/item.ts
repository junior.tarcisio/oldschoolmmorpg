import { MagicEffect } from './../../effects/magic-effect';
import { Direction } from './../../enumerables/direction';
import { Game } from "../../game"
import itemsDefinition, { ItemDefinition, ItemProperties } from "./items-definition"
import itemsSpriteTable from "./items-sprite-table"
import { GameConstants } from "../../game-constants"
import Player from "../player";

export class Item {
    private animatePos = null //can be refactored to calculated on draw time
    readonly MAX_ANIMATION_FLY = GameConstants.spriteCalculatedSize
    decayAt:number
    alive = true

    constructor(public id: number, public count: number = null, public props:any = null, 
                public flyItem: Direction = null, public decayTime : number = null, public content: Array<Item> = null) {

        if (flyItem != null) {
            this.animatePos = this.MAX_ANIMATION_FLY*-1
        }

        if (decayTime != null) {
            this.decayAt = Date.now() + decayTime
        }
    }

    update(game:Game, now:number) {
        if (this.decayAt && now >= this.decayAt) {
            this.alive = false
            
            if (game.player.openedContainer == this)
                game.player.openedContainer = null
        }
    }

    use(player:Player) {
        if (!this.alive) return 
        if (!player.alive) return
        
        if (player.game.frame.time < player.exhaustedTimer) {
            player.game.addMessageHistory('You are exhausted.')
            player.game.addComponent(new MagicEffect(player.pos, player.game, MagicEffect.E_SMOKE))
            return
        }
        
        const scriptType = (this.getDefinition().properties[ItemProperties.ITEM_SCRIPT])

        if (scriptType) {
            new scriptType().cast(this, player, player)
            player.exhaustedTimer = player.game.frame.time + 1000
        }
        else
            player.game.addMessageHistory('You cannot use this item.')
    }

    draw(game: Game, drawX:number, drawY:number, now:number) {
        if (!this.alive)
            return
        const ItemDefinition = itemsDefinition[this.id]
        const currentFrame = ItemDefinition.getCurrentFrame(now)

        let animationModifierY = 0
        let animationModifierX = 0

        if (this.animatePos != null) {
            this.animatePos+=4

            animationModifierY = (this.MAX_ANIMATION_FLY - Math.abs(this.animatePos))*1.5

            game.ctx.shadowOffsetX = GameConstants.GLOBAL_SHADOW_X * (animationModifierY/this.MAX_ANIMATION_FLY);
            game.ctx.shadowOffsetY = GameConstants.GLOBAL_SHADOW_Y * (animationModifierY/this.MAX_ANIMATION_FLY);
            game.ctx.shadowBlur = GameConstants.GLOBAL_SHADOW_BLUR * (animationModifierY/this.MAX_ANIMATION_FLY);
            game.ctx.shadowColor = GameConstants.GLOBAL_SHADOW_COLOR;

            switch (this.flyItem) {
                case Direction.RIGHT:
                    animationModifierX = ((this.MAX_ANIMATION_FLY - this.animatePos)/2)
                    break;
                case Direction.LEFT:
                    animationModifierX = ((this.MAX_ANIMATION_FLY*-1 + this.animatePos)/2)
                    break;
                case Direction.UP:
                    animationModifierY += ((this.MAX_ANIMATION_FLY*-1 + this.animatePos)/2)
                    break;
                case Direction.DOWN:
                    animationModifierY += ((this.MAX_ANIMATION_FLY - this.animatePos)/2)
                    break;
            }

            if (this.animatePos >= this.MAX_ANIMATION_FLY)
                this.animatePos = null

        } else if (this.getDefinition().castShadows) {
            game.ctx.shadowOffsetX = GameConstants.GLOBAL_SHADOW_X 
            game.ctx.shadowOffsetY = GameConstants.GLOBAL_SHADOW_Y 
            game.ctx.shadowBlur = GameConstants.GLOBAL_SHADOW_BLUR 
            game.ctx.shadowColor = GameConstants.GLOBAL_SHADOW_COLOR
        }
        
        if (this.decayAt && this.decayAt - now <= 2000) {
            game.ctx.globalAlpha = (this.decayAt - now) / 2000  //(this.DECAY_TIME - elapsedTime) / this.DECAY_TIME_START;
        }

        game.ctx.drawImage(itemsSpriteTable,
            0,
            ItemDefinition.frames[currentFrame] * GameConstants.SPRITE_REAL_RESOLUTION, 
            GameConstants.SPRITE_REAL_RESOLUTION,
            GameConstants.SPRITE_REAL_RESOLUTION,
            drawX - animationModifierX,
            drawY - animationModifierY,
            GameConstants.spriteCalculatedSize,
            GameConstants.spriteCalculatedSize)

        if (itemsDefinition[this.id].framesY) {
            game.ctx.drawImage(itemsSpriteTable,
                0,
                itemsDefinition[this.id].framesY[currentFrame] * GameConstants.SPRITE_REAL_RESOLUTION, 
                GameConstants.SPRITE_REAL_RESOLUTION,
                GameConstants.SPRITE_REAL_RESOLUTION,
                drawX - animationModifierX,
                drawY - animationModifierY - GameConstants.spriteCalculatedSize,
                GameConstants.spriteCalculatedSize,
                GameConstants.spriteCalculatedSize)
        }
        
        game.ctx.shadowOffsetX = 0;
        game.ctx.shadowOffsetY = 0;
        game.ctx.shadowBlur = 0;
        game.ctx.globalAlpha = 1
        
        if (itemsDefinition[this.id].properties[ItemProperties.LIGHT_COLOR])
            game.nightEffectManager.addLightEffect(
                { x: drawX - animationModifierX, y: drawY - animationModifierY}, 1, 
                itemsDefinition[this.id].properties[ItemProperties.LIGHT_COLOR])
    }    

    getDefinition() : ItemDefinition {
        return itemsDefinition[this.id]
    }
}


