export enum ItemFlags {
    NONE = 0,
    BLOCK_PATH = 1 << 0, // 0001
    PICKABLE = 1 << 1, // 0010
    USABLE = 1 << 2, // 0100
    CONTAINER = 1 << 3, // 1000
    BLOCK_PROJECTILE = 1 << 4
}

//let traits = Traits.Mean | Traits.Funny; // (0010 | 0100) === 0110
//let testFlags = ItemFlags.BLOCK_PATH | ItemFlags.PICKABLE
//testFlags = testFlags | ItemFlags.PICKABLE

//if (testFlags & ItemFlags.PICKABLE) //-> true