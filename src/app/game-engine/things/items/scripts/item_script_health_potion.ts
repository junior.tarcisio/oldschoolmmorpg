import { MagicEffect } from './../../../effects/magic-effect';
import { ItemScript } from "./item_script";
import Player from "../../player";
import { Item } from "../item";
import { Creature } from "../../creature";
import { ItemProperties } from '../items-definition';

export class ItemScriptHealthPotion extends ItemScript {

    getPower(item: Item) {
        const scriptPower = item.getDefinition().properties[ItemProperties.ITEM_SCRIPT_POWER]
        if (scriptPower)
            return scriptPower
        else
            return 50
    }

    cast(item: Item, caster: Player, target: Creature) {
        const fixedHeal = this.getPower(item)
        const randomHeal = (fixedHeal * 0.2 * Math.random())|0

        target.receiveHealing(fixedHeal + randomHeal)
        target.game.addComponent(new MagicEffect(target.pos, target.game,1))
        target.game.audioManager.SOUNDS.DRINK_1.play()
        item.alive = false
    }
}