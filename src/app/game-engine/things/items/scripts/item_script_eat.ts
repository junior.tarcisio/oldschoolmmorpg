
import { ItemScript } from "./item_script";
import Player from "../../player";
import { Item } from "../item";
import { Creature } from "../../creature";
import { ItemProperties } from '../items-definition';
import { MonsterSpeech } from '../../../effects/monster-speech';
import { ConditionType } from '../../../conditions/conditions-definition';

export class ItemScriptEat extends ItemScript {

    getPower(item: Item) {
        const scriptPower = item.getDefinition().properties[ItemProperties.ITEM_SCRIPT_POWER]
        if (scriptPower)
            return scriptPower
        else
            return 50
    }

    cast(item: Item, caster: Player, target: Creature) {
        const amount = this.getPower(item)

        const conditionResult = caster.addCondition(ConditionType.FEEDING, amount)

        if (conditionResult) {
            caster.game.addComponent(
                new MonsterSpeech(caster.pos, "Nhac...", caster.game))
    
            target.game.audioManager.SOUNDS.CHEW_01.play()
            
            item.alive = false
        }
    }
}