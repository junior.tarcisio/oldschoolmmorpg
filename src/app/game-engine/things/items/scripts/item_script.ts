import { Item } from "../item";
import Player from "../../player";
import { Creature } from "../../creature";

export abstract class ItemScript {
    abstract cast(item:Item, caster:Player, target:Creature)
}