import { DamageType } from './../../enumerables/damage-type';
import { HitBox } from './../../game';
import { Creature } from "../creature"
import { Game } from "../../game"
import { Direction } from "../../enumerables/direction"
import monstersDefinition, { MonsterDefinition, MovementType } from "./monster-definition"
import Position from "../../position";
import { MonsterSpeech } from "../../effects/monster-speech";
import { GameConstants } from "../../game-constants";
import { Item } from '../items/item';
import { MagicEffect } from '../../effects/magic-effect';
import { ItemFlags } from '../items/item-flags';
import spellsDefinition from '../../spells/spells-definition';

export class Monster extends Creature {
    monsterDefinition : MonsterDefinition //pointer vs ID? let's go on pointer now
    currentFrame = 0
        
    static readonly targetImg = new Image
    static readonly targetImgSmall = new Image
    static readonly THINK_INTERVAL = 15
    //readonly FRAME_INTERVAL = 80 //may come from monster definition

    //what about refacting it to a global number? less memory and more performatic
    thinkTimer = Date.now()
    frameTimer = Date.now()
    speechExhaustedTill = Date.now()
    moveCloseRandomTimer = Date.now()
    spellExhaustList = []
    dyingTimer : number
    dyingLoot 
    respawnPosition 

    alive = true
    path : Array<any> = []
    movingPercent = 0
    movingToDirection : Direction = null

    constructor(public monsterDefinitionID:number, position, game : Game) {
        super(position, game)
        this.respawnPosition = {x : position.x, y: position.y}
        this.monsterDefinition = monstersDefinition[monsterDefinitionID]
        this.loadMonster()
    }

    dropLoot() {
        let itemDirection = Math.random()*10 % 4 +1 |0 //from 1 - 4
        let lootItemPos 

        for (let i = 0; i < this.monsterDefinition.loots.length; i++) {
            if (Math.random() <= this.monsterDefinition.loots[i].chance) {

                if (itemDirection == Direction.LEFT) {
                    itemDirection = Direction.RIGHT
                    lootItemPos = {x: this.pos.x+1, y: this.pos.y}
                }
                else if (itemDirection == Direction.RIGHT) {
                    itemDirection = Direction.UP
                    lootItemPos = {x: this.pos.x, y: this.pos.y - 1}
                }
                else if (itemDirection == Direction.UP)  {
                    itemDirection = Direction.DOWN
                    lootItemPos = {x: this.pos.x, y: this.pos.y+1}
                } else {
                    itemDirection = Direction.LEFT
                    lootItemPos = {x: this.pos.x-1, y: this.pos.y}
                }

                let qtd = 1

                //const qtd = Math.ceil(this.loots[i].maxqtd * Math.random()) from 1 to max
                if (this.monsterDefinition.loots[i].maxqtd > 1) {
                    qtd = Math.ceil(this.monsterDefinition.loots[i].maxqtd * Math.random())
                } else {
                    qtd = null
                }

                const flyItem = Math.random() <= this.monsterDefinition.loots[i].flychance

                if (flyItem && 
                    this.game.map[lootItemPos.y] && 
                    this.game.map[lootItemPos.y][lootItemPos.x] && 
                    !this.game.map[lootItemPos.y][lootItemPos.x].movementBlockedIgnoreCreature())                
                    this.game.map[lootItemPos.y][lootItemPos.x].addItem(this.game, lootItemPos, new Item(this.monsterDefinition.loots[i].itemId, qtd, null, itemDirection))
                else  {                                
                    if (this.dyingLoot == null) 
                        this.dyingLoot = new Array()

                    this.dyingLoot.push(new Item(this.monsterDefinition.loots[i].itemId, qtd))
                }
            }
        }
    }

    loadMonster() {
        //max health, and others too? getters setters js/ts?
        //just current/mutable values are individual
        this.max_health = this.monsterDefinition.max_health
        this.health = this.monsterDefinition.max_health
        this.speed = this.monsterDefinition.speed
        this.target = this.game.player
    }

    getMagicEffectType() {
        return this.monsterDefinition.me_attack
    }

    getRunningFromDirection(pos) {
        let distX = Math.abs(this.pos.x - pos.x)
        let distY = Math.abs(this.pos.y - pos.y)
        
        if (distX >= distY){
            if (this.pos.x > pos.x) {

                if (this.canMoveTo(Direction.RIGHT)) {
                    return Direction.RIGHT
                } else {
                    if (this.pos.y > pos.y)
                        return Direction.DOWN
                    else
                        return Direction.UP
                }
            }
            else {
                if (this.canMoveTo(Direction.LEFT)) {
                    return Direction.LEFT
                } else {
                    if (this.pos.y > pos.y)
                        return Direction.DOWN
                    else
                        return Direction.UP
                }
            }
        }
        else {
            if (this.pos.y > pos.y) {
                if (this.canMoveTo(Direction.DOWN)) {
                    return Direction.DOWN
                } else {
                    if (this.pos.x > pos.x)
                        return Direction.RIGHT
                    else
                        return Direction.LEFT
                }
            }
            else {
                if (this.canMoveTo(Direction.UP)) {
                    return Direction.UP
                } else {
                    if (this.pos.x > pos.x)
                        return Direction.RIGHT
                    else
                        return Direction.LEFT
                }
            }
        }
    }

    update() {
        if (this.dyingTimer) {
            if (Date.now() - this.dyingTimer > 300)
                this.removeAndAddToRespawn()
            return
        }

        if (!this.alive) return 

        if (Date.now() - this.thinkTimer <= Monster.THINK_INTERVAL) return

        this.thinkTimer = Date.now()    

        if (this.game.player.god) return

        if (!this.target || !this.target.alive) {
            if (this.game.player.alive)
                this.target = this.game.player
        }

        if (this.target && this.distanceForShootTo(this.target.pos) <= this.monsterDefinition.attack_range) {    
            
            if (this.movingPercent == 0) {
                
                if (this.monsterDefinition.run_on_health && this.health <= this.monsterDefinition.run_on_health)
                    this.movingToDirection = this.getRunningFromDirection(this.target.pos)
                else if (this.distanceForShootTo(this.target.pos) < this.monsterDefinition.attack_range) 
                    this.movingToDirection = this.getRunningFromDirection(this.target.pos)
                else 
                    this.movingToDirection = this.getRandomCloseDirection(this.target.pos)                
            }        

            //target can be dead
            this.attack()
        }
        else if (this.movingPercent == 0) {

            //if target has changed the position to far, reset the pathfind            
            if (this.target) {
                
                if (this.monsterDefinition.run_on_health && this.health <= this.monsterDefinition.run_on_health) {
                    this.movingToDirection = this.getRunningFromDirection(this.target.pos)
                    this.path = []
                }
                else {
                    if (this.path.length > 0 && (
                        Math.abs(this.path[0].x - this.target.pos.x) > this.monsterDefinition.attack_range ||
                        Math.abs(this.path[0].y - this.target.pos.y) > this.monsterDefinition.attack_range))
                        this.path = []
                    
                    if (this.path.length == 0) {
    
                        if (this.distanceForShootTo(this.target.pos) < this.monsterDefinition.passivity_range) {
                            this.path = this.game.pathfind.findArray(this.pos, this.target.pos, 50, 1, this.monsterDefinition.movement_type)
                            if (this.path.length > 0)
                                this.path.pop()
                        }
                    }
                    
                    if (this.path.length > 0) 
                        this.movingToDirection = this.getDirectionToPosition(this.path.pop())
                    else 
                        this.movingToDirection = this.getRandomDirection()
                } 
            }
            else {
                this.movingToDirection = this.getRandomDirection()
            }
        }

        if (this.movingPercent == 0 && this.movingToDirection) {
            if (this.canMoveTo(this.movingToDirection, this.monsterDefinition.movement_type))
                this.moveToTileOnDirection(this.movingToDirection)    
            else
                this.movingToDirection = null
        }
        
        this.movingPercent += this.getSpeed() / 10  * this.getMapTile().getTile().speed
        
        if (this.movingPercent >= 100) {
            this.movingPercent = 0
            this.movingToDirection = null
        }

        this.randomSpeech()  
        this.castSpells()
        this.updateConditions()
        this.updateFrame()
    }


    getRandomCloseDirectionTimer() {
        if (MovementType.FLY) 
            return 0
        else
            return 1000
    }
    getRandomCloseDirection(posCloseTo) : Direction {
        const now = Date.now()

        if (now  - this.moveCloseRandomTimer < this.getRandomCloseDirectionTimer())
            return

        this.moveCloseRandomTimer = now
        const randomResult = Math.random()
        let direction : Direction

        if (MovementType.FLY) {
            if (randomResult < 0.25)
                direction = Direction.LEFT 
            else if (randomResult < 0.5)
                direction = Direction.RIGHT 
            else if (randomResult < 0.75)
                direction = Direction.UP 
            else
                direction = Direction.DOWN 
        } else {

            if (randomResult < 0.2)
                direction = Direction.LEFT 
            else if (randomResult < 0.4)
                direction = Direction.RIGHT 
            else if (randomResult < 0.6)
                direction = Direction.UP 
            else if (randomResult < 0.8)
                direction = Direction.DOWN 
            else
                return null
        }


        const pos = Position.getSubjacentPOS(this.pos, direction)

        if (Math.abs(pos.x - posCloseTo.x) <= 1 && Math.abs(pos.y - posCloseTo.y) <= 1)
            return direction
        else 
            return null
    }

    updateFrame() {
        
        if (Date.now() - this.frameTimer > this.monsterDefinition.frame_timer) {
            if (this.movingToDirection != null || this.monsterDefinition.movement_type == MovementType.FLY) {
                this.frameTimer = Date.now()        
                this.currentFrame++
                
                if (this.currentFrame >= this.monsterDefinition.frames.length)
                    this.currentFrame = 0
            } else {
                this.currentFrame = 0
            }
        }
    }

    randomSpeech() {

        if (!this.monsterDefinition.speeches || this.monsterDefinition.speeches.length == 0) 
            return 

        const now = Date.now()
        
        if (now < this.speechExhaustedTill)
            return

        const randomResult = Math.random()

        for (let i = 0; i < this.monsterDefinition.speeches.length; i++) {
            const speech = this.monsterDefinition.speeches[i]
            if (randomResult <= speech.chance) {
                this.game.addComponent(
                    new MonsterSpeech(this.pos, speech.text, this.game))
                this.speechExhaustedTill = now + speech.interval
                
                if (speech.sound)
                    this.game.audioManager.SOUNDS[speech.sound].play()
                return
            }
        }

        this.speechExhaustedTill = now + 1000
    }

    castSpells() {

        if (!this.monsterDefinition.spells || this.monsterDefinition.spells.length == 0) 
            return

        const now = this.game.frame.time

        for (let i = 0; i < this.monsterDefinition.spells.length; i++) {            
            if (now < this.spellExhaustList[i])
                continue

            const spell = this.monsterDefinition.spells[i]

            if (spell.chance >= 1 || Math.random() <= spell.chance) {
                spellsDefinition[spell.spellType].cast(this,this, spell.formula)
            }
            
            this.spellExhaustList[i] = now + spell.interval
        }
    }
    
    getPixelPositionOnScreen() {
        let pos = this.game.gamePositionToScreenPixelsPosition(this.pos)
        
        let drawX = 0, drawY = 0, drawModifier = 0
    
        drawModifier = ((this.movingPercent / 100) * GameConstants.spriteCalculatedSize) - GameConstants.spriteCalculatedSize
    
        switch (this.movingToDirection) {
            case Direction.UP:            
                drawY = drawModifier * -1
                break
            case Direction.DOWN:
                drawY = drawModifier
                break
            case Direction.LEFT:
                drawX = drawModifier * -1
                break
            case Direction.RIGHT:
                drawX = drawModifier
                break
            default:
                break
        }

        pos.x += drawX
        pos.y += drawY

        return pos
    }

    removeAndAddToRespawn() {
        this.game.addComponent(new MagicEffect(this.pos, this.game,8));
        this.game.map[this.pos.y][this.pos.x].removeCreature(this)  

        let keepGoing = true;

        while (keepGoing) {
            const sqmItemList = this.game.map[this.pos.y][this.pos.x].itemList

            if (!sqmItemList || sqmItemList.length == 0) {
                keepGoing = false
                break
            }

            const item = sqmItemList[sqmItemList.length-1]

            if (item.getDefinition().flags & ItemFlags.PICKABLE) {
                if (!this.dyingLoot)
                    this.dyingLoot = new Array()
                    
                this.dyingLoot.push(sqmItemList.pop())
            }
            else if (item.getDefinition().flags & ItemFlags.CONTAINER && item.decayAt != null && item.content) {
                if (!this.dyingLoot)
                    this.dyingLoot = new Array()

                this.dyingLoot = this.dyingLoot.concat(item.content)
                item.content = null
                keepGoing = false
                break
            } else {
                keepGoing = false
                break
            }
        }

        this.game.map[this.pos.y][this.pos.x].addItem(this.game, this.pos, new Item(this.monsterDefinition.body_item_id,null,null,null,30000, this.dyingLoot))
        this.dyingLoot = null

        this.dyingTimer = null
        this.alive = false        
        this.target = null
        this.frameTimer = null
        this.movingToDirection = null
        this.thinkTimer = null
        this.game.addMonsterToRespawn(this)
        
        // this.cleanObjectForGC() 
    }
    
    respawn() {
        this.loadMonster()
        this.pos.x = this.respawnPosition.x
        this.pos.y = this.respawnPosition.y
        this.game.map[this.pos.y][this.pos.x].addCreature(this)  
        this.game.addComponent(new MagicEffect({x:this.pos.x,y:this.pos.y}, this.game, 4)) 
        this.alive = true      
    }

    die(attacker:Creature) {
        super.die(attacker)
        this.game.audioManager.SOUNDS.MONSTER_1.play()
        attacker.receiveExperience(this.monsterDefinition.experience)
        this.dropLoot()
        this.dyingTimer = Date.now()
    }

    receiveDamage(attacker:Creature, amount:number, type : DamageType) {
        if (this.dyingTimer)
            return
        super.receiveDamage(attacker, amount, type)
        //this.target = attacker
    }
    
    sumUpAttackAnimation(calculatedCameraAcceleration) {

        const attackModifier = Date.now() - this.lastAttack
        let attackModifierCalc = 0
        if (attackModifier < 200) {
            attackModifierCalc =  (100 - (Math.abs(100 - attackModifier)))/10*2
        }

        //1st just set the direction here...
        if (this.target && this.target.alive) {
            
            if (this.pos.x == this.target.pos.x ) { 
                    
                if (this.pos.y > this.target.pos.y) { // up /\
                    calculatedCameraAcceleration.y += attackModifierCalc

                } else { // down \/
                    calculatedCameraAcceleration.y -= attackModifierCalc
                }

            } else if (this.pos.x > this.target.pos.x) {
                if (this.pos.y == this.target.pos.y) { // > right
                    calculatedCameraAcceleration.x += attackModifierCalc
                    calculatedCameraAcceleration.y += attackModifierCalc/2
                } 
                else if (this.pos.y > this.target.pos.y) { // > \/ right bottom
                    calculatedCameraAcceleration.x += attackModifierCalc
                    calculatedCameraAcceleration.y += attackModifierCalc

                } else { // > /\ right up
                    calculatedCameraAcceleration.x += attackModifierCalc
                    calculatedCameraAcceleration.y -= attackModifierCalc

                }

            } else { //this.pos.x < this.target.pos.x
                if (this.pos.y == this.target.pos.y) { // < left
                    calculatedCameraAcceleration.x -= attackModifierCalc
                    calculatedCameraAcceleration.y += attackModifierCalc/2

                } 
                else if (this.pos.y > this.target.pos.y) { // < \/ left bottom
                    calculatedCameraAcceleration.x -= attackModifierCalc
                    calculatedCameraAcceleration.y += attackModifierCalc

                } else { // < /\ left up
                    calculatedCameraAcceleration.x -= attackModifierCalc
                    calculatedCameraAcceleration.y -= attackModifierCalc
                }
            }

        }

        //jump 4 steps
        //first make the 4 steps, then think about dir up and down
    }

    draw() {
        if (!this.alive) 
            return

        if (!this.dyingTimer)
            super.draw(this.monsterDefinition.hpbar_pos_modifier)

        const calculatedCameraAcceleration = {x:0,y:0}
        this.sumUpAttackAnimation(calculatedCameraAcceleration)
    
        const screenPixelsPosition = this.getPixelPositionOnScreen()
    
        this.game.ctx.shadowOffsetX = GameConstants.GLOBAL_SHADOW_X
        this.game.ctx.shadowOffsetY = GameConstants.GLOBAL_SHADOW_Y
        this.game.ctx.shadowBlur = GameConstants.GLOBAL_SHADOW_BLUR
        this.game.ctx.shadowColor = GameConstants.GLOBAL_SHADOW_COLOR
        

        if (this.monsterDefinition.transparency)
            this.game.ctx.globalAlpha = this.monsterDefinition.transparency


        this.game.ctx.drawImage(this.monsterDefinition.frames[this.currentFrame],
            screenPixelsPosition.x - (GameConstants.spriteCalculatedSize * (this.monsterDefinition.size.x - 1)) - calculatedCameraAcceleration.x,
            screenPixelsPosition.y - (GameConstants.spriteCalculatedSize * (this.monsterDefinition.size.y - 1)) - calculatedCameraAcceleration.y,
            GameConstants.spriteCalculatedSize * this.monsterDefinition.size.x, 
            GameConstants.spriteCalculatedSize * this.monsterDefinition.size.y)

        this.game.ctx.shadowOffsetX = 0;
        this.game.ctx.shadowOffsetY = 0;
        this.game.ctx.shadowBlur = 0;

        //TODO: if it works well, refactor
        this.game.hitboxes.push(new HitBox(this, 
            screenPixelsPosition.x - (GameConstants.spriteCalculatedSize * (this.monsterDefinition.size.x - 1)),
            GameConstants.spriteCalculatedSize * this.monsterDefinition.size.x, 
            screenPixelsPosition.y - (GameConstants.spriteCalculatedSize * (this.monsterDefinition.size.y - 1)),
            GameConstants.spriteCalculatedSize * this.monsterDefinition.size.y))

        if (this.game.player.target && this.game.player.target === this) {

            this.game.ctx2.globalAlpha = 0.8 - 0.8 * Math.abs(((this.game.frame.time % 2000 / 2000) - 0.5))
            this.game.ctx2.drawImage(Monster.targetImg,
                screenPixelsPosition.x,
                screenPixelsPosition.y,
                GameConstants.spriteCalculatedSize, 
                GameConstants.spriteCalculatedSize)
            this.game.ctx2.globalAlpha = 1
        }

        this.game.ctx.globalAlpha = 1

        if (this.monsterDefinition.light)
            this.game.nightEffectManager.addLightEffect(screenPixelsPosition,1, this.monsterDefinition.light)
    }

    drawWithFocus(screenPixelsPosition) {
        if (this.dyingTimer) return

        const calculatedCameraAcceleration = {x:0,y:0}
        this.sumUpAttackAnimation(calculatedCameraAcceleration)

        this.game.ctx2.drawImage(this.monsterDefinition.frames[this.currentFrame],
            screenPixelsPosition.x - (GameConstants.spriteCalculatedSize * (this.monsterDefinition.size.x - 1)) - calculatedCameraAcceleration.x,
            screenPixelsPosition.y - (GameConstants.spriteCalculatedSize * (this.monsterDefinition.size.y - 1)) - calculatedCameraAcceleration.y,
            GameConstants.spriteCalculatedSize * this.monsterDefinition.size.x, 
            GameConstants.spriteCalculatedSize * this.monsterDefinition.size.y)
            
        if (this.game.player.target && this.game.player.target === this) {

            this.game.ctx2.globalAlpha = 0.8 - 0.8 * Math.abs(((this.game.frame.time % 2000 / 2000) - 0.5))
            this.game.ctx2.drawImage(Monster.targetImg,
                screenPixelsPosition.x,
                screenPixelsPosition.y,
                GameConstants.spriteCalculatedSize, 
                GameConstants.spriteCalculatedSize)
            this.game.ctx2.globalAlpha = 1
        }
        
        super.draw(this.monsterDefinition.hpbar_pos_modifier)
    }
    
    getName(): string {
        return this.monsterDefinition.name
    }
    
    getAttackDamage(): number {
        return this.monsterDefinition.attack_damage
    }
    
    getArmor(): number {
        return this.monsterDefinition.armor
    }
    
    getAttackShootEffect() : number {
        return this.monsterDefinition.attack_shoot_effect
    }
}

Monster.targetImg.src = "assets/ico_target.png"
Monster.targetImgSmall.src = "assets/ico_target_Y.png"