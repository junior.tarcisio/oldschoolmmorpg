import { ShootEffect } from './../../effects/shoot-effect';
import { DamageType } from './../../enumerables/damage-type';
import { MagicEffect } from './../../effects/magic-effect';
import { SpellBasicAttack } from './../../spells/spell-basic-attack';
import { SpellType } from './../../spells/spells-definition'
import { LightColorRGB } from './../../enumerables/rgb-color'
import { SpellLightHealing } from './../../spells/spell-light-healing'
import { ManagedAudio } from '../../audio-manager'
import { SpellHaste } from '../../spells/spell-haste'
import { SpellGreatFireBall } from '../../spells/great-fire-ball';
import { SpellUltimateExplosion } from '../../spells/ultimate-explosion';
import { SpellWhirlWind } from '../../spells/whirl-wind';
import { SpellParalyze } from '../../spells/spell-paralyze';

export class MonsterLoot {
    itemId : number
    chance : number
    maxqtd : number
    flychance : number = 0.5 //0 -> 1
}

export class MonsterSpeech {
    text : string
    chance : number
    interval : number
    sound: string
}

export class MonsterSpell {
    spellType : SpellType
    chance : number
    interval : number
    formula : any
}

export enum MovementType {
    WALK,
    FLY, //can pass over shootover
    GHOST // can pass over anything, except other monsters ghosts?
}

export class MonsterDefinition {
    public name : string = 'unnamed'
    public max_health : number = 100
    public attack_damage : number = 10
    public armor : number = 0
    public speed : number = 20
    public experience : number = 5
    public movement_type : MovementType
    public body_item_id : 2
    public light : LightColorRGB = null
    public shadow_override = null
    public transparency : number
    public frame_timer = 80
    public size : {x: number, y : number} = { x: 1, y : 1}
    public hpbar_pos_modifier : {x:number, y:number} = { x: 0, y : 0}
    public me_attack = 5
    public attack_shoot_effect = null
    public attack_range = 1
    public passivity_range = 7
    public run_on_health : number = null

    //first prototype it's ok to continue using N sprites for each one
    //get from a unique sprite file, passing the positions only
    public frames : Array<HTMLImageElement> = []
    public sounds : Array<ManagedAudio> = []
    public loots : Array<MonsterLoot> = []
    public speeches : Array<MonsterSpeech>  = []
    public spells : Array<MonsterSpell> = []
    
    constructor(props) {
        for(let key in props) {
            if (key == "frames") {
                props.frames.forEach(url => {
                    this.frames.push(new Image)
                    this.frames[this.frames.length-1].src = url
                });                
            }                
            else {
                this[key] = props[key]
            }
        }

        this.loots.forEach(loot => { 
            if (!loot.flychance )
                loot.flychance = 0.5            
        });
    }
}

const monstersDefinition = new Array<MonsterDefinition>()
monstersDefinition[0] = new MonsterDefinition({
    name : 'Panda',
    max_health : 280,
    attack_damage : 150,
    speed: 30,
    experience : 245,
    movement_type : MovementType.WALK,
    light : null,
    shadow_override : null,
    body_item_id : 2,
    me_attack : 7,
    frames : [
        "assets/panda_00.png",
        "assets/panda_01.png",
        "assets/panda_02.png"
    ],
    sounds : [ //TODO
        { 'die' : 'MONSTER_1', chance : 100 },
        { 'step' : '', chance : 100 },
        { 'step_alt' : '', chance : 100  },
        { 'random' : '', chance : 20  } //   'die' : this.game.audioManager.SOUNDS.MONSTER_1
    ],
    loots : [ 
        { itemId: 1, chance: 0.25, maxqtd : 1},
        { itemId: 40, chance: 0.5, maxqtd : 1},
        { itemId: 41, chance: 0.5, maxqtd : 1},
        { itemId: 62, chance: 0.5, maxqtd : 10},
        { itemId: 75, chance: 0.25, maxqtd : 1}, // durian
        { itemId: 76, chance: 0.25, maxqtd : 1}, // dragon fruit
        { itemId: 74, chance: 0.25, maxqtd : 1}, // nasi goreng telur
        { itemId: 72, chance: 0.25, maxqtd : 1}, // nastar
        { itemId: 71, chance: 0.25, maxqtd : 1}, // ketupat
        { itemId: 69, chance: 0.25, maxqtd : 1}, // bakso
        { itemId: 67, chance: 0.25, maxqtd : 1}, // sate packet
        { itemId: 66, chance: 0.25, maxqtd : 1} // sate
    ],
    speeches : [  //TODO
        { text : 'Some honey always worth!!!', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'HOOOOOOWWNNN', chance : 0.08, interval : 5000, sound: null, color:null },
    ]
})

monstersDefinition[1] = new MonsterDefinition({
    name : 'Pocong',
    max_health : 160,
    attack_damage : 70,
    speed: 25,
    experience : 80,
    movement_type : MovementType.WALK,
    light : null,
    shadow_override : null,
    body_item_id : 6,
    me_attack : MagicEffect.E_SLASH,
    attack_range : 1,
    passivity_range : 6,
    run_on_health : 50,
    frames : [
        "assets/pocong_00.png",
        "assets/pocong_01.png",
        "assets/pocong_02.png",
        "assets/pocong_01.png"
    ],
    loots : [ 
        { itemId: 1, chance: 0.2, maxqtd : 1},
        { itemId: 40, chance: 0.4, maxqtd : 1},
        { itemId: 41, chance: 0.4, maxqtd : 1},
        { itemId: 62, chance: 1, maxqtd : 10, flychance : 0 },
        { itemId: 67, chance: 0.25, maxqtd : 1} ,// sate packet
        { itemId: 66, chance: 0.25, maxqtd : 1} // sate
    ],
    speeches : [  //TODO
        { text : 'Groooooarrrrrrr', chance : 0.05, interval : 5000, sound: 'MONSTER_POC_00', color:null },
        { text : 'Buuuuuuhhhhwaaa', chance : 0.1, interval : 5000, sound: 'MONSTER_POC_00', color:null },
    ],
    spells : [ 
        { spellType : SpellType.BASIC_ATTACK, chance : 0.6, interval : 3000, formula : SpellBasicAttack.createFormula(20, 35, MagicEffect.E_BITE, DamageType.DEATH, 2, null) },
        { spellType : SpellType.HEALING, chance : 0.35, interval : 2500, formula : SpellLightHealing.createFormula(10, 20) }
    ]
})

monstersDefinition[2] = new MonsterDefinition({
    name : 'Kuntilanak',
    max_health : 220,
    attack_damage : 100,
    speed: 35,
    experience : 140,
    movement_type : MovementType.WALK,
    light : null,
    transparency: 1,
    shadow_override : null,
    body_item_id : 7,
    me_attack : 6,
    frames : [
        "assets/kuntilanak_00.png",
        "assets/kuntilanak_01.png",
        "assets/kuntilanak_02.png"
    ],
    loots : [ 
        { itemId: 1, chance: 0.2, maxqtd : 1},
        { itemId: 40, chance: 0.4, maxqtd : 1},
        { itemId: 41, chance: 0.4, maxqtd : 1},
        { itemId: 62, chance: 0.5, maxqtd : 10},
        { itemId: 69, chance: 0.25, maxqtd : 1}, // bakso
    ],
    speeches : [  
        { text : 'Hihihihihi....', chance : 0.1, interval : 3000, sound: 'MONSTER_KA_00', color:null },
        { text : 'Hihihi...', chance : 0.2, interval : 3000, sound: 'MONSTER_KA_01', color:null },
        { text : 'HIHIhihihi!!!!', chance : 0.3, interval : 3000, sound: 'MONSTER_KA_02', color:null }
    ],
    spells : [ 
        { spellType : SpellType.HEALING, chance : 0.75, interval : 2000, formula : SpellLightHealing.createFormula(50, 70) },
        { spellType : SpellType.BASIC_ATTACK, chance : 0.6, interval : 2500, formula : SpellBasicAttack.createFormula(30, 45, MagicEffect.E_BITE, DamageType.DEATH, 4, null) },
    ]
})

monstersDefinition[3] = new MonsterDefinition({
    name : 'Soul Reaper',
    max_health : 155,
    attack_damage : 170,
    speed: 35,
    experience : 140,
    movement_type : MovementType.WALK,
    light : null,
    shadow_override : null,
    body_item_id : 8,
    frames : [
        "assets/reaper_00.png",
        "assets/reaper_01.png",
        "assets/reaper_02.png"
    ],
    loots : [ 
        { itemId: 1, chance: 0.2, maxqtd : 1},
        { itemId: 40, chance: 0.4, maxqtd : 1},
        { itemId: 41, chance: 0.4, maxqtd : 1},
        { itemId: 62, chance: 0.5, maxqtd : 10},
        { itemId: 71, chance: 0.25, maxqtd : 1}, // ketupat
        { itemId: 69, chance: 0.25, maxqtd : 1}, // bakso
        { itemId: 67, chance: 0.25, maxqtd : 1}, // sate packet
        { itemId: 66, chance: 0.25, maxqtd : 1} // sate
    ],
    speeches : [  //TODO
        { text : 'COME WITH ME, NOW!', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'YOUR TIME HAS FINISHED', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [
        { spellType : SpellType.HASTE, chance : 0.2, interval : 5000, formula : SpellHaste.createFormula(200, 1000)}
    ]
})

monstersDefinition[4] = new MonsterDefinition({
    name : 'Unholy Crusader',
    max_health : 625,
    attack_damage : 250,
    speed: 45,
    experience : 400,
    movement_type : MovementType.WALK,
    light : null,
    shadow_override : null,
    body_item_id : 9,
    armor: 40,
    frames : [
        "assets/crusader_00.png",
        "assets/crusader_01.png",
        "assets/crusader_02.png"
    ],
    loots : [ 
        { itemId: 10, chance: 0.25, maxqtd : 1},
        { itemId: 11, chance: 0.01, maxqtd : 1},
        { itemId: 40, chance: 0.4, maxqtd : 1},
        { itemId: 41, chance: 0.4, maxqtd : 1},
        { itemId: 62, chance: 0.5, maxqtd : 100},
        { itemId: 71, chance: 0.25, maxqtd : 1}, // ketupat
    ],
    speeches : [  //TODO
        { text : 'IN THE NAME OF THE ORDER!', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'YOUR SINS WILL BE PUNISHED', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [
       { spellType : SpellType.HEALING, chance : 0.2, interval : 5000, formula : SpellLightHealing.createFormula(20, 40)},       
       { spellType : SpellType.CHAIN_LIGHTNING, chance : 0.1, interval : 8000, formula : SpellLightHealing.createFormula(40, 110)}
   //     { spellType : WhirlWind, chance : 0.2, interval : 5000 }
    ]
})


monstersDefinition[5] = new MonsterDefinition({
    name : 'Barbarian',
    max_health : 1300,
    attack_damage : 420,
    speed: 55,
    experience : 1100,
    movement_type : MovementType.WALK,
    light : null,
    shadow_override : null,
    body_item_id : 12,
    armor: 60,
    frames : [
        "assets/player_00.png",
        "assets/player_01.png",
        "assets/player_02.png"
    ],
    loots : [ 
        { itemId: 10, chance: 0.25, maxqtd : 1},
        { itemId: 11, chance: 0.05, maxqtd : 1},
        { itemId: 62, chance: 0.5, maxqtd : 100}
    ],
    speeches : [  //TODO
        { text : 'IN THE NAME OF VALHALA', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'I WANT YOUR LOOT', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
     spells : [
        { spellType : SpellType.HASTE, chance : 0.2, interval : 5000, formula : SpellHaste.createFormula(40, 5000)},
        { spellType : SpellType.WHIRLD_WILD, chance : 0.1, interval : 6000, formula : SpellHaste.createFormula(200, 300)}
     ]
})


monstersDefinition[6] = new MonsterDefinition({
    name : 'Skeleton',
    max_health : 50,
    attack_damage : 50,
    speed: 20,
    experience : 25,
    movement_type : MovementType.WALK,
    light : null,
    shadow_override : null,
    body_item_id : 32,
    me_attack : 6,
    passivity_range : 7,
    frames : [
        "assets/skellx_00.png",
        "assets/skellx_01.png",
        "assets/skellx_02.png"
    ],
    loots : [ 
        { itemId: 40, chance: 0.25, maxqtd : 1},
        { itemId: 41, chance: 0.25, maxqtd : 1},
        { itemId: 62, chance: 0.5, maxqtd : 10},
        { itemId: 66, chance: 0.25, maxqtd : 1} // sate
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        // { itemId: 11, chance: 0.05, maxqtd : 1}
    ],
    // speeches : [  //TODO
    //     { text : 'IN THE NAME OF VALHALA', chance : 0.04, interval : 5000, sound: null, color:null },
    //     { text : 'I WANT YOUR LOOT', chance : 0.08, interval : 5000, sound: null, color:null },
    // ]
})

monstersDefinition[7] = new MonsterDefinition({
    name : 'Banaspati',
    max_health : 50,
    attack_damage : 150,
    speed: 60,
    experience : 90,
    movement_type : MovementType.WALK,
    light : LightColorRGB.RED,
    shadow_override : null,
    body_item_id : 34,
    frames : [
        "assets/banaspati_00.png",
        "assets/banaspati_01.png",
        "assets/banaspati_02.png"
    ],
    loots : [ 
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        // { itemId: 11, chance: 0.05, maxqtd : 1}
    ],
    // speeches : [  //TODO
    //     { text : 'IN THE NAME OF VALHALA', chance : 0.04, interval : 5000, sound: null, color:null },
    //     { text : 'I WANT YOUR LOOT', chance : 0.08, interval : 5000, sound: null, color:null },
    // ]
})

monstersDefinition[8] = new MonsterDefinition({
    name : 'Giant Dragonfly',
    max_health : 30,
    attack_damage : 45,
    speed: 120,
    experience : 5,
    movement_type : MovementType.FLY,
    body_item_id : 35,
    frame_timer : 15,
    passivity_range : 2,
    run_on_health : 10,
    frames : [
        "assets/dragonfly_00.png",
        "assets/dragonfly_01.png",
        "assets/dragonfly_02.png"
    ],
    loots : [ 
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        // { itemId: 11, chance: 0.05, maxqtd : 1}
    ],
    speeches : [  
        { text : 'Zzzzzzzhhhhh', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [
        { spellType : SpellType.HASTE, chance : 0.2, interval : 5000, formula : SpellHaste.createFormula(100, 2000)}
    ]
})

monstersDefinition[9] = new MonsterDefinition({
    name : 'Giant Dragonfly',
    max_health : 30,
    attack_damage : 45,
    speed: 120,
    experience : 5,
    movement_type : MovementType.FLY,
    body_item_id : 36,
    frame_timer : 15,
    passivity_range : 2,
    run_on_health : 10,
    frames : [
        "assets/dragonflyR_00.png",
        "assets/dragonflyR_01.png",
        "assets/dragonflyR_02.png"
    ],
    loots : [ 
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        // { itemId: 11, chance: 0.05, maxqtd : 1}
    ],
    speeches : [  
        { text : 'Zzzzzzzhhhhh', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [
        { spellType : SpellType.HASTE, chance : 0.2, interval : 5000, formula : SpellHaste.createFormula(100, 2000)}
    ]
})

monstersDefinition[10] = new MonsterDefinition({
    name : 'Giant Dragonfly',
    max_health : 30,
    attack_damage : 45,
    speed: 120,
    experience : 5,
    movement_type : MovementType.FLY,
    body_item_id : 37,
    frame_timer : 15,
    passivity_range : 2,
    run_on_health : 10,
    frames : [
        "assets/dragonflyG_00.png",
        "assets/dragonflyG_01.png",
        "assets/dragonflyG_02.png",
        "assets/dragonflyG_01.png"
    ],
    loots : [ 
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        // { itemId: 11, chance: 0.05, maxqtd : 1}
    ],
    speeches : [  
        { text : 'Zzzzzzzhhhhh', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [
        { spellType : SpellType.HASTE, chance : 0.2, interval : 5000, formula : SpellHaste.createFormula(100, 2000)}
    ]
})

monstersDefinition[11] = new MonsterDefinition({
    name : 'Butterfly',
    max_health : 25,
    attack_damage : 45,
    speed: 45,
    experience : 5,
    movement_type : MovementType.FLY,
    body_item_id : 33,
    frame_timer : 60,
    passivity_range : 2,
    run_on_health : 10,
    frames : [
        "assets/buttlefly_00.png",
        "assets/buttlefly_01.png",
        "assets/buttlefly_02.png"
    ],
    loots : [ 
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        // { itemId: 11, chance: 0.05, maxqtd : 1}
    ],
    speeches : [  
        { text : 'Vrrriiiiiiiihhhhh', chance : 0.08, interval : 5000, sound: null, color:null },
    ]
})

monstersDefinition[12] = new MonsterDefinition({
    name : 'Java Spider',
    max_health : 150,
    attack_damage : 110,
    speed: 45,
    experience : 110,
    movement_type : MovementType.WALK,
    body_item_id : 31,
    frame_timer : 60,
    passivity_range : 5,
    frames : [
        "assets/spider_00.png",
        "assets/spider_01.png",
        "assets/spider_00.png",
        "assets/spider_02.png"
    ],
    loots : [ 
        { itemId: 62, chance: 0.5, maxqtd : 40},
        { itemId: 62, chance: 0.5, maxqtd : 40},
        { itemId: 62, chance: 0.5, maxqtd : 40}
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        // { itemId: 11, chance: 0.05, maxqtd : 1}
    ],
    speeches : [  
        { text : 'Shhhhhhhh', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [
        { spellType : SpellType.HASTE, chance : 0.2, interval : 5000, formula : SpellHaste.createFormula(40, 5000)}
    ]
})

monstersDefinition[13] = new MonsterDefinition({
    name : 'Queen Ant',
    max_health : 150,
    attack_damage : 90,
    speed: 30,
    experience : 90,
    movement_type : MovementType.WALK,
    body_item_id : 30,
    frame_timer : 60,
    me_attack : 6,
    passivity_range : 5,
    run_on_health : 50,
    frames : [
        "assets/ant_00.png",
        "assets/ant_01.png",
        "assets/ant_02.png"
    ],
    loots : [ 
        { itemId: 62, chance: 0.5, maxqtd : 100},
        { itemId: 72, chance: 0.25, maxqtd : 1}, // nastar
        { itemId: 71, chance: 0.25, maxqtd : 1}, // ketupat
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        // { itemId: 11, chance: 0.05, maxqtd : 1}
    ],
    speeches : [  
        { text : 'The kingdom of ants will rule this world', chance : 0.08, interval : 5000, sound: null, color:null },
    ]
})

monstersDefinition[14] = new MonsterDefinition({
    name : 'Banaspati',
    max_health : 50,
    attack_damage : 155,
    speed: 100,
    experience : 5,
    movement_type : MovementType.WALK,
    light : LightColorRGB.RED,
    body_item_id : 34,
    frame_timer : 60,
    passivity_range : 3,
    frames : [
        "assets/banaspati2_00.png",
        "assets/banaspati2_01.png",
        "assets/banaspati2_02.png"
    ],
    loots : [ 
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
    ],
    speeches : [  
        { text : 'RUN RUN RUN!!!', chance : 0.3, interval : 5000, sound: null, color:null },
        { text : 'RUN OR BURN WITH ME FOREVER!', chance : 0.6, interval : 5000, sound: null, color:null },
        { text : 'I WILL NOT WARN AGAIN!', chance : 0.9, interval : 5000, sound: null, color:null },
    ],
    spells : [        
        { spellType : SpellType.HASTE, chance : 1, interval : 5000, formula : SpellHaste.createFormula(50, 5000)},        
        { spellType : SpellType.GREAT_FIRE_BALL, chance : 0.2, interval : 6000, formula : SpellGreatFireBall.createFormula(100,200)}
    ]
})


monstersDefinition[15] = new MonsterDefinition({
    name : 'Shiny Kuntilanak',
    max_health : 1250,
    attack_damage : 120,
    speed: 45,
    experience : 820,
    movement_type : MovementType.GHOST,
    light : LightColorRGB.BLUE,
    transparency: 0.333,
    shadow_override : null,
    body_item_id : 7,
    me_attack : 6,
    armor: 20,
    frames : [
        "assets/kuntilanak_00.png",
        "assets/kuntilanak_01.png",
        "assets/kuntilanak_02.png"
    ],
    loots : [ 
        { itemId: 1, chance: 0.25, maxqtd : 1},
        { itemId: 62, chance: 0.5, maxqtd : 100},
        { itemId: 69, chance: 0.25, maxqtd : 1}, // bakso
        { itemId: 62, chance: 1, maxqtd : 30, flychance : 0 },
        { itemId: 62, chance: 1, maxqtd : 30, flychance : 0 },
        { itemId: 62, chance: 1, maxqtd : 30, flychance : 0 },
    ],
    speeches : [  
        { text : 'Hihihihihi....', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'Grreeeewwww...', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [ 
        { spellType : SpellType.HEALING, chance : 0.5, interval : 3000, formula : SpellLightHealing.createFormula(20,200)},
        { spellType : SpellType.CHAIN_LIGHTNING, chance : 0.5, interval : 3000, formula : SpellGreatFireBall.createFormula(100,200)},
    ]
})

monstersDefinition[16] = new MonsterDefinition({
    name : 'Dragon',
    max_health : 3000,
    attack_damage : 400,
    speed: 30,
    experience : 2500,
    movement_type : MovementType.WALK,
    light : null,
    transparency: 1,
    shadow_override : null,
    body_item_id : 34,
    size : { x: 2, y : 2},
    hpbar_pos_modifier : {x:0, y:25},
    me_attack : 7,
    run_on_health : 1000,
    armor: 60,
    frames : [
        "assets/dragonx_00.png",
        "assets/dragonx_01.png",
        "assets/dragonx_02.png"
    ],
    loots : [ 
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 1, chance: 0.25, maxqtd : 1, flychance: 1},
        { itemId: 62, chance: 0.5, maxqtd : 100, flychance: 1},
        { itemId: 75, chance: 0.25, maxqtd : 1, flychance: 1}, // durian
        { itemId: 76, chance: 0.25, maxqtd : 1, flychance: 1}, // dragon fruit
    ],
    speeches : [  
        { text : 'Hihihihihi....', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'Grreeeewwww...', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    // spells : [ 
    //     { spellType : SpellLightHealing, chance : 0.25, interval : 2000, min : 45, max: 65 } //as MonsterSpell
    // ]
})

monstersDefinition[17] = new MonsterDefinition({
    name : 'Horned Dragon',
    max_health : 6000,
    attack_damage : 500,
    speed: 30,
    experience : 2500,
    movement_type : MovementType.WALK,
    light : LightColorRGB.RED,
    transparency: 1,
    shadow_override : null,
    body_item_id : 34,
    size : { x: 2, y : 2},
    hpbar_pos_modifier : {x:0, y:25},
    me_attack : 7,
    run_on_health : 1500,
    armor: 60,
    frames : [
        "assets/dragon_horn_00.png",
        "assets/dragon_horn_01.png",
        "assets/dragon_horn_02.png"
    ],
    loots : [ 
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 1, chance: 0.25, maxqtd : 1, flychance: 1},
        { itemId: 62, chance: 0.5, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.75, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.75, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.55, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.5, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.5, maxqtd : 100, flychance: 1},
        { itemId: 75, chance: 0.25, maxqtd : 1, flychance: 1}, // durian
        { itemId: 76, chance: 0.55, maxqtd : 1, flychance: 1}, // dragon fruit
        { itemId: 76, chance: 0.55, maxqtd : 1, flychance: 1}, // dragon fruit
        { itemId: 76, chance: 0.55, maxqtd : 1, flychance: 1}, // dragon fruit
    ],
    speeches : [  
        { text : 'SSHHHHHHHFFFFFFFF!!!', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'GRRRRRRRRRAAAAAASHHHHH!!!', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [ 
        { spellType : SpellType.HEALING, chance : 0.5, interval : 4000, formula : SpellLightHealing.createFormula(100,400)},
        { spellType : SpellType.ULTIMATE_EXPLOSION_FIRE, chance : 0.25, interval : 4500, formula : SpellUltimateExplosion.createFormula(300,600)},
        { spellType : SpellType.GREAT_FIRE_BALL, chance : 0.6, interval : 2000, formula : SpellGreatFireBall.createFormula(200,400)},
        { spellType : SpellType.HASTE, chance : 0.45, interval : 4000, formula : SpellHaste.createFormula(200, 1000)},
        { spellType : SpellType.WHIRLD_WILD, chance : 0.25, interval : 6000, formula : SpellWhirlWind.createFormula(200, 650)},
    ]
})

monstersDefinition[18] = new MonsterDefinition({
    name : 'Blue Dragon',
    max_health : 8000,
    attack_damage : 600,
    speed: 30,
    experience : 2500,
    movement_type : MovementType.WALK,
    light : LightColorRGB.BLUE,
    transparency: 1,
    shadow_override : null,
    body_item_id : 34,
    size : { x: 2, y : 2},
    hpbar_pos_modifier : {x:0, y:25},
    me_attack : 7,
    armor: 60,
    run_on_health : 2000,
    frames : [
        "assets/dragon_hornB_00.png",
        "assets/dragon_hornB_01.png",
        "assets/dragon_hornB_02.png"
    ],
    loots : [ 
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 1, chance: 0.25, maxqtd : 1, flychance: 1},
        { itemId: 62, chance: 0.75, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.75, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.55, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.5, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.5, maxqtd : 100, flychance: 1},
        { itemId: 75, chance: 0.25, maxqtd : 1, flychance: 1}, // durian
        { itemId: 76, chance: 0.55, maxqtd : 1, flychance: 1}, // dragon fruit
        { itemId: 76, chance: 0.55, maxqtd : 1, flychance: 1}, // dragon fruit
        { itemId: 76, chance: 0.55, maxqtd : 1, flychance: 1}, // dragon fruit
    ],
    speeches : [  
        { text : 'SSHHHHHHHFFFFFFFF!!!', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'THE EXISTENCE WILL END TODAY!!!', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [ 
        { spellType : SpellType.HEALING, chance : 0.6, interval : 4000, formula : SpellLightHealing.createFormula(400,600)},
        { spellType : SpellType.ULTIMATE_EXPLOSION_ENERGY, chance : 0.45, interval : 4000, formula : SpellUltimateExplosion.createFormula(400,800)},
        { spellType : SpellType.CHAIN_LIGHTNING, chance : 0.6, interval : 2500, formula : SpellGreatFireBall.createFormula(300,600)},
        { spellType : SpellType.HASTE, chance : 0.5, interval : 6000, formula : SpellHaste.createFormula(200, 650)},
        { spellType : SpellType.WHIRLD_WILD, chance : 0.45, interval : 4700, formula : SpellWhirlWind.createFormula(200, 650)},
    ]
})

monstersDefinition[19] = new MonsterDefinition({
    name : 'Dragon',
    max_health : 3000,
    attack_damage : 200,
    speed: 30,
    experience : 1100,
    movement_type : MovementType.WALK,
    light : null,
    transparency: 1,
    shadow_override : null,
    body_item_id : 34,
    size : { x: 2, y : 2},
    hpbar_pos_modifier : {x:0, y:25},
    me_attack : 7,
    armor: 60,
    run_on_health : 1000,
    frames : [
        "assets/dragon_hornG_00.png",
        "assets/dragon_hornG_01.png",
        "assets/dragon_hornG_02.png"
    ],
    loots : [ 
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 34, chance: 1, maxqtd : 1, flychance: 1},
        { itemId: 62, chance: 0.75, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.75, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.55, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.5, maxqtd : 100, flychance: 1},
        { itemId: 62, chance: 0.5, maxqtd : 100, flychance: 1},
        { itemId: 1, chance: 0.25, maxqtd : 1, flychance: 1},
        { itemId: 75, chance: 0.25, maxqtd : 1, flychance: 1}, // durian
        { itemId: 76, chance: 0.55, maxqtd : 1, flychance: 1}, // dragon fruit
        { itemId: 76, chance: 0.55, maxqtd : 1, flychance: 1}, // dragon fruit
        { itemId: 76, chance: 0.55, maxqtd : 1, flychance: 1}, // dragon fruit
    ],
    speeches : [  
        { text : 'SSHHHHHHHFFFFFFFF!!!', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'GRRRRRRRRRAAAAAASHHHHH!!!', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [ 
        { spellType : SpellType.HEALING, chance : 0.4, interval : 4000, formula : SpellLightHealing.createFormula(100,200)},
        { spellType : SpellType.ULTIMATE_EXPLOSION_FIRE, chance : 0.25, interval : 4500, formula : SpellUltimateExplosion.createFormula(100,300)},
        { spellType : SpellType.GREAT_FIRE_BALL, chance : 0.5, interval : 3000, formula : SpellGreatFireBall.createFormula(100,250)},
        { spellType : SpellType.HASTE, chance : 0.4, interval : 10000, formula : SpellHaste.createFormula(20, 8000)},
    ]
})

monstersDefinition[20] = new MonsterDefinition({
    name : 'Assassin Ant',
    max_health : 200,
    attack_damage : 150,
    speed: 45,
    experience : 150,
    movement_type : MovementType.WALK,
    body_item_id : 64,
    frame_timer : 60,
    me_attack : 6,
    passivity_range : 8,
    run_on_health : 60,
    frames : [
        "assets/ant_x_00.png",
        "assets/ant_x_01.png",
        "assets/ant_x_02.png"
    ],
    loots : [ 
        { itemId: 62, chance: 0.5, maxqtd : 100},
        { itemId: 72, chance: 0.25, maxqtd : 1}, // nastar
        { itemId: 71, chance: 0.25, maxqtd : 1}, // ketupat
        // { itemId: 10, chance: 0.25, maxqtd : 1},
        // { itemId: 11, chance: 0.05, maxqtd : 1}
    ],
    speeches : [  
        { text : 'The kingdom of ants will rule this world', chance : 0.08, interval : 5000, sound: null, color:null },
    ],
    spells : [ 
        { spellType : SpellType.HASTE, chance : 0.2, interval : 8000, formula : SpellHaste.createFormula(100, 650)}
    ]
})


monstersDefinition[21] = new MonsterDefinition({
    name : 'Shiny Pocong',
    max_health : 800,
    attack_damage : 100,
    speed: 40,
    experience : 510,
    movement_type : MovementType.WALK,
    light : LightColorRGB.GREEN,
    shadow_override : null,
    body_item_id : 6,
    me_attack : 6,
    passivity_range : 7,
    armor: 40,
    run_on_health : 200,
    frames : [
        "assets/pocong_00.png",
        "assets/pocong_01.png",
        "assets/pocong_02.png",
        "assets/pocong_01.png"
    ],
    loots : [ 
        { itemId: 1, chance: 0.2, maxqtd : 1},
        { itemId: 40, chance: 0.4, maxqtd : 1},
        { itemId: 41, chance: 0.4, maxqtd : 1},
        { itemId: 62, chance: 1, maxqtd : 30, flychance : 0 },
        { itemId: 62, chance: 1, maxqtd : 30, flychance : 0 },
        { itemId: 62, chance: 1, maxqtd : 30, flychance : 0 },
        { itemId: 67, chance: 0.25, maxqtd : 1} ,// sate packet
        { itemId: 66, chance: 0.25, maxqtd : 1} // sate
    ],
    speeches : [  //TODO
        // { text : '', chance : 0.04, interval : 5000, sound: null, color:null },
        { text : 'Groooooarrrrrrr', chance : 0.05, interval : 5000, sound: 'MONSTER_POC_00', color:null },
        { text : 'Buuuuuuhhhhwaaa', chance : 0.1, interval : 5000, sound: 'MONSTER_POC_00', color:null },
    ],
    spells: [        
        { spellType : SpellType.PARALYZE, chance : 0.55, interval : 5000, formula : SpellParalyze.createFormula(0.2, 4000) },
        { spellType : SpellType.HEALING, chance : 0.55, interval : 2000, formula : SpellLightHealing.createFormula(14, 36) },
        { spellType : SpellType.BASIC_ATTACK, chance : 0.66, interval : 2500, formula : SpellBasicAttack.createFormula(50, 85, MagicEffect.E_BITE, DamageType.DEATH, 4, null) },
    ]
})


monstersDefinition[22] = new MonsterDefinition({
    name : 'Hunter',
    max_health : 180,
    attack_damage : 130,
    speed: 25,
    experience : 110,
    movement_type : MovementType.WALK,
    light : LightColorRGB.BLUE,
    shadow_override : null,
    body_item_id : 65,
    attack_range : 4,
    attack_shoot_effect : ShootEffect.E_ARROW,
    me_attack : MagicEffect.E_BLOOD,
    passivity_range : 9,
    run_on_health : 70,
    frames : [
        "assets/jav_ws_00.png",
        "assets/jav_ws_01.png",
        "assets/jav_ws_02.png",
        "assets/jav_ws_01.png"
    ],
    loots : [ 
        { itemId: 1, chance: 0.2, maxqtd : 1},
        { itemId: 40, chance: 0.4, maxqtd : 1},
        { itemId: 41, chance: 0.4, maxqtd : 1},
        { itemId: 62, chance: 1, maxqtd : 10, flychance : 0 },
        { itemId: 67, chance: 0.25, maxqtd : 1} ,// sate packet
        { itemId: 66, chance: 0.25, maxqtd : 1} // sate
    ],
    speeches : [  //TODO
        { text : 'I WANT YOUR HEAD', chance : 0.05, interval : 5000, sound: null, color:null }
    ],
    spells : [ 
        { spellType : SpellType.BASIC_ATTACK, chance : 0.6, interval : 3500, formula : SpellBasicAttack.createFormula(30, 95, MagicEffect.E_DIE_BLOOD, DamageType.PHYSICAL, 5, ShootEffect.E_ARROW) },
        { spellType : SpellType.HEALING, chance : 0.35, interval : 2500, formula : SpellLightHealing.createFormula(10, 20) },
        { spellType : SpellType.HASTE, chance : 0.15, interval : 6000, formula : SpellHaste.createFormula(25, 3000) },
    ]
})

monstersDefinition['getByName'] = (name) => 
    monstersDefinition.find(x=> x.name.trim().toLowerCase() == name.trim().toLowerCase())

window['monstersDefinition'] = monstersDefinition
export default monstersDefinition