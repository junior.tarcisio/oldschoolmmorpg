export class NPCDefinition { 
    // constructor(public name:string, public frames:Array<any>, public speeches:Array<any>=[]) {

    // }

    public name : string = 'unnamed'
    public frames : Array<HTMLImageElement> = []
    public speeches : Array<any>  = []
    public tradeItems : Array<any>  = []
    
    constructor(props) {
        for(let key in props) {
            if (key == "frames") {
                props.frames.forEach(url => {
                    this.frames.push(new Image)
                    this.frames[this.frames.length-1].src = url
                });                
            }                
            else {
                this[key] = props[key]
            }
        }
    }
}

const NPCsDefinition = []
NPCsDefinition[0] = new NPCDefinition({
    name :"Gajah Madah", 
    frames : [
        'assets/jav_w_00.png',
        'assets/jav_w_01.png',
        'assets/jav_w_02.png'
    ],
    speeches : [
        { on: 'activate', text: '', audio : ''},
        { on: 'deactivate', text: '', audio : ''}
    ],
    tradeItems: [
        { itemDefinition: 40, cost: 50 },
        { itemDefinition: 38, cost: 120 },
        { itemDefinition: 41, cost: 50 },
        { itemDefinition: 39, cost: 120 },
        { itemDefinition: 1, cost: 650 },
        { itemDefinition: 40, cost: 50 },
        { itemDefinition: 38, cost: 120 },
        { itemDefinition: 41, cost: 50 },
        { itemDefinition: 39, cost: 120 },
        { itemDefinition: 1, cost: 650 }
    ]}
)


NPCsDefinition[1] = new NPCDefinition({
    name :"Tukang Sayur", 
    frames : [
        'assets/jav_00.png',
        'assets/jav_01.png',
        'assets/jav_02.png'
    ],
    speeches : [
        { on: 'activate', text: '', audio : ''},
        { on: 'deactivate', text: '', audio : ''}
    ],
    tradeItems: [
        { itemDefinition: 40, cost: 50 },
        { itemDefinition: 38, cost: 120 },
        { itemDefinition: 41, cost: 50 },
        { itemDefinition: 39, cost: 120 },
        { itemDefinition: 1, cost: 650 },
        { itemDefinition: 40, cost: 50 },
        { itemDefinition: 38, cost: 120 },
        { itemDefinition: 41, cost: 50 },
        { itemDefinition: 39, cost: 120 },
        { itemDefinition: 1, cost: 650 }
    ]}
)

export default NPCsDefinition