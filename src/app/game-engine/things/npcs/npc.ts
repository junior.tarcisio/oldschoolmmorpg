import { Creature } from "../creature"
import { Game, HitBox } from "../../game"
import { Direction } from "../../enumerables/direction"
import { GameConstants } from "../../game-constants"
import NPCsDefinition, { NPCDefinition } from './npc-definition'
import { MovementType } from '../monsters/monster-definition'
import { Item } from "../items/item";

export class NPC extends Creature {
    npcDefinition : NPCDefinition //pointer vs ID? let's go on pointer now
    currentFrame = 0
    originPos

    //what about refacting it to a global number? less memory and more performatic
    thinkTimer = Date.now()
    frameTimer = Date.now()
    walkTimer = Date.now()

    alive = true
    movingPercent = 0
    movingToDirection : Direction = null

    readonly THINK_INTERVAL = 15
    readonly FRAME_INTERVAL = 80 //may come from monster definition
    readonly MAX_DISTANCE_WALK = 3
    static speakIMG : HTMLImageElement = new Image

    constructor(public npcDefinitionID:number, position, game : Game) {
        super(position, game)
        this.originPos = { x: this.pos.x, y : this.pos.y }
        this.npcDefinition = NPCsDefinition[npcDefinitionID]
        this.loadNPC()
    }

    loadNPC() {
        this.max_health = 100
        this.health = 100
        this.speed = 15
    }

    update() {

        if (this.game.frame.time - this.thinkTimer <= this.THINK_INTERVAL) return

        this.thinkTimer = this.game.frame.time

        if (this.movingPercent == 0 && this.game.frame.time - this.walkTimer > 3000) {
            
            this.movingToDirection = this.getRandomDirection()
            
            if (this.distanceBetween(this.getDirectionPos(this.pos, this.movingToDirection), this.originPos) > this.MAX_DISTANCE_WALK)
                this.movingToDirection = null
            else if (this.canMoveTo(this.movingToDirection, MovementType.WALK)) {
                this.moveToTileOnDirection(this.movingToDirection)    
                this.walkTimer = this.game.frame.time
            }
            else
                this.movingToDirection = null
        }
        
        if (this.movingPercent >= 100) {
            this.movingPercent = 0
            this.movingToDirection = null
        } else {
            this.movingPercent += this.speed / 10  * this.getMapTile().getTile().speed
        }

        //this.randomSpeech()  
        this.updateFrame()
    }

    updateFrame() {
        
        if (Date.now() - this.frameTimer > 80) { //this.npcDefinition.frame_timer) {
            if (this.movingToDirection != null) { // || this.monsterDefinition.movement_type == MovementType.FLY) {
                this.frameTimer = Date.now()        
                this.currentFrame++
                
                if (this.currentFrame >= this.npcDefinition.frames.length)
                    this.currentFrame = 0
            } else {
                this.currentFrame = 0
            }
        }
    }

    tradeItem(tradeItem) {
        const slot = this.game.player.getFirstFreeInventorySlot()

        if (this.game.player.gold < tradeItem.cost) {
            this.game.addMessageHistory('You don\'t have enough gold to buy it')
            return
        }

        if (slot != null) {
            slot.item = new Item(tradeItem.itemDefinition, 1)
            this.game.addMessageHistory(`You have purchased the item ${slot.item.getDefinition().name}`)
            this.game.player.gold -= tradeItem.cost
            
            if (this.game.frame.random > 0.5)                         
                this.game.audioManager.SOUNDS.COIN_1.play()
            else
                this.game.audioManager.SOUNDS.COIN_2.play()
        } else {
            this.game.addMessageHistory('You don\'t have enough space in your inventory')
        }
    }

    // randomSpeech() {

    //     if (!this.npcDefinition.speeches || this.npcDefinition.speeches.length == 0) 
    //         return 

    //     const now = Date.now()
        
    //     if (now < this.speechExhaustedTill)
    //         return

    //     const randomResult = Math.random()

    //     for (let i = 0; i < this.npcDefinition.speeches.length; i++) {
    //         const speech = this.monsterDefinition.speeches[i]
    //         if (randomResult <= speech.chance) {
    //             this.game.addComponent(
    //                 new MonsterSpeech(this.pos, speech.text, this.game))
    //             this.speechExhaustedTill = now + speech.interval
                
    //             if (speech.sound)
    //                 this.game.audioManager.SOUNDS[speech.sound].play()
    //             return
    //         }
    //     }

    //     this.speechExhaustedTill = now + 1000
    // }
    
    getPixelPositionOnScreen() {
        let pos = this.game.gamePositionToScreenPixelsPosition(this.pos)
        
        let drawX = 0, drawY = 0, drawModifier = 0
    
        drawModifier = ((this.movingPercent / 100) * GameConstants.spriteCalculatedSize) - GameConstants.spriteCalculatedSize
    
        switch (this.movingToDirection) {
            case Direction.UP:            
                drawY = drawModifier * -1
                break
            case Direction.DOWN:
                drawY = drawModifier
                break
            case Direction.LEFT:
                drawX = drawModifier * -1
                break
            case Direction.RIGHT:
                drawX = drawModifier
                break
            default:
                break
        }

        pos.x += drawX
        pos.y += drawY

        return pos
    }

    cleanObjectForGC() {
        this.target = null
        this.frameTimer = null
        this.game = null
        this.movingToDirection = null
        this.pos = null
        this.thinkTimer = null
    }
    
    receiveDamage(attacker:Creature, amount:number) {
        //this.game.addComponent(new MagicEffect(this.pos, this.game, 9))
        //return
        //super.receiveDamage(attacker, amount)
        //this.target = attacker
    }

    drawWithFocus(screenPixelsPosition) {

        // const calculatedCameraAcceleration = {x:0,y:0}

        // this.game.ctx2.drawImage(this.monsterDefinition.frames[this.currentFrame],
        //     screenPixelsPosition.x - (72 * (this.monsterDefinition.size.x - 1)) - calculatedCameraAcceleration.x,
        //     screenPixelsPosition.y - (72 * (this.monsterDefinition.size.y - 1)) - calculatedCameraAcceleration.y,
        //     GameConstants.spriteCalculatedSize * this.monsterDefinition.size.x, 
        //     GameConstants.spriteCalculatedSize * this.monsterDefinition.size.y)
            
        // if (this.game.player.target && this.game.player.target === this) {

        //     this.game.ctx2.globalAlpha = 0.8 - 0.8 * Math.abs(((this.game.frame.time % 2000 / 2000) - 0.5))
        //     this.game.ctx2.drawImage(Monster.targetImg,
        //         screenPixelsPosition.x,
        //         screenPixelsPosition.y,
        //         GameConstants.spriteCalculatedSize, 
        //         GameConstants.spriteCalculatedSize)
        //     this.game.ctx2.globalAlpha = 1
        // }
        
        // super.draw(this.monsterDefinition.hpbar_pos_modifier)
    }
    
    draw() {
        super.draw()//this.monsterDefinition.hpbar_pos_modifier)
    
        const screenPixelsPosition = this.getPixelPositionOnScreen()
    
        this.game.ctx.shadowOffsetX = GameConstants.GLOBAL_SHADOW_X
        this.game.ctx.shadowOffsetY = GameConstants.GLOBAL_SHADOW_Y
        this.game.ctx.shadowBlur = GameConstants.GLOBAL_SHADOW_BLUR
        this.game.ctx.shadowColor = GameConstants.GLOBAL_SHADOW_COLOR
        
        //if (this.monsterDefinition.transparency)
        //    this.game.ctx.globalAlpha = this.monsterDefinition.transparency
        
        //TODO: if it works well, refactor
        this.game.hitboxes.push(new HitBox(this, 
            screenPixelsPosition.x, // - (72 * (this.monsterDefinition.size.x - 1)),
            GameConstants.spriteCalculatedSize,// * this.monsterDefinition.size.x, 
            screenPixelsPosition.y, // - (72 * (this.monsterDefinition.size.y - 1)),
            GameConstants.spriteCalculatedSize))// * this.monsterDefinition.size.y))

        this.game.ctx.drawImage(this.npcDefinition.frames[this.currentFrame],
            screenPixelsPosition.x,// - (72 * (this.monsterDefinition.size.x - 1)),
            screenPixelsPosition.y,// - (72 * (this.monsterDefinition.size.y - 1)),
            GameConstants.spriteCalculatedSize,// * this.monsterDefinition.size.x, 
            GameConstants.spriteCalculatedSize)// * this.monsterDefinition.size.y)

        this.game.ctx.shadowOffsetX = 0;
        this.game.ctx.shadowOffsetY = 0;
        this.game.ctx.shadowBlur = 0;

        this.game.ctx.globalAlpha = 1

        // if (this.monsterDefinition.light)
        //     this.game.nightEffectManager.addLightEffect(screenPixelsPosition,1, this.monsterDefinition.light)
    }

    getMagicEffectType() {
        return 0
    }
    
    getName(): string {
        return this.npcDefinition.name
    }
    
    getAttackDamage(): number {
        return 0
    }
    
    getAttackShootEffect() : number {
        return null
    }
    
    getArmor(): number {
        return 0
    }
}
NPC.speakIMG.src = "assets/ico_speak.png"

window['NPC'] = NPC