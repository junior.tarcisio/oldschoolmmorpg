import { Game } from "../game";
import { Thing } from "./thing";
import { LightColorRGB } from "../enumerables/rgb-color";
import { GameConstants } from "../game-constants";

export class Body extends Thing {
    creationTime : number
    img
    opacity = 1
    type

    readonly DECAY_TIME = 30000;
    readonly DECAY_TIME_START = 28000;

    constructor(position, game : Game, type = 0) {
        super(position, game)
        this.creationTime = Date.now()
        this.type = type
        this.img = new Image

        if (type == 0)
            this.img.src = "assets/reaper_body.png"
        else if (type == 1)
            this.img.src = "assets/crusader_body.png"
        else if (type == 2)
            this.img.src = "assets/pocong_body.png"
        else if (type == 3)
            this.img.src = "assets/e_pocong_body.png"
        else if (type == 4)
            this.img.src = "assets/panda_body.png"
    }   

    update () {
        const elapsedTime = Date.now() - this.creationTime;
        if (elapsedTime > this.DECAY_TIME)
            this.alive = false;
        
        else if (elapsedTime > this.DECAY_TIME_START)
            this.opacity = (this.DECAY_TIME - elapsedTime) / this.DECAY_TIME_START;
    }

    draw() {
        const pos = this.game.gamePositionToScreenPixelsPosition(this.pos)
        
        this.game.ctx.globalAlpha = this.opacity
        this.game.ctx.drawImage(
            this.img, pos.x,pos.y,
            GameConstants.spriteCalculatedSize, 
            GameConstants.spriteCalculatedSize)
        this.game.ctx.globalAlpha = 1

        if (this.type == 0)
            this.game.nightEffectManager.addLightEffect(pos,1, LightColorRGB.GREEN)
    }
}