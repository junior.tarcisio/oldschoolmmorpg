import { Game } from '../game'
import { GameConstants } from '../game-constants';

export class ScreenMessageManager {
    private readonly MESSAGE_LIFE_TIME = 4000
    private msgList: Array<string>
    private currentMessageStartShowTime: number
    public alive: boolean = true

    constructor(public game: Game) {
        this.msgList = new Array<string>()
    }

    addMessage(message: string) {
        if (this.msgList.length == 0)
            this.currentMessageStartShowTime = Date.now()

        this.msgList.push(message)
        this.game.addMessageHistory(message)
    }

    update() {
        if (this.msgList.length > 1) {
            this.msgList.shift()
        }
        if (Date.now() - this.currentMessageStartShowTime > this.MESSAGE_LIFE_TIME) {
            this.msgList.shift()
            this.currentMessageStartShowTime = Date.now()
        }
    }

    draw() {
        if (this.msgList.length == 0) return

        const msgLines = this.msgList[0].split("\n")

        let posY = GameConstants.spriteCalculatedSize * 4.5 // 380
        msgLines.forEach(line => {
            this.game.ctx2.beginPath()
            this.game.ctx2.textAlign = "center"
            this.game.ctx2.font = "bold 22px Calibri"
            this.game.ctx2.fillStyle = '#FFF'
            this.game.ctx2.strokeStyle = '#000'
            this.game.ctx2.lineWidth = 1
            this.game.ctx2.fillText(line, GameConstants.spriteCalculatedSize * 5.5, posY)
            this.game.ctx2.strokeText(line, GameConstants.spriteCalculatedSize * 5.5, posY)
            posY+=24
        });
    }
}

