import { ConditionParalyzed } from './../conditions/condition-paralyze';
import { MovementType } from './monsters/monster-definition';
import { DamageLabel } from './../effects/damage-label';
import { Thing } from "./thing";
import { Game } from "../game";
import { MagicEffect } from '../effects/magic-effect';
import { Direction } from '../enumerables/direction';
import { MapTile } from '../map/map';
import Position from '../position';
import { GameConstants } from '../game-constants';
import { PlayerSpeechType } from './player_speeches';
import { ConditionHaste } from '../conditions/condition-haste';
import conditionsDefinition, { ConditionType } from '../conditions/conditions-definition';
import { Condition } from '../conditions/condition';
import { DamageType } from '../enumerables/damage-type';
import { ShootEffect } from '../effects/shoot-effect';
import Player from './player';

export abstract class Creature extends Thing {
    game : Game;
    public health : number;
    public max_health : number;
    public mana : number;
    public max_mana : number;
    public speed : number;
    public target : Creature;
    public lastAttack = Date.now();
    conditions : any = {}
    
    constructor(position, game) {
        super(position, game)
    }

    receiveMana(amount:number, showEffect:boolean=true) {
        if (this.mana + amount > this.max_mana)
            amount = this.max_mana - this.mana;

        this.mana += amount;

        if (amount > 0 && showEffect)
            this.game.addComponent(new DamageLabel(this.pos, amount, this.game, DamageType.HEALING));
    }

    receiveHealing(amount:number) {
        
        amount = this.receiveHealth(amount)

        if (amount > 0)
            this.game.addComponent(new DamageLabel(this.pos, amount, this.game, DamageType.HEALING));
    }

    receiveHealth(amount:number) : number {
        if (this.health + amount > this.max_health)
            amount = this.max_health - this.health;

        this.health += amount;

        return amount
    }

    getPrefixAttackedBy = (attacker:Creature) => attacker["player"]?'':'a '
    
    receiveDamage(attacker:Creature, amount:number, type : DamageType) {

        switch (type) {
            case DamageType.PHYSICAL:
                amount -= this.getArmor()
                
                if (amount > 0) {
                    this.game.addComponent(new MagicEffect(this.pos, this.game, MagicEffect.E_BLOOD))
                }
                else {
                    this.game.addComponent(new MagicEffect(this.pos, this.game,MagicEffect.E_SPARK_ARMOR))
                        
                    if (this.game.frame.random < 0.333)
                        this.game.audioManager.SOUNDS.HIT_ARMOR_1.play()
                    else if (this.game.frame.random < 0.667)
                        this.game.audioManager.SOUNDS.HIT_ARMOR_2.play()
                    else    
                        this.game.audioManager.SOUNDS.HIT_ARMOR_3.play()

                    return
                }
                break;
        
            default:
                break;
        }

        this.game.addMessageHistory(`${this.getName()} loses ${amount} hitpoints due to ${this.getPrefixAttackedBy(attacker)}${attacker.getName()} attack.`)
        
        if (amount > this.health)
            amount = this.health

        this.health = this.health - amount
        
        const newDamageLabel = new DamageLabel({
            x : this.pos.x,
            y : this.pos.y
          }, amount, this.game, type)

        this.game.addComponent(newDamageLabel)

        const randomResult = Math.random()
        if (randomResult < 0.333)
            this.game.audioManager.SOUNDS.HIT_1_1.play()
        else if (randomResult < 0.667)
            this.game.audioManager.SOUNDS.HIT_1_2.play()
        else
            this.game.audioManager.SOUNDS.HIT_1_3.play()   
    
        if (this.health < 0) this.health = 0
        if (this.health === 0) this.die(attacker)
    }

    abstract getMagicEffectType()
    
    addCondition(type:ConditionType, props:any) : boolean {
        const existingCondition : Condition = this.conditions[type]
        if (existingCondition) {
            return existingCondition.add(this, props)
        } else {
            const conditionClass =  conditionsDefinition[type]
            const condition : Condition = new conditionClass()
            const canAdd = condition.add(this, props)

            if (!canAdd)
                return false

            this.conditions[type] = condition
            return true
        }
    }

    
    updateConditions() {
        for(let key in this.conditions) {
            const condition : Condition = this.conditions[key]

            if (condition == null)
                continue
            
            if (condition.alive) {
                condition.update(this)
                
                if (!condition.alive)
                    this.conditions[key] = null
            }
        }
    }

    attack() {
        if (Date.now() - this.lastAttack > 2000) {
            if (!this.target.alive) {
                this.target = null
                return
            }
            
            const dmg = Math.ceil(this.getAttackDamage() * Math.random()) 

            this.target.receiveDamage(this,dmg, DamageType.PHYSICAL)
            this.game.addComponent(new MagicEffect(this.target.pos, this.game,this.getMagicEffectType()))
            this.lastAttack = Date.now()

            const shootEffect : number = this.getAttackShootEffect()

            if (shootEffect != null) {
                this.game.addComponent(new ShootEffect(this.game,this.pos, this.target.pos, shootEffect))
            }
        }
    }

    receiveExperience(amount:number) {        
        this.game.addComponent(new DamageLabel(this.pos, amount, this.game, DamageType.WHITE_EXP))        
    }
    
    die(attacker:Creature) {        
        this.game.audioManager.SOUNDS.DIE_1.play()

        if (attacker === this.game.player)            
            this.game.playerSpeeches.playIfTimeout(this.game.frame.time, PlayerSpeechType.KILL)
    }

    getPixelPositionOnScreen() {
        return this.game.gamePositionToScreenPixelsPosition(this.pos);
    }

    getDirectionToPosition(pos) {
        if (this.pos.x > pos.x) 
            return Direction.LEFT 
        else if (this.pos.x < pos.x) 
            return Direction.RIGHT                
        else if (this.pos.y > pos.y) 
            return Direction.UP                
        else if (this.pos.y < pos.y) 
            return Direction.DOWN
        else 
            return null
    }

    getRandomDirection() {
        const randomResult = Math.random()

        if (randomResult < 0.25)
            return Direction.LEFT 
        else if (randomResult < 0.5)
            return Direction.RIGHT 
        else if (randomResult < 0.75)
            return Direction.UP 
        else
            return Direction.DOWN 
    }

    canMoveTo(direction:Direction, mov_type : MovementType = MovementType.WALK) {
        const SQMTo : MapTile = Position.getSubjacentSQM(this.game.map, this.pos, direction)
        if (mov_type == MovementType.WALK)
            return SQMTo && !SQMTo.movementBlocked()
        else
            return SQMTo && !SQMTo.hasCreature()
    }

    getSpeed() {
        const conditionParalyzed : ConditionParalyzed = this.conditions[ConditionType.PARALYZED]

        if (conditionParalyzed && conditionParalyzed.alive) {
            console.log(conditionParalyzed.amount)
            return this.speed * conditionParalyzed.amount
        }
        
        const conditionHaste : ConditionHaste = this.conditions[ConditionType.HASTE]

        if (conditionHaste && conditionHaste.alive) {
            return this.speed + conditionHaste.amount
        }

        return this.speed
    }

    moveToTileOnDirection(direction:Direction) {
        if (this.conditions[ConditionType.HASTE]) // .getSpeed() > 60)
            this.game.addComponent(new MagicEffect({x:this.pos.x, y:this.pos.y}, this.game, MagicEffect.E_SMOKE_PUFT))
            
        if (this.getSpeed() > 100 && this instanceof Player)
            this.game.addComponent(new MagicEffect({x:this.pos.x, y:this.pos.y}, this.game, MagicEffect.E_SHADOW))

        const oldPosition = { x : this.pos.x, y : this.pos.y }
        switch (direction) {
            case Direction.LEFT:
                this.pos.x--
                break
            case Direction.UP:
                this.pos.y--
                break
            case Direction.RIGHT:
                this.pos.x++
                break
            case Direction.DOWN:
                this.pos.y++
                break
        }
        this.moveToTilePosition(oldPosition)
    }
    
    moveToTilePosition(oldPosition) {
        this.game.map[oldPosition.y][oldPosition.x].removeCreature(this)
        this.game.map[this.pos.y][this.pos.x].addCreature(this)
    }

    update() {}

    abstract getName() : string

    abstract getAttackDamage() : number

    abstract getArmor() : number

    abstract getAttackShootEffect() : number

    abstract drawWithFocus(screenPixelsPosition) 

    draw(hpbar_mod=null) {        
        if (!this.alive) return;
        
        let ctx = this.game.ctx2

        if (hpbar_mod == null) {
            hpbar_mod = {x:0,y:0}
        }
        
        const screenPixelsPosition = this.getPixelPositionOnScreen();
        
        const HEALTH_BAR_HEIGHT = GameConstants.spriteCalculatedSize/14.4;
        const HEALTH_BAR_WIDTH = GameConstants.spriteCalculatedSize - 10;

        //heath bar black-bg
        ctx.beginPath();
        ctx.rect(screenPixelsPosition.x - hpbar_mod.x,
            screenPixelsPosition.y - 5 - hpbar_mod.y,
            HEALTH_BAR_WIDTH,
            HEALTH_BAR_HEIGHT);
        
        ctx.fillStyle = '#000';
        ctx.fill();   
        
        //heath bar stronger border 2px
        ctx.lineWidth = 2;
        ctx.strokeStyle = '#000';
        ctx.stroke(); 
    
        const health_percent = this.health / this.max_health;
        
        //heath bar fill
        if (this.health > 0) {
            const HEALTH_BAR_PADDING = 1;
            ctx.beginPath()
            ctx.rect(
                screenPixelsPosition.x + HEALTH_BAR_PADDING - hpbar_mod.x,
                screenPixelsPosition.y - 5 + HEALTH_BAR_PADDING - hpbar_mod.y,
                HEALTH_BAR_WIDTH * health_percent - 2,
                HEALTH_BAR_HEIGHT - 2);
            
            if (health_percent > 0.6)
                ctx.fillStyle = '#0F0';
            else if (health_percent > 0.3)
                ctx.fillStyle = '#FF0';
            else 
                ctx.fillStyle = '#F00';
            
            ctx.lineWidth = 0;
            ctx.fill();      
        }

        //name 
        ctx.beginPath();
        ctx.textAlign="center";
        ctx.font = "bold 22px Calibri";
        ctx.fillStyle = '#ff0';
        ctx.strokeStyle = '#220';
        ctx.lineWidth = 1;
        ctx.fillText(this.getName(),
            screenPixelsPosition.x + GameConstants.spriteCalculatedSize/2 - hpbar_mod.x,
            screenPixelsPosition.y - 10 - hpbar_mod.y);
            
        ctx.strokeText(this.getName(),
            screenPixelsPosition.x + GameConstants.spriteCalculatedSize/2 - hpbar_mod.x,
            screenPixelsPosition.y - 10 - hpbar_mod.y);
        ctx.stroke();
        ctx.fill();
    }
}