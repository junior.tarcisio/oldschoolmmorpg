import { HotkeyType } from './../game-commands-hotkey';

import { Direction } from './../enumerables/direction';
import { Game } from "../game"
import { Creature } from "./creature"
import { Speech } from "../effects/speech"
import { SpellSpeech } from "../spells/spell-speech"
import { DamageLabel } from "../effects/damage-label"
import { InventorySlot } from './inventory-slot'
import { MagicEffect } from '../effects/magic-effect'
import Position from '../position'
import { GameConstants } from '../game-constants'
import { Item } from "./items/item"
import { ItemFlags } from "./items/item-flags"
import { ItemProperties } from './items/items-definition';
import { NPC } from './npcs/npc';
import conditionsDefinition, { ConditionType } from '../conditions/conditions-definition';
import { ConditionLight } from '../conditions/condition-light';
import spellsDefinition, { SpellType } from '../spells/spells-definition';
import { GameCommandsHotkey } from '../game-commands-hotkey';
import { KeyCodes } from '../enumerables/key-codes';
import { DamageType } from '../enumerables/damage-type';

export enum SlotType {
    CANT_WEAR = 0,
    NECK = 1,
    HEAD = 2,
    CHEST = 4,
    LEFT_HAND = 8,
    RIGHT_HAND = 16,
    LEFT_FINGER = 32,
    RIGHT_FINGER = 64,
    LEGS = 128,
    FEET = 256
}

export default class Player extends Creature {
    name : string
    game : Game
    frames = []
    currentFrame = 0
    frameTimer = 0
    exhaustedTimer : number = 0
    bodyFrames = []
    img = null
    speed = 50
    movingToDirection : Direction = null
    movingPercent = null
    god = false
    cameraAcceleration = { x: 0, y : 0 }
    gold : number = 0

    inventory : Array<InventorySlot>
    equipment : Array<Item> = new Array<Item>(8)
    spells : Array<SpellType>
    hotkeys : {
        default : Array<GameCommandsHotkey>,
        shift : Array<GameCommandsHotkey>,
        // alt : Array<GameCommandsHotkey>
    } 

    npcTarget : NPC

    alive = true
    player = true

    level : number
    experience : number

    movingItem : Item = null
    openedContainer : Item = null
    openedContainerPos

    lookingDirection : Direction

    // conditions : any = {}

    constructor(pos, game : Game) {
        super(pos, game)
            
        this.frames[0] = new Image
        this.frames[0].src = "assets/jav_ws_00.png"

        this.frames[1] = new Image
        this.frames[1].src = "assets/jav_ws_01.png"

        this.frames[2] = new Image
        this.frames[2].src = "assets/jav_ws_02.png"

        this.bodyFrames[0] = new Image
        this.bodyFrames[0].src = "assets/player_body.png"

        this.img = this.frames[0]

        this.mockPlayer()

        this.game.addComponent(new MagicEffect({x:pos.x,y:pos.y}, game, 4)) 
    }

    mockPlayer() {
        this.health = 185
        this.max_health = 185
        this.mana = 100
        this.max_mana = 100
        this.speed = 25
        this.name = "Hagar"
        this.level = 1
        this.experience = 0
        this.gold = 0
        
        this.inventory = new Array<InventorySlot>()
        while(this.inventory.length < 20)
            this.inventory.push(new InventorySlot())

        for (let i = 0; i < 4; i++) {
            this.addItemToInventory(new Item(40))            
            this.addItemToInventory(new Item(38))
            this.addItemToInventory(new Item(41))            
            this.addItemToInventory(new Item(39))
        }

        this.equipment[SlotType.RIGHT_HAND] = new Item(11)
        this.equipment[SlotType.CHEST] = new Item(42)
        this.equipment[SlotType.HEAD] = new Item(44)
        this.equipment[SlotType.LEFT_HAND] = new Item(45)
        this.equipment[SlotType.NECK] = new Item(46)
        this.equipment[SlotType.RIGHT_FINGER] = new Item(47)
        this.equipment[SlotType.LEFT_FINGER] = new Item(47)
        this.equipment[SlotType.LEGS] = new Item(48)
        this.equipment[SlotType.FEET] = new Item(49)

        this.spells = new Array
        for (let i = 0; i < spellsDefinition.length; i++) {
            if (spellsDefinition[i] != undefined)
                this.spells.push(i)            
        }

        this.hotkeys = {
            default : new Array,
            shift : new Array,
            // alt : new Array
        }
        
        while(this.hotkeys.default.length < 10) {
            this.hotkeys.default.push(new GameCommandsHotkey(this.hotkeys.default.length+1, HotkeyType.EMPTY))
            this.hotkeys.shift.push(new GameCommandsHotkey(10 - (this.hotkeys.shift.length), HotkeyType.EMPTY))
        }

        this.hotkeys.default[0].type = HotkeyType.SPELL
        this.hotkeys.default[0].tag = SpellType.HEALING

        this.hotkeys.default[1].type = HotkeyType.SPELL
        this.hotkeys.default[1].tag = SpellType.FIRE_STRIKE

        this.hotkeys.default[2].type = HotkeyType.SPELL
        this.hotkeys.default[2].tag = SpellType.ENERGY_STRIKE

        this.hotkeys.default[3].type = HotkeyType.SPELL
        this.hotkeys.default[3].tag = SpellType.WHIRLD_WILD
        
        this.hotkeys.default[4].type = HotkeyType.SPELL
        this.hotkeys.default[4].tag = SpellType.GREAT_FIRE_BALL
        
        this.hotkeys.default[5].type = HotkeyType.SPELL
        this.hotkeys.default[5].tag = SpellType.CHAIN_LIGHTNING
        
        this.hotkeys.default[6].type = HotkeyType.SPELL
        this.hotkeys.default[6].tag = SpellType.ARROWS_RAIN

        
        this.hotkeys.shift[0].type = HotkeyType.SPELL
        this.hotkeys.shift[0].tag = SpellType.HASTE
        
        this.hotkeys.shift[1].type = HotkeyType.SPELL
        this.hotkeys.shift[1].tag = SpellType.LIGHT

        this.hotkeys.shift[2].type = HotkeyType.SPELL
        this.hotkeys.shift[2].tag = SpellType.ULTIMATE_EXPLOSION_FIRE
        
        this.hotkeys.shift[3].type = HotkeyType.SPELL
        this.hotkeys.shift[3].tag = SpellType.ULTIMATE_EXPLOSION_ENERGY
    }

    addItemToInventory(item:Item) {
        for (let i = 0; i < this.inventory.length; i++) {
            const slot = this.inventory[i];
            if (!slot.item) {
                slot.item = item
                return
            }
        }
    }

    /**
     * level 1 - 0
     * level 2 - 100
     * level 3 - 300
     * level 4 - 700
     * level 5 - 1,500
     * level 6 - 3,100
     * level 7 - 6,300
     * level 8 - 12,700
     * level 9 - 51,100
     * level 10 - 102,300
     * level 20 - 104,857,500
     * level 30 - 107,374,182,300
     * or similar to ots:
     * some exp curve               reduce the curve              base for low levels 
     * --------------               ----------------              --------------------  
     * ((50*Math.pow(level,4))/2) - ((100*Math.pow(level,2))/2) + ((850*level)/2) - 200
     */
    getLevelRequiredExperience(level:number) {
        return Math.pow(2,level-1) * 100 - 100
    }
    
    receiveExperience(amount:number) {        
        this.game.addComponent(new DamageLabel(this.pos, amount, this.game, DamageType.WHITE_EXP))
        this.experience+= amount

        this.levelUp()
    }

    levelUp() {
        const prev_level = (this.level)

        while (this.experience >= this.getLevelRequiredExperience(this.level+1)) {
            this.level++
            this.max_health+= 30
            this.max_mana += 10
            this.speed += 1
            this.health = this.max_health
            this.mana = this.max_mana
        }

        if (this.level > prev_level) {
            this.game.addComponent(new DamageLabel(this.pos, "level up!", this.game, DamageType.HEALING, 20, 2000))
            this.game.audioManager.SOUNDS.MUSIC_INTRO_1.play()
            this.game.screenMessageManager.addMessage(`You advanced from level ${prev_level} to level ${this.level}.`)
            //this.game.addMessageHistory()       
        }
    }
    
    getLight() {
        const conditionLight : ConditionLight = this.conditions[ConditionType.LIGHT]

        if (conditionLight && conditionLight.alive) {
            return conditionLight.getCurrentLight(this.game.frame.time)
        }

        return null
    }

    getMagicEffectType() {
        return 5
    }

    earthquakeCounter = 0

    update() {

        if (!this.alive) {
            this.img = this.bodyFrames[0]
            return
        }
    
        //apply keyboard command
        if (this.movingToDirection == null) {
            this.movingToDirection = this.game.gameCommandsDOM.getMovingTo()
                
            if (this.movingToDirection != null) {
                this.movingPercent = 0
                this.game.pointer.alive = false 
                this.openedContainer = null
            }
        }
    
        //check for pathfind
        if (this.game.pointer.alive && this.movingToDirection == null) {

            if (this.game.pointer.goingGetItem && this.closeTo(this.game.pointer.pos)) {
                this.game.pointer.alive = false    
                this.tryGetItem(this.game.pointer.pos)
            } 
            else if (this.game.pointer.goingUseItem  && this.closeTo(this.game.pointer.pos)) {
                this.game.pointer.alive = false    
                this.tryUseItem(this.game.pointer.pos)

            }
            else if (this.game.pointer.path.length > 0) {
                const node = this.game.pointer.path.pop()
                this.movingToDirection = this.getDirectionToPosition(node)                    
                this.movingPercent = 0
                this.openedContainer = null
            } 
            else {
                this.game.pointer.alive = false
            }
        }
        
        if (this.target) {
            if (!this.target.alive)
                this.target = null
            else if (this.distanceForShootTo(this.target.pos) <= this.getAttackRange())
                this.attack()
        }

        if (this.movingToDirection != null) {

            //only checks when the movement is starting
            if (this.movingPercent == 0) {
                if (this.canMoveTo(this.movingToDirection) || (this.god && Position.getSubjacentSQM(this.game.map, this.pos, this.movingToDirection))) {
                    

                    this.moveToTileOnDirection(this.movingToDirection)    
                    
                    if (this.npcTarget) this.npcTarget = null
                }
                else
                    this.movingToDirection = null
            }
            
            this.movingPercent += (this.getSpeed() / 10) * this.getMapTile().getTile().speed
    
            if (this.movingPercent >= 100) {
                this.movingToDirection = null
                this.movingPercent = null
            }

        } else {
            this.movingPercent = 0
        }

        //TODO: Refactor, making here only to don't forget to make it later lol
        if (this.earthquakeCounter == 0) {
            if (this.game.frame.random < 0.00001) {
                this.earthquakeCounter = this.game.frame.time + 10000
            }
        }

        if (this.earthquakeCounter > 0) {
            const equakeProgress = this.earthquakeCounter - this.game.frame.time
            //10.000 start, ends 0.000, peak 5.000
            
            const progressFactor = (1 -(equakeProgress / 10000))
            //0.0 -> 1.0

            const curveFactor = 1 +  (Math.abs(0.5 - progressFactor)*-2)
            //0 -> 0.5 -> 1 
            //0->    1 -> 0
            //-1 -> 0  -> -1
            // 0 -> 1 -> 0

            console.log(progressFactor)

            if (equakeProgress > 0) {
                this.cameraAcceleration.x += (0.5 * curveFactor) - (Math.random() * (1 * curveFactor))
                this.cameraAcceleration.y += (0.5 * curveFactor) - (Math.random() * (1 * curveFactor))
                    
                if (this.cameraAcceleration.x > 1)
                    this.cameraAcceleration.x = 1
                else if (this.cameraAcceleration.x < -1)
                    this.cameraAcceleration.x = -1
                    
                if (this.cameraAcceleration.y > 1)
                    this.cameraAcceleration.y = 1
                else if (this.cameraAcceleration.y < -1)
                    this.cameraAcceleration.y = -1
                    
            } else {
                this.earthquakeCounter = 0
            }
        }

    
        if (this.movingToDirection) {

            this.lookingDirection = this.movingToDirection

            switch (this.movingToDirection) {
                case Direction.RIGHT:
                    this.cameraAcceleration.x -= 0.01
                    if (this.cameraAcceleration.x < -1)
                        this.cameraAcceleration.x = -1
                    break;
                case Direction.LEFT:
                    this.cameraAcceleration.x += 0.01
                    if (this.cameraAcceleration.x > 1)
                        this.cameraAcceleration.x = 1
                    break;
                case Direction.DOWN:
                    this.cameraAcceleration.y -= 0.01
                    if (this.cameraAcceleration.y < -1)
                        this.cameraAcceleration.y = -1
                    break;
                case Direction.UP:
                    this.cameraAcceleration.y += 0.01
                    if (this.cameraAcceleration.y > 1)
                        this.cameraAcceleration.y = 1
                    break;            
                default:
                    break;
            }

        } else {
            if (this.cameraAcceleration.x < 0)
                this.cameraAcceleration.x += 0.02
            else if (this.cameraAcceleration.x > 0.05)
                this.cameraAcceleration.x -= 0.02
                
            if (this.cameraAcceleration.y < 0)
                this.cameraAcceleration.y += 0.02
            else if (this.cameraAcceleration.y > 0.05)
                this.cameraAcceleration.y -= 0.02           
        }

        //set the animation frame for the movement    
        if (this.movingPercent > 0) {

            //speed / 10 x terrain
            //60 / 10 = 6,         
            // if (this.movingPercent < 16)
            //     this.currentFrame = 1
            // else if (this.movingPercent < 32)
            //     this.currentFrame = 2
            // else if (this.movingPercent < 48)
            //     this.currentFrame = 0
            // else if (this.movingPercent < 64)
            //     this.currentFrame = 1
            // else if (this.movingPercent < 80)
            //     this.currentFrame = 2
            // else                    
            //     this.currentFrame = 0

            const actualSpeed = this.speed * this.getMapTile().getTile().speed
            
            const animationSpeedMod = actualSpeed > 90 ? 20 : (110 - actualSpeed)

            //110 - 90 = 20

            //110 - 25 = 85
            //110 - 100 = 10

            this.currentFrame = (this.game.frame.timeMs/animationSpeedMod % 3) | 0
            
            if (this.currentFrame == 1)
                this.game.audioManager.SOUNDS.STEP_1.play()
            else if (this.currentFrame == 2)
                this.game.audioManager.SOUNDS.STEP_2.play()
            
        }
        else {
            this.currentFrame = 0
        }
    
        this.img = this.frames[this.currentFrame]

        this.updateConditions()
        this.updateHotKeys()
    }

    keyExhaust : number 
    updateHotKeys() {
        //0 = 48
        //9 = 57

        if (this.game.frame.time < this.keyExhaust)
            return

        const pressedKeys = this.game.gameCommandsDOM.pressedKeys

        for (let i = 0; i < 10; i++) {
            const keycode = i+48
            if (pressedKeys[keycode]) {             
                
                
                //0 is the last numberic key on keyboard lol (after 9)
                const keyAux = i == 0?9:i-1

                const key = pressedKeys[KeyCodes.SHIFT]?this.hotkeys.shift[keyAux]:this.hotkeys.default[keyAux]

                switch (key.type) {
                    case HotkeyType.SPELL:
                        this.castSpell(key.tag, this)
                        break;
                    default:
                        console.log('hotkeytype not implemented ')
                        break;
                }

                this.keyExhaust = this.game.frame.time + 250
            }
        }
    }

    tryGetItem(pos) {
        const mapTile = this.game.map[pos.y][pos.x]
        if (mapTile.itemList != null &&
            mapTile.itemList.length > 0 &&
            mapTile.itemList[mapTile.itemList.length-1].getDefinition().flags & ItemFlags.PICKABLE) {


            if (mapTile.itemList[mapTile.itemList.length-1].id == 62) {
                const gotItem = mapTile.itemList.pop()

                this.gold += gotItem.count

                if (this.game.frame.random > 0.5)                         
                    this.game.audioManager.SOUNDS.COIN_1.play()
                else
                    this.game.audioManager.SOUNDS.COIN_2.play()
                    
                this.game.addComponent(new MagicEffect(pos, this.game, MagicEffect.E_SPARK_ARMOR))

            } else {                
                const slot = this.getFirstFreeInventorySlot()
                if (slot) {
                    const gotItem = mapTile.itemList.pop()
                    slot.item = gotItem
                    this.game.addComponent(new MagicEffect(pos, this.game, MagicEffect.E_SPARK_ARMOR))
                }
                else {
                    this.game.addMessageHistory('You don\'t have more in your inventory.')
                }
                // this.movingItem = gotItem
                this.game.audioManager.SOUNDS.GET_1.play()
            }
        }
    }

    tryUseItem(pos) {
        const mapTile = this.game.map[pos.y][pos.x]
        
        if (mapTile.itemList != null &&
            mapTile.itemList.length > 0) {                

            const item = mapTile.itemList[mapTile.itemList.length-1]

            if (item.getDefinition().flags & ItemFlags.USABLE)  {

                //this.game.addComponent(new MagicEffect({x:pos.x, y:pos.y}, this.game, MagicEffect.E_SMOKE_PUFT))
                this.game.addComponent(new MagicEffect({x:pos.x, y:pos.y}, this.game, MagicEffect.E_SPARK_ARMOR))
                item.use(this)

                return
            }
            else if (item.getDefinition().flags & ItemFlags.CONTAINER) {
                if (this.openedContainer) {
                    if (this.openedContainer == item)
                        this.openedContainer = null
                    else {
                        this.openedContainer = null
                        this.game.addComponent(new MagicEffect(pos, this.game, MagicEffect.E_DIE_BLOOD))
                        setTimeout(() => {
                            this.openedContainer = item
                            this.openedContainerPos = pos
                        })
                    }
                }
                else {
                    this.openedContainer = item
                    this.openedContainerPos = pos
                    this.game.addComponent(new MagicEffect(pos, this.game, MagicEffect.E_DIE_BLOOD))
                }

                return
            } 
            // else if (item.id == 62) {
            //     this.tryGetItem(pos)
            //     return
            // }
        }

        this.game.addMessageHistory('You cannot use this.')
    }

    takeAllLoot() {
        let keepGoing = true

        while (keepGoing) {
            if (!this.openedContainer.content || this.openedContainer.content.length == 0) {
                this.game.audioManager.SOUNDS.GET_1.play()
                this.openedContainer = null
                this.game.addComponent(new MagicEffect(this.openedContainerPos, this.game, MagicEffect.E_DIE_BLOOD))
                break
            }

            if (this.openedContainer.content[this.openedContainer.content.length - 1].id == 62) {
                const item = this.openedContainer.content.pop()
                this.gold += item.count
                item.alive = false
                this.game.audioManager.SOUNDS.COIN_1.play()
            } else {

                const slot = this.game.player.getFirstFreeInventorySlot()

                if (!slot) {
                    this.game.addMessageHistory('You don\'t have enough space in your inventory')
                    break
                }
    
                slot.item = this.openedContainer.content.pop()
            }
        }
    }

    getLoot(item, index) {

        if (!this.openedContainer.content[index]) {
            this.game.addMessageHistory(`Ops, cannot find the item on index ${index}`) 
            return
        }

        if (item.id == 62) {
            this.openedContainer.content.splice(index, 1);
            this.gold += item.count
            this.game.audioManager.SOUNDS.COIN_1.play()
            this.game.addComponent(new MagicEffect(this.openedContainerPos, this.game, MagicEffect.E_DIE_BLOOD))
        } else {

            const slot = this.game.player.getFirstFreeInventorySlot()

            if (slot != null) {
                this.openedContainer.content.splice(index, 1);
                slot.item = item
    
                if (this.openedContainer.content.length == 0)
                    this.openedContainer = null

                this.game.audioManager.SOUNDS.GET_1.play()

                this.game.addComponent(new MagicEffect(this.openedContainerPos, this.game, MagicEffect.E_DIE_BLOOD))
    
            } else {
                this.game.addMessageHistory('You don\'t have enough space in your inventory')
            }
        }
        
    }

    getPixelPositionOnScreen() {
        return {
            x : GameConstants.SCREEN_CENTER_SQM_X * GameConstants.spriteCalculatedSize,
            y : GameConstants.SCREEN_CENTER_SQM_Y * GameConstants.spriteCalculatedSize
        }
    }

    getCalculatedDrawCameraAcceleration() {
        //mousediff sucks = (0.5 - pos.x/width) * 16
        return {x: GameConstants.spriteCalculatedSize/2 * this.cameraAcceleration.x, 
                y: GameConstants.spriteCalculatedSize/2*this.cameraAcceleration.y}
    }

    sumUpAttackAnimation(calculatedCameraAcceleration) {

        const attackModifier = Date.now() - this.lastAttack
        let attackModifierCalc = 0
        if (attackModifier < 200) {
            attackModifierCalc =  (100 - (Math.abs(100 - attackModifier)))/10*2
        }

        //1st just set the direction here...
        if (this.target && this.target.alive) {
            
            if (this.pos.x == this.target.pos.x ) { 
                    
                if (this.pos.y > this.target.pos.y) { // up /\
                    calculatedCameraAcceleration.y += attackModifierCalc

                } else { // down \/
                    calculatedCameraAcceleration.y -= attackModifierCalc
                }

            } else if (this.pos.x > this.target.pos.x) {
                if (this.pos.y == this.target.pos.y) { // > right
                    calculatedCameraAcceleration.x += attackModifierCalc
                    calculatedCameraAcceleration.y += attackModifierCalc/2
                } 
                else if (this.pos.y > this.target.pos.y) { // > \/ right bottom
                    calculatedCameraAcceleration.x += attackModifierCalc
                    calculatedCameraAcceleration.y += attackModifierCalc

                } else { // > /\ right up
                    calculatedCameraAcceleration.x += attackModifierCalc
                    calculatedCameraAcceleration.y -= attackModifierCalc

                }

            } else { //this.pos.x < this.target.pos.x
                if (this.pos.y == this.target.pos.y) { // < left
                    calculatedCameraAcceleration.x -= attackModifierCalc
                    calculatedCameraAcceleration.y += attackModifierCalc/2

                } 
                else if (this.pos.y > this.target.pos.y) { // < \/ left bottom
                    calculatedCameraAcceleration.x -= attackModifierCalc
                    calculatedCameraAcceleration.y += attackModifierCalc

                } else { // < /\ left up
                    calculatedCameraAcceleration.x -= attackModifierCalc
                    calculatedCameraAcceleration.y -= attackModifierCalc
                }
            }

        }

        //jump 4 steps
        //first make the 4 steps, then think about dir up and down
    }

    drawWithFocus(screenPixelsPosition) {

    }

    // northImg
    // westImg
    // southImg
    
    draw() {
        if (!this.alive)
            return

        const screenPixelsPosition = this.getPixelPositionOnScreen();
        const calculatedCameraAcceleration = this.getCalculatedDrawCameraAcceleration()

        super.draw(calculatedCameraAcceleration)
        
        this.sumUpAttackAnimation(calculatedCameraAcceleration)
    
        this.game.ctx.shadowOffsetX = GameConstants.GLOBAL_SHADOW_X
        this.game.ctx.shadowOffsetY = GameConstants.GLOBAL_SHADOW_Y
        this.game.ctx.shadowBlur = GameConstants.GLOBAL_SHADOW_BLUR
        this.game.ctx.shadowColor = GameConstants.GLOBAL_SHADOW_COLOR

        // if (this.lookingDirection == Direction.UP) {
        //     if (!this.northImg) {
        //         this.northImg= new Array

        //         this.northImg[0] = new Image
        //         this.northImg[0].src = "assets/jav_ws_N_00.png"
                
        //         this.northImg[1] = new Image
        //         this.northImg[1].src = "assets/jav_ws_N_01.png"
                
        //         this.northImg[2] = new Image
        //         this.northImg[2].src = "assets/jav_ws_N_02.png"
        //     }
        //     this.img = this.northImg[this.currentFrame]
        // }
        // else if (this.lookingDirection == Direction.LEFT) {
        //     if (!this.westImg) {
        //         this.westImg= new Array

        //         this.westImg[0] = new Image
        //         this.westImg[0].src = "assets/jav_ws_W_00.png"
                
        //         this.westImg[1] = new Image
        //         this.westImg[1].src = "assets/jav_ws_W_01.png"
                
        //         this.westImg[2] = new Image
        //         this.westImg[2].src = "assets/jav_ws_W_02.png"
        //     }
        //     this.img = this.westImg[this.currentFrame]
        // }
        // else if (this.lookingDirection == Direction.DOWN) {
        //     if (!this.southImg) {
        //         this.southImg= new Array

        //         this.southImg[0] = new Image
        //         this.southImg[0].src = "assets/jav_ws_S_00.png"
                
        //         this.southImg[1] = new Image
        //         this.southImg[1].src = "assets/jav_ws_S_01.png"
                
        //         this.southImg[2] = new Image
        //         this.southImg[2].src = "assets/jav_ws_S_02.png"
        //     }
        //     this.img = this.southImg[this.currentFrame]
        // }

        this.game.ctx.drawImage(this.img,
            screenPixelsPosition.x - calculatedCameraAcceleration.x, // + attackModifierCalc,
            screenPixelsPosition.y - calculatedCameraAcceleration.y,// - attackModifierCalc,// - 36,
            this.img.width * GameConstants.SPRITE_SIZE_MULTIPLIER,
            this.img.height * GameConstants.SPRITE_SIZE_MULTIPLIER)// + 36)    
            
        this.game.ctx.shadowOffsetX = 0;
        this.game.ctx.shadowOffsetY = 0;
        this.game.ctx.shadowBlur = 0;

        const light = this.getLight()

        if (light)
            this.game.nightEffectManager.addLightEffect(screenPixelsPosition, light.intensity, light.color)
    }

    
    speak(message:string) {
        const spellSpeech = new SpellSpeech()
        if (spellSpeech.tryToCast(this, message)) 
            this.game.addComponent(new Speech(this.pos, `${this.name}: ` + message, this.game))
    }

    castSpell(spellType : SpellType, target : Creature) : boolean {
        if (this.game.frame.time <= this.exhaustedTimer) {
            this.game.addComponent(new MagicEffect(this.pos, this.game, MagicEffect.E_SMOKE_PUFT))
            this.game.addMessageHistory('You are exhausted.')
            return false
        }

        const spell = spellsDefinition[spellType]

        if (this.level < spell.level) {
            this.game.addComponent(new MagicEffect(this.pos, this.game, MagicEffect.E_SMOKE_PUFT))
            this.game.addMessageHistory('You do not have enough level to cast this spell.')
            return false
        }

        if (this.mana < spell.mana) {
            this.game.addComponent(new MagicEffect(this.pos, this.game, MagicEffect.E_SMOKE_PUFT))
            this.game.addMessageHistory('You do not have enough mana to cast this spell.')
            return false
        }
        
        const formula = spell.getSpellFormulaPlayer(this)

        if (spell.cast(this, target, formula)) {
            this.mana -= spell.mana
            this.addCondition(ConditionType.EXHAUSTED,1000)
            this.exhaustedTimer = this.game.frame.time + 1000
            return true
        } else {
            return false
        }
    }
    
    die(attacker:Creature) {
        super.die(attacker)
        
        this.game.map[this.pos.y][this.pos.x].addItem(this.game, this.pos, new Item(63,null,null,null,30000))
        this.game.audioManager.SOUNDS.DIE.play()
        this.game.pointer.alive = false
        this.mana = 0
        this.movingPercent = 0
        this.game.drawMovingModifierX = 0
        this.game.drawMovingModifierY = 0
        let prefix = ''
        if (!attacker['player'])
            prefix = 'a '
        this.game.screenMessageManager.addMessage(`You are dead.\nYou were killed by ${prefix}${attacker.getName()}.`)        
        this.alive = false
        this.applyDeathPenalties()
    }

    applyDeathPenalties() {
        this.experience = Math.floor(this.experience*0.95)
        this.gold = Math.floor(this.gold*0.95)

        //reqxp = Math.pow(2,level-1) * 100 - 100
        const prev_level = (this.level)

        this.level = 1
        
        while (this.experience >= this.getLevelRequiredExperience(this.level+1)) {
            this.level++
            // this.max_health+= 30
            // this.max_mana += 10
            // this.speed += 1
            // this.health = this.max_health
            // this.mana = this.max_mana
        }
        
        if (this.level != prev_level) {
            this.game.addMessageHistory(`You have downgraded from level ${prev_level} to level ${this.level}.`)       
        }
    }

    respawn() {
        this.health = 185 + (this.level-1)*30
        this.max_health = 185 + (this.level-1)*30
        this.mana = 100 + (this.level-1)*10
        this.max_mana = 100 + (this.level-1)*10
        this.speed = 25 + (this.level-1)*1
        this.name = "Hagar"
        this.alive = true
        this.game.pointer.alive = false
        this.movingPercent = 0
        this.movingToDirection = null
        this.conditions = new Array

        
        const oldPosition = { x : this.pos.x, y : this.pos.y }
        
        this.pos.x = GameConstants.PLAYER_INITIAL_POSITION.x
        this.pos.y = GameConstants.PLAYER_INITIAL_POSITION.y
        this.game.addComponent(new MagicEffect({x:this.pos.x,y:this.pos.y}, this.game, 4)) 

        this.moveToTilePosition(oldPosition)
    }
    
    enableGodMode() {        
        if (this.god == false) {
            this.game.screenMessageManager.addMessage("Entering map editor mode.\nYou have received god mode.")
            this.health = 99999
            this.max_health = 99999
            this.mana = 99999
            this.max_mana = 99999
            this.speed = 400
            this.god = true
            this.alive = true
        }
    }

    updateInventoryItems() {
        for (let i = 0; i < this.inventory.length; i++) {
            const slot = this.inventory[i];
            if (slot.item && !slot.item.alive) 
                slot.item = null
        }

        for (let i = 0; i < this.equipment.length; i++) {
            const element = this.equipment[i];
            if (element && !element.alive) 
                this.equipment[i] = null
        }

        // if (this.openedContainer) {
        //     for (let i = 0; i < this.openedContainer.content.length; i++) {
        //         const item = this.openedContainer.content[i];
        //         if (item.alive == false)
        //         {
                       
        //         }
        //     }
        // }
    }
    
    getFirstFreeInventorySlot() : InventorySlot {
        for (let i = 0; i < this.game.player.inventory.length; i++) {
            const slot = this.game.player.inventory[i];
            if (slot.item != null)
                continue
            else
                return slot            
        }
        return null
    }

    getName(): string {
        return this.name
    }

    getAttackDamage() : number {

        const leftHand = this.equipment[SlotType.LEFT_HAND]

        if (leftHand) {
            const leftHandDamage = leftHand.getDefinition().properties[ItemProperties.ATTACK_DAMAGE]
            if (leftHandDamage)
                return leftHandDamage
        }
        
        const rightHand = this.equipment[SlotType.RIGHT_HAND]

        if (rightHand) {
            const rightHandDamage = rightHand.getDefinition().properties[ItemProperties.ATTACK_DAMAGE]
    
            if (rightHandDamage)
                return rightHandDamage

        }

        return 5
    }

    getAttackRange() : number {

        const leftHand = this.equipment[SlotType.LEFT_HAND]

        if (leftHand) {
            const leftHandRange = leftHand.getDefinition().properties[ItemProperties.ATTACK_RANGE]
            if (leftHandRange)
                return leftHandRange
        }
        
        const rightHand = this.equipment[SlotType.RIGHT_HAND]

        if (rightHand) {
            const rightHandRange = rightHand.getDefinition().properties[ItemProperties.ATTACK_RANGE]
    
            if (rightHandRange)
                return rightHandRange

        }

        return 1
    }
    
    getAttackShootEffect() : number {

        const leftHand = this.equipment[SlotType.LEFT_HAND]

        if (leftHand) {
            const leftHandSE = leftHand.getDefinition().properties[ItemProperties.SHOOT_EFFECT]
            if (leftHandSE)
                return leftHandSE
        }
        
        const rightHand = this.equipment[SlotType.LEFT_HAND]

        if (rightHand) {
            const rightHandSE = rightHand.getDefinition().properties[ItemProperties.SHOOT_EFFECT]
    
            if (rightHandSE)
                return rightHandSE

        }

        return null
    }
    
    getArmorItem(slot:SlotType):number {
        const equip = this.equipment[slot]
        if (equip) {
            const armor = this.equipment[slot].getDefinition().properties[ItemProperties.ARMOR] 
            if (armor)
                return armor
            else 
                return 0
        }
        return 0
    }

    getArmor(): number {
        return  this.getArmorItem(SlotType.HEAD) +
                this.getArmorItem(SlotType.CHEST) +
                this.getArmorItem(SlotType.LEGS) +
                this.getArmorItem(SlotType.FEET) +
                this.getArmorItem(SlotType.NECK)
    }

    getSpeed() {
        let itemsSpeed = 0

        const equipFeet = this.equipment[SlotType.FEET]

        if (equipFeet != null && equipFeet.getDefinition().properties) {
            const equipFeetSpeed = equipFeet.getDefinition().properties[ItemProperties.SPEED]
            
            if (equipFeet) {
                itemsSpeed+= equipFeetSpeed
            }
        }

        return super.getSpeed() + itemsSpeed
    }
}
