export enum HotkeyType {
    EMPTY = 0,
    SPELL = 1,
    ITEM = 2,
    GM_ACTIONS = 4
}

export class GameCommandsHotkey {
    constructor(public tag : number = null, public type:HotkeyType = HotkeyType.EMPTY) {

    }
}