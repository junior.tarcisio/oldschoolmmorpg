import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { StatusBarComponent } from './game-ui/status-bar/status-bar.component';
import { ActionBarComponent } from './game-ui/action-bar/action-bar.component';
import { SkillsBarComponent } from './game-ui/skills-bar/skills-bar.component';
import { BattleBarComponent } from './game-ui/battle-bar/battle-bar.component';
import { MapEditorSideBarComponent } from './game-ui/map-editor-side-bar/map-editor-side-bar.component';
import { PerformanceMonitorComponent } from './game-ui/performance-monitor/performance-monitor.component';
import { MessageHistory } from './game-ui/message-history/message-history.component';
import { GameTimerComponent } from './game-ui/game-timer/game-timer.component';
import { PickingUpItemComponent } from './game-ui/picking-up-item/picking-up-item.component';
import { InventoryComponent } from './game-ui/inventory/inventory.component';
import { EquipmentComponent } from './game-ui/equipment/equipment.component';
import { EquipmentSlotComponent } from './game-ui/equipment/equipment-slot/equipment-slot.component';
import { NpcTradeComponent } from './game-ui/npc-dialog/npc-trade/npc-trade.component';
import { NpcDialogComponent } from './game-ui/npc-dialog/npc-dialog.component';
import { NpcTalkComponent } from './game-ui/npc-dialog/npc-talk/npc-talk.component';
import { RespawnDialogComponent } from './game-ui/respawn-dialog/respawn-dialog.component';
import { LootDialogComponent } from './game-ui/loot-dialog/loot-dialog.component';
import { ConditionBarComponent } from './game-ui/condition-bar/condition-bar.component';


@NgModule({
  declarations: [
    AppComponent,
    StatusBarComponent,
    ActionBarComponent,
    SkillsBarComponent,
    BattleBarComponent,
    MapEditorSideBarComponent,
    PerformanceMonitorComponent,
    MessageHistory,
    GameTimerComponent,
    PickingUpItemComponent,
    InventoryComponent,
    EquipmentComponent,
    EquipmentSlotComponent,
    NpcTradeComponent,
    NpcDialogComponent,
    NpcTalkComponent,
    RespawnDialogComponent,
    LootDialogComponent,
    ConditionBarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
