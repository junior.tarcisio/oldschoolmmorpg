import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../game-engine/game';
import { interval } from 'rxjs/observable/interval'
const sourceJsMemoryObservable = interval(1000)

@Component({
  selector: 'app-performance-monitor',
  templateUrl: './performance-monitor.component.html',
  styleUrls: ['./performance-monitor.component.css']
})
export class PerformanceMonitorComponent implements OnInit {
  @Input() game : Game
  memory : any = {
    jsHeapSizeLimit : 0,
    usedJSHeapSize : 0,
    usedJSHeapSizeDiff : 0,
    totalJSHeapSize: 0
  }

  constructor() { }

  ngOnInit() {
    sourceJsMemoryObservable.subscribe(val => {      
      const m = eval('window.performance.memory')
      const memdiff = +m.usedJSHeapSize - +this.memory.usedJSHeapSize
      this.memory = {
        jsHeapSizeLimit: m.jsHeapSizeLimit,
        usedJSHeapSize: m.usedJSHeapSize,
        usedJSHeapSizeDiff : memdiff,
        totalJSHeapSize: m.totalJSHeapSize
      }
    });
    
  }
  
}
