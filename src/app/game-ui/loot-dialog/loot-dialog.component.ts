import DraggingManager from './../dragging-manager';
import { Item } from './../../game-engine/things/items/item';
import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../game-engine/game';
import itemsDefinition from '../../game-engine/things/items/items-definition';

@Component({
  selector: 'app-loot-dialog',
  templateUrl: './loot-dialog.component.html',
  styleUrls: ['./loot-dialog.component.css']
})
export class LootDialogComponent implements OnInit {
  @Input() game : Game
  draggingManager = DraggingManager

  constructor() { }

  ngOnInit() {
  }

  contextMenuDisableX() {
    return false
  }

  getTitle() {
    if (!this.game || !this.game.player || ! this.game.player.openedContainer)
      return
      
      return this.game.player.openedContainer.getDefinition().name 
  }

  getDecayID() {
    if (!this.game || !this.game.player || ! this.game.player.openedContainer)
      return null

    return this.game.player.openedContainer.decayAt
  }

  getTitleSprite(item:Item) {
    if (!item) return ""

    const itemdef = itemsDefinition[item.id]
    if (itemdef) 
      return `url('assets/ItemSpriteTable.png') 0px -${itemdef.frames[0] * 24}px`
    else 
      return  ``
  }

  selectItem(item, index) {
    this.game.player.getLoot(item,index)
  }
  
  useItem(item:Item) {
    item.use(this.game.player)
    return false
  }
  
  getItemsLoot() {
    if (!this.game || !this.game.player || !this.game.player.openedContainer || !this.game.player.openedContainer.content)
      return

    return this.game.player.openedContainer.content.filter(x=> x.alive)
  }

  getItemSprite(item:Item) {
    if (!item) return ""

    const itemdef = itemsDefinition[item.id]
    if (itemdef) 
      return `url('assets/ItemSpriteTable.png') 0px -${itemdef.frames[0] * 24}px`
    else 
      return  ``
  }
  
  takeAll() {
    this.game.player.takeAllLoot()
    return false
  }

  close(){
    this.game.player.openedContainer = null
    return false
  }
}
