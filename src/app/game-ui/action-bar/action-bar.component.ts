import { GameCommandsHotkey, HotkeyType } from './../../game-engine/game-commands-hotkey';
import { Component, OnInit, Input } from '@angular/core'
import Player from '../../game-engine/things/player'
import spellsDefinition from '../../game-engine/spells/spells-definition';
import { bounce, fadeIn, rotatorVertical } from '../animations/animations';
import { KeyCodes } from '../../game-engine/enumerables/key-codes';

@Component({
  selector: 'app-action-bar',
  templateUrl: './action-bar.component.html',
  styleUrls: ['./action-bar.component.css'],
  animations : [rotatorVertical()]
})
export class ActionBarComponent implements OnInit {
  @Input() player : Player
  @Input() huds
  gmBarHidden = false
  sidebar = false

  constructor() { }

  ngOnInit() {
  }

  getHotkeyType() {
    if(!this.player || !this.player.game)
        return

    const pressedKeys = this.player.game.gameCommandsDOM.pressedKeys

    if (pressedKeys[KeyCodes.SHIFT] == true)
      return 1
    // else if (pressedKeys[KeyCodes.ALT] == true)
    //   return 2
    else       
      return 0
  }

  editSlot(slot:GameCommandsHotkey) {
    alert('under development')
  }

  useSlot(slot:GameCommandsHotkey) {
    if (slot.type == HotkeyType.SPELL)
      this.player.castSpell(slot.tag, this.player)
  }
  
  getSlotIcon(slot:GameCommandsHotkey) {
    if (slot.type == HotkeyType.SPELL)
      return spellsDefinition[slot.tag].ico
    else 
      return '&nbsp;'
  }
  
  getSlotDesc(slot:GameCommandsHotkey) {
    if (slot.type == HotkeyType.SPELL)
      return spellsDefinition[slot.tag].name
    else 
      return 'Empty action slot'
  }

  earthquake() {
    this.player.earthquakeCounter = this.player.game.frame.time + 10000
  }
  
  addMonster() {
    if (!this.player.god)
      this.player.enableGodMode()
    else
      this.player.god = false
  }

  normalizeHighLevel() {
    this.player.god = false
        
    this.player.receiveExperience(10000000000000)

    this.player.max_health = 350
    this.player.health = 350
    this.player.max_mana = 1500
    this.player.mana = 1500
    this.player.speed = 40
    this.player.gold = 1000000
  }

  toggleMapedTiles() {
    if (this.huds.mapeditor == true) {
      if (this.player.game.mapEditor.selectedTab == 0)
        this.huds.mapeditor = false
      else
        this.player.game.mapEditor.selectedTab = 0

    } else {
      this.huds.mapeditor = true
      this.player.game.mapEditor.selectedTab = 0
    }
  }

  toggleMapedMonsters() {
    if (this.huds.mapeditor == true) {
      if (this.player.game.mapEditor.selectedTab == 2)
        this.huds.mapeditor = false
      else
        this.player.game.mapEditor.selectedTab = 2

    } else {
      this.huds.mapeditor = true
      this.player.game.mapEditor.selectedTab =2
    }
  }

  rain() {
      if (this.player.game.weatherManager.rainAngleX <= 0)
          this.player.game.weatherManager.rainAngleX = 25
      else 
          this.player.game.weatherManager.rainAngleX =  - 10
  }
}
