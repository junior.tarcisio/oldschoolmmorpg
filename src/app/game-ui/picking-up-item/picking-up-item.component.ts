import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../game-engine/game';
import { ItemDefinition } from '../../game-engine/things/items/items-definition';

@Component({
  selector: 'app-picking-up-item',
  templateUrl: './picking-up-item.component.html',
  styleUrls: ['./picking-up-item.component.css']
})
export class PickingUpItemComponent implements OnInit {
  @Input() game : Game

  constructor() { }

  ngOnInit() {
  }

  getItemSprite() {
   // return `url('assets/ItemSpriteTable.png') 0px -${item.frames[item.currentFrame] * 24}px`
  }
}
