class DraggingManager {
    
  isDragging = false
  ref
  relativeCoordinates

  startDrag(e, ref) {
    console.log(e)
    console.log(ref)
    this.isDragging = true
    this.ref = ref
    //this.relativeCoordinates = this.getRelativeCoordinates(e, ref)
  }

  endDrag(e) {
    this.isDragging = false
  }

  checkDragging(e) {
    if (!this.isDragging) return
    this.ref.style.position = "fixed"
    this.ref.style.top = e.clientY - 20 + "px"
    this.ref.style.left = e.clientX - 50  + "px"
  }

  getRelativeCoordinates (event, referenceElement) {

    const position = {
      x: event.pageX,
      y: event.pageY
    };
  
    const offset = {
      left: referenceElement.offsetLeft,
      top: referenceElement.offsetTop
    };
  
    let reference = referenceElement.offsetParent;
  
    while(reference !== null || reference !== undefined){
      offset.left += reference.offsetLeft;
      offset.top += reference.offsetTop;
      reference = reference.offsetParent;
    }
  
    return { 
      x: position.x - offset.left,
      y: position.y - offset.top,
    }; 
  
  }
}

export default new DraggingManager()