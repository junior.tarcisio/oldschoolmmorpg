import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../game-engine/game';

@Component({
  selector: 'app-game-timer',
  templateUrl: './game-timer.component.html',
  styleUrls: ['./game-timer.component.css']
})
export class GameTimerComponent implements OnInit {
  @Input() game : Game

  constructor() { }

  ngOnInit() {
  }

  getDayTime() {
    if (this.game == null) return ""
    const getMinutes = (time) => ((time%1)*60)|0
    const getHour = (time) => time|0
    return `${ getHour(this.game.nightEffectManager.dayHour).toString().padStart(2,'0') }:${ getMinutes(this.game.nightEffectManager.dayHour).toString().padStart(2,'0') }` 
  }
}
