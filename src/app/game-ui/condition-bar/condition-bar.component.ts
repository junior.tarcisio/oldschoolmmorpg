import { Component, OnInit, Input } from '@angular/core';
import { conditionAnimation } from '../animations/animations';
import Player from '../../game-engine/things/player';
import { interval } from 'rxjs/observable/interval'
import { Condition } from '../../game-engine/conditions/condition';
const conditionsObservable = interval(100)

@Component({
  selector: 'app-condition-bar',
  templateUrl: './condition-bar.component.html',
  styleUrls: ['./condition-bar.component.css'],
  animations : [conditionAnimation()]
})
export class ConditionBarComponent implements OnInit {
  @Input() player : Player

  conditions : Array<Condition>
  now: number
  constructor() { }

  ngOnInit() {
    conditionsObservable.subscribe(val => {      
      this.now = Date.now()
      this.conditions = []
      if (!this.player || !this.player.alive || !this.player.conditions) {
        return 
      }

      for (const key in this.player.conditions) {
        const condition : Condition = this.player.conditions[key]
        
        if (condition == null || !condition.alive)
          continue

        this.conditions.push(condition)
      }
    })
  }
}
