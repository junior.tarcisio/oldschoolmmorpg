import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../../game-engine/game';

@Component({
  selector: 'app-npc-talk',
  templateUrl: './npc-talk.component.html',
  styleUrls: ['./npc-talk.component.css']
})
export class NpcTalkComponent implements OnInit {
  @Input() game : Game

  constructor() { }

  ngOnInit() {
  }

}
