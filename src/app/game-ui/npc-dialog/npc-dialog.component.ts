import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../game-engine/game';
import { bounce, fadeOutGrowing, fadeIn } from '../animations/animations';
import DraggingManager from './../dragging-manager';

@Component({
  selector: 'app-npc-dialog',
  templateUrl: './npc-dialog.component.html',
  styleUrls: ['./npc-dialog.component.css'],
  animations: [fadeIn()]
})
export class NpcDialogComponent implements OnInit {
  @Input() game : Game
  tabSelection : string = 'talk'
  draggingManager = DraggingManager

  constructor() { }

  ngOnInit() {
  }


  contextMenuDisableX() {
    return false
  }

  selectTab(name: string) {
    this.tabSelection = name
  }
}
