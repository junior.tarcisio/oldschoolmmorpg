import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../../game-engine/game';
import itemsDefinition from '../../../game-engine/things/items/items-definition';

@Component({
  selector: 'app-npc-trade',
  templateUrl: './npc-trade.component.html',
  styleUrls: ['./npc-trade.component.css']
})
export class NpcTradeComponent implements OnInit {
  @Input() game : Game
  selectedItem : any

  constructor() { }

  ngOnInit() {
  }


  contextMenuDisableX() {
    return false
  }

  selectItem(item) {
    this.game.player.npcTarget.tradeItem(item)
  }
  
  getItemSprite(item) {
    if (!item) return ""

    if (item.itemDefinition) {
      const itemdef = itemsDefinition[item.itemDefinition]
      return `url('assets/ItemSpriteTable.png') 0px -${itemdef.frames[0] * 24}px`
    }
    else 
      return  ``
  }

  getGoldSprite() {
    return "url('assets/ItemSpriteTable.png') 0px -1776px"
  }
  
  getItemName(item) {
    if (!item) return ""
    return itemsDefinition[item.itemDefinition].name
  }

  getItemWeight(item) {
    if (!item) return ""
    return itemsDefinition[item.itemDefinition].weight
  }

  showDetail(item) {
    this.selectedItem = item
  }

  hideDetail() {
    this.selectedItem = null
  }
}
