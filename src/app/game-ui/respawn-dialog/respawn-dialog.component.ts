import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../game-engine/game';
import DraggingManager from './../dragging-manager';

@Component({
  selector: 'app-respawn-dialog',
  templateUrl: './respawn-dialog.component.html',
  styleUrls: ['./respawn-dialog.component.css']
})
export class RespawnDialogComponent implements OnInit {
  @Input() game : Game
  draggingManager = DraggingManager

  constructor() { }

  ngOnInit() {
  }
  contextMenuDisableX() {
    return false
  }

}
