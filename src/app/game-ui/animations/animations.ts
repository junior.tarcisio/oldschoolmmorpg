import {
    trigger,
    state,
    style,
    animate,
    transition,
    AnimationTriggerMetadata,
    keyframes
  } from '@angular/animations';


  export function bounce() : AnimationTriggerMetadata {
    return trigger('dialogAnimation', [
        state('in', style({
            opacity: 1,
            transform: 'translateY(0)'
        })),
        transition('void => *',[
            animate(300,keyframes([
                style({
                    opacity:0,
                    transform: 'translateY(-80px)',
                    offset : 0
                }),
                style({
                    opacity:1,
                    transform: 'translateY(0px)',
                    offset : 0.5
                }),
                style({
                    opacity:1,
                    transform: 'translateY(-10px)',
                    offset : 0.75
                }),
                style({
                    opacity:1,
                    transform: 'translateY(0px)',
                    offset : 1
                })
            ]))
        ]),
        transition('* => void',[                
            animate(200, style({
                opacity:0,
                transform: 'translateY(-80px)'
            }))
        ])
    ]);
  }
  

export function fadeIn() : AnimationTriggerMetadata {
    return trigger('fadeIn', [
        state('in', style({
            opacity: 1
        })),
        transition('void => *',[
            style({ opacity:0, transform: 'translateY(-10px)' }), animate(300)
        ])
    ]);
}

export function fadeOutGrowing() : AnimationTriggerMetadata {
    return trigger('fadeOutGrowing', [
        state('in', style({
            opacity: 1
        })),
        transition('void => *',[
            style({ opacity:0, transform: 'translateY(-10px)' }), animate(300)
        ]),
        transition('* => void',[
            animate(300,keyframes([
                style({
                    opacity:1,
                    transform: 'scale(1)',
                    offset : 0
                }),
                style({
                    opacity: 0,
                    transform: 'scale(1.5)',
                    offset : 1
                })
            ]))
        ]),
    ]);
}

export function grow() : AnimationTriggerMetadata {
    return trigger('grow', [
        state('in', style({
            transform: 'scale(1)'
        })),
        transition('void => *',[
            style({ transform: 'scale(0)' }), animate(300)
        ])
    ]);
}


export function rotatorVertical() : AnimationTriggerMetadata {
    return trigger('rotatorVertical', [
        state('in', style({
            opacity: 1
        })),
        transition('void => *',[
            animate(90,keyframes([
                style({
                    position: 'relative',
                    opacity:0, 
                    transform: 'translateY(-10px)',
                    offset : 0
                }),
                style({ 
                    position: 'relative',
                    opacity:1, 
                    transform: 'translateY(0px)' ,
                    offset : 1
                }), 
            ]))
        ]),
        transition('* => void',[
            animate(90,keyframes([
                style({
                    position: 'absolute',
                    top:0,
                    left:0,
                    opacity:1, 
                    transform: 'translateY(0px)',
                    offset : 0
                }),
                style({ 
                    position: 'absolute',
                    top:0,
                    left:0,
                    opacity:0, 
                    transform: 'translateY(10px)' ,
                    offset : 1
                }), 
            ]))
        ]),
    ]);
}


export function conditionAnimation() : AnimationTriggerMetadata {
    return trigger('conditionAnimation', [
        state('in', style({
            opacity: 1
        })),
        transition('void => *',[
            animate(150,keyframes([
                style({
                    opacity:0, 
                    transform: 'translateY(-10px)',
                    offset : 0
                }),
                style({ 
                    opacity:1, 
                    transform: 'translateY(0px)' ,
                    offset : 1
                }), 
            ]))
        ]),
        transition('* => void',[
            animate(150,keyframes([
                style({
                    opacity:1, 
                    transform: 'translateY(0px)',
                    offset : 0
                }),
                style({ 
                    opacity:0, 
                    transform: 'translateY(10px)' ,
                    offset : 1
                }), 
            ]))
        ]),
    ]);
}


