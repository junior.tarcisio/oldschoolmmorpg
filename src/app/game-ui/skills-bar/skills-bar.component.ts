import { Component, OnInit, Input } from '@angular/core';
import Player from '../../game-engine/things/player';
import DraggingManager from './../dragging-manager';

@Component({
  selector: 'app-skills-bar',
  templateUrl: './skills-bar.component.html',
  styleUrls: ['./skills-bar.component.css']
})
export class SkillsBarComponent implements OnInit {
  @Input() player : Player
  tooltip
  draggingManager = DraggingManager

  constructor() { }

  ngOnInit() {
  }

  getLevelBarSize() { 
    if (!this.player)
      return 0;
    
    const requiredxpOldLevel = this.player.getLevelRequiredExperience(this.player.level);
    const requiredxpNextLevel = this.player.getLevelRequiredExperience(this.player.level+1) - requiredxpOldLevel;

    const passedxpSinceOldLevel = this.player.experience - requiredxpOldLevel 
    this.tooltip = requiredxpNextLevel - passedxpSinceOldLevel;
    return (passedxpSinceOldLevel / requiredxpNextLevel * 100) + "%";
  }

  contextMenuDisableX() {
    return false
  }
}
