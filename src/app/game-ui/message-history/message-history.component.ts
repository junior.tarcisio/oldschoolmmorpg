import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../game-engine/game';

@Component({
  selector: 'app-message-history',
  templateUrl: './message-history.component.html',
  styleUrls: ['./message-history.component.css']
})
export class MessageHistory implements OnInit {
  @Input() game : Game
  // @ViewChild('messageHistory') messageHistory : ElementRef not working...

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.game.callbackMessageHistory = this.onMessageHistory
  }

  onMessageHistory() {
    setTimeout(() => {
      const messageHistory = document.getElementById('message-history')
      messageHistory.scrollTop = messageHistory.scrollHeight
    },0)
  }
}
