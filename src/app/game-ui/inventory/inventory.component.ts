import DraggingManager from './../dragging-manager';
import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../game-engine/game';
import { InventorySlot } from '../../game-engine/things/inventory-slot';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  @Input() game : Game
  draggingManager = DraggingManager

  constructor() { }

  ngOnInit() {
  }

  selectSlot(slot:InventorySlot) {
    if (this.game.player.movingItem) {
      if (slot.item) {
        const slotItem = slot.item 
        slot.item = this.game.player.movingItem
        this.game.player.movingItem = slotItem
      } else {
        slot.item = this.game.player.movingItem
        this.game.player.movingItem = null
      }
      this.game.audioManager.SOUNDS.PUT_1.play()
    } 
    else if (slot.item) {
      this.game.player.movingItem = slot.item 
      slot.item = null
      this.game.audioManager.SOUNDS.GET_1.play()
    }
  }

  useSlot(slot:InventorySlot) {
    if (!slot.item) return
    slot.item.use(this.game.player)
    return false
  }

  getItemSprite(slot:InventorySlot) {
    const item = slot.item

    if (item) {
      const itemdef = item.getDefinition()
      return `url('assets/ItemSpriteTable.png') 0px -${itemdef.frames[itemdef.getCurrentFrame(this.game.frame.time)] * 24}px`
    }
    else 
      return  ``
  }


  contextMenuDisableX() {
    return false
  }

  getGoldSprite() {
    return "url('assets/ItemSpriteTable.png') 0px -1776px"
  }
}
