import { Game } from './../../game-engine/game';
import { Component, OnInit, Input } from '@angular/core';
import { Creature } from '../../game-engine/things/creature';
import Player from '../../game-engine/things/player';

@Component({
  selector: 'app-battle-bar',
  templateUrl: './battle-bar.component.html',
  styleUrls: ['./battle-bar.component.css']
})
export class BattleBarComponent implements OnInit {
  @Input() game : Game
  
  constructor() { }

  ngOnInit() {
  }

  getCreatures() : Array<Creature>  {
    let creatures = new Array<Creature>();

    if (this.game == null || !this.game.running)
      return creatures

    // this.game.componentList.forEach(x=> {
    //   if (x instanceof Creature && !(x instanceof Player) && x.alive)
    //     creatures.push(x)        
    // })

    return creatures
  }

  isPlayerTarget = (creature : Creature) : boolean => 
    this.game.player.target && 
    this.game.player.target === creature
  
  getHealthBarSize(creature:Creature) {
    return creature.health / creature.max_health * 100 + "%"
  }

  getHealthbarColor(creature:Creature) {
    const hpPercentage = creature.health / creature.max_health

    if (hpPercentage > 0.66)
      return "#0F0"
    else if (hpPercentage > 0.33)
      return "#FF0"
    else
      return "#F00"
  }

  setTarget = (creature:Creature) => this.game.player.target = creature
}
