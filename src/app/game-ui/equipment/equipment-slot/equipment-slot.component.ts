import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../../game-engine/game';
import { Item } from '../../../game-engine/things/items/item';
import { ItemProperties } from '../../../game-engine/things/items/items-definition';

@Component({
  selector: 'app-equipment-slot',
  templateUrl: './equipment-slot.component.html',
  styleUrls: ['./equipment-slot.component.css']
})
export class EquipmentSlotComponent implements OnInit {
  @Input() game : Game
  @Input() slotType : number

  constructor() { }

  ngOnInit() {
  }

  getItemSprite() {
    if (!this.game || !this.game.player)
      return 
      
    const item : Item = this.game.player.equipment[this.slotType]
    if (item) {
      const itemdef = item.getDefinition()
      return `url('assets/ItemSpriteTable.png') 0px -${itemdef.frames[itemdef.getCurrentFrame(this.game.frame.time)] * 24}px`
    }
    else 
      return  ``
  }

  
  selectSlot() {
    const item : Item = this.game.player.equipment[this.slotType]

    if (this.game.player.movingItem) {

      const itemSlotType = this.game.player.movingItem.getDefinition().properties[ItemProperties.SLOT_TYPE]

      if (!itemSlotType || !(itemSlotType & this.slotType) ) {
        this.game.addMessageHistory("You cannot wear this item on that slot")
        return
      }

      if (item) {
        const auxItem = item
        this.game.player.equipment[this.slotType] = this.game.player.movingItem
        this.game.player.movingItem = auxItem
      } else {
        this.game.player.equipment[this.slotType] = this.game.player.movingItem
        this.game.player.movingItem = null
      }
      this.game.audioManager.SOUNDS.PUT_1.play()
    } 
    else if (item) {
      this.game.player.movingItem = item
      this.game.player.equipment[this.slotType] = null
      this.game.audioManager.SOUNDS.GET_1.play()
    }
  }

  useSlot() {
    if (!this.game.player.equipment[this.slotType]) return
    this.game.player.equipment[this.slotType].use(this.game.player)
    return false
  }

}
