import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../game-engine/game';
import DraggingManager from './../dragging-manager';

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css']
})
export class EquipmentComponent implements OnInit {
  @Input() game : Game
  draggingManager = DraggingManager

  constructor() { }

  ngOnInit() {
  }

  contextMenuDisableX() {
    return false
  }

}
