import { Tile } from './../../game-engine/map/tile';
import { Component, OnInit, Input, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { Game } from '../../game-engine/game';
import tiles from '../../game-engine/map/tiles'
import itemsDefinition, { ItemDefinition } from '../../game-engine/things/items/items-definition'
import { MapEditorSelectionType } from '../../game-engine/map/map-editor';
import monstersDefinition, { MonsterDefinition } from '../../game-engine/things/monsters/monster-definition';
import { interval } from 'rxjs/observable/interval';
const frameInterval = interval(100)

@Component({
  selector: 'app-map-editor-side-bar',
  templateUrl: './map-editor-side-bar.component.html',
  styleUrls: ['./map-editor-side-bar.component.css']
})
export class MapEditorSideBarComponent implements OnInit, AfterViewChecked {
  @Input() game : Game
  tiles = tiles
  itemsDefinition = itemsDefinition
  monstersDefinition = monstersDefinition
  now : number

  constructor(private cdRef : ChangeDetectorRef) { }

  ngOnInit() {
    frameInterval.subscribe(val => {
      this.now = Date.now()
    })
  }
  
  ngAfterViewChecked(): void {
    this.cdRef.detectChanges()
  }

  selectTile(tileID) {
    this.game.mapEditor.selectTile(tileID)
  }

  isTileSelected = (i) => i=== this.game.mapEditor.selection.id && this.game.mapEditor.selection.type == MapEditorSelectionType.Tile

  cleanTileSelection() {
    this.game.mapEditor.cleanSelection()
  }

  getTileSprite(tile:Tile) {
    return `url('assets/spriteTable.png') 0px -${tile.frames[tile.getCurrentFrame(this.now)] * 24}px`
  }
  
  selectItem(itemID) {
    this.game.mapEditor.selectItem(itemID)
  }

  isItemSelected = (i) => i === this.game.mapEditor.selection.id && this.game.mapEditor.selection.type == MapEditorSelectionType.Item
  
  getItemSprite(item:ItemDefinition) {
    return `url('assets/ItemSpriteTable.png') 0px -${item.frames[item.getCurrentFrame(this.now)] * 24}px`
  }

  selectMonster(MonsterID) {
    this.game.mapEditor.selectMonster(MonsterID)
  }

  isMonsterSelected = (i) => i === this.game.mapEditor.selection.id && this.game.mapEditor.selection.type == MapEditorSelectionType.Monster
  
  getMonsterSprite(monster:MonsterDefinition) {
    const currentFrame = ((this.now/100) % monster.frames.length)|0
    return `url('${monster.frames[currentFrame].src}')`
  }
  
  getMonsterSpriteStyle(monster:MonsterDefinition) {
        return monster.size.x > 1 || monster.size.y > 1
  }

  selecteEraser() {
    this.game.mapEditor.selection.type = MapEditorSelectionType.Eraser
  }
  
  mapToClipboard() {
    var el : any = document.createElement('textarea');
    el.value = this.game.mapEditor.saveMap();
    el.setAttribute('readonly', '');
    el.style = {position: 'absolute', left: '-9999px'};
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
    this.game.screenMessageManager.addMessage("Your map was succesfully copied to the clipboard.\nDon't forget to save it somewhere.")
  }
}