import { Component, OnInit, Input } from '@angular/core';
import Player from '../../game-engine/things/player';

@Component({
  selector: 'app-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.css']
})
export class StatusBarComponent implements OnInit {
  @Input() player : Player

  constructor() { }

  ngOnInit() {
  }

  getHealthBarSize() {    
    if (!this.player)
      return "180px"
    return this.player.health / this.player.max_health * 180 + "px"
  }

  isDead() {
    if (!this.player)
      return false;
    return this.player.health <= 0;
  }

  getManaBarSize() {    
    if (!this.player)
      return "180px"
    return this.player.mana / this.player.max_mana * 180 + "px"
  }

  lightHealing() {
    this.player.speak('exura');
  }
}
